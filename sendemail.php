<?php

session_start();
//send email
require 'PHPMailer/PHPMailerAutoload.php';
$_SESSION['email_sent'] = 'false';
$emailAdd = $_SESSION['email'];
$result = array();

$mail = new PHPMailer;

$mail->isSMTP();                            // Set mailer to use SMTP
$mail->Host = 'optimus.vqbn.com';             // Specify main and backup SMTP servers
$mail->SMTPAuth = false;                     // Enable SMTP authentication
$mail->Username = 'no.reply@viewqwest.com';          // SMTP username
$mail->Password = '$c.password'; // SMTP password
$mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
$mail->Port = 25;                          // TCP port to connect to

$mail->setFrom('no-reply@viewqwest.com', 'ViewQwest');
$mail->addReplyTo('no-reply@viewqwest.com', 'ViewQwest');
$mail->addAddress($emailAdd);   // Add a recipient
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

$mail->isHTML(true);  // Set email format to HTML
//$custName = $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
$custName = $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
$token = rand_passwd();
$_SESSION['securitytoken'] = $token;

$bodyContent = '';

$bodyContent .= '<table id="main-table" border="0" cellpadding="0" cellspacing="0" class="body" style="border:1px solid #000;border-radius:5px;position: relative;left: 30%;height: 250px;padding:0px 20px;">' .
        '<tr>' .
        '<td style="height:70px;">' .
        '<img style="display:block;margin:0 auto;" src="http://i.imgur.com/3vQSzzx.jpg" alt="ViewQwest Customer Portal"/>' .
        '</td>' .
        '</tr>' .
        '<tr>' .
        '<td style="padding-top:0px; vertical-align:top;height:50px;">' .
        '</b><p>Hi ' . $custName . ',</p>' .
        '<p>Please find your Security Token below.</p>' .
        '<p><b>' . $token . '</b></p>' .
        '<p>This is an automated email from ViewQwest Online Signup Portal.<br><br></p>' .
        '</td>' .
        '</tr>' .
        '</table>';

$mail->Subject = 'Viewqwest Online Signup Portal - Security Token';
$mail->Body = $bodyContent;

if (!$mail->send()) {
  $_SESSION['email_sent'] = 'false';
} else {
  $_SESSION['email_sent'] = 'true';
   
}


function rand_passwd($length = 4, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
    return substr(str_shuffle($chars), 0, $length);
}

?>