function getVqValue(name){
    // return $.cookie(name);
    return $.ajax({
      method: "POST",
      url: "util.php",
      data: { action: "get", key: name},
      async: false
    }).responseText;
}

function setVqValue(name, value){
    // $.cookie(name, value);
    $.ajax({
      method: "POST",
      url: "util.php",
      data: { action: "set", key: name, val: value },
      async: false
    })
}

function removeVqValue(name){
    // $.removeCookie(name);
    $.ajax({
      method: "POST",
      url: "util.php",
      data: { action: "removeOne", key: name},
      async: false
    })
}

function removeAllVqValue(){
  $.ajax({
    method: "POST",
    url: "util.php",
    data: { action: "removeAll"},
    async: false
  })
}
