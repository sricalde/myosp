<?php 
include 'security.php';

session_start();
unset($_SESSION['payment_token']);
?>
<html>
    <head>
        <title>Secure Acceptance - API Payment Form Example</title>
        <link rel="stylesheet" type="text/css" href="payment.css"/>
        <script src="jquery-1.7.min.js" type="text/javascript"></script>
    </head>
    <body>

        <fieldset id="response">
            <legend>Receipt</legend>
            <div>
                <form id="receipt">
<?php
foreach ($_REQUEST as $name => $value) {
    if($name == 'payment_token'){
        $_SESSION['payment_token'] = $value;
    }
    $params[$name] = $value;
    echo "<span>" . $name . "</span><input type=\"text\" name=\"" . $name . "\" size=\"50\" value=\"" . $value . "\" readonly=\"true\"/><br/>";
}

echo "<span>Signature Verified:</span><input type=\"text\" name=\"verified\" size=\"50\" value=\"";
if (strcmp($params["signature"], sign($params)) == 0) {
    echo "True";
} else {
    echo "False";
}
echo "\" readonly=\"true\"/><br/>";
?>
                </form>
            </div>
        </fieldset>
<div id="cc_payment_transaction">
    Please Wait
</div>
    </body>
</html>
<script>
alert("Success");
$(document).ready(function(){
$('#cc_payment_transaction').load("create_accnt.php");
});
</script>
