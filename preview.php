<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/custom/preview.js" type="text/javascript"></script>
        <script src="js/pageLoader.js"></script>
        <script src="js/verify.notify.min.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>

        <link href="css/pageLoaderLoadingBar.css" rel="stylesheet" type="text/css" />
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <style type="text/css">
            .cssload-container{
                display: block;
                margin:49px auto;
                width:97px;
            }

            .cssload-loading i{
                width: 19px;
                height: 19px;
                display: inline-block;
                border-radius: 50%;
                background: rgb(224,23,56);
            }
            .cssload-loading i:first-child{
                opacity: 0;
                animation:cssload-loading-ani2 0.58s linear infinite;
                -o-animation:cssload-loading-ani2 0.58s linear infinite;
                -ms-animation:cssload-loading-ani2 0.58s linear infinite;
                -webkit-animation:cssload-loading-ani2 0.58s linear infinite;
                -moz-animation:cssload-loading-ani2 0.58s linear infinite;
                transform:translate(-19px);
                -o-transform:translate(-19px);
                -ms-transform:translate(-19px);
                -webkit-transform:translate(-19px);
                -moz-transform:translate(-19px);
            }
            .cssload-loading i:nth-child(2),
            .cssload-loading i:nth-child(3){
                animation:cssload-loading-ani3 0.58s linear infinite;
                -o-animation:cssload-loading-ani3 0.58s linear infinite;
                -ms-animation:cssload-loading-ani3 0.58s linear infinite;
                -webkit-animation:cssload-loading-ani3 0.58s linear infinite;
                -moz-animation:cssload-loading-ani3 0.58s linear infinite;
            }
            .cssload-loading i:last-child{
                animation:cssload-loading-ani1 0.58s linear infinite;
                -o-animation:cssload-loading-ani1 0.58s linear infinite;
                -ms-animation:cssload-loading-ani1 0.58s linear infinite;
                -webkit-animation:cssload-loading-ani1 0.58s linear infinite;
                -moz-animation:cssload-loading-ani1 0.58s linear infinite;
            }

            @keyframes cssload-loading-ani1{
                100%{
                    transform:translate(39px);
                    opacity: 0;
                }
            }

            @-o-keyframes cssload-loading-ani1{
                100%{
                    -o-transform:translate(39px);
                    opacity: 0;
                }
            }

            @-ms-keyframes cssload-loading-ani1{
                100%{
                    -ms-transform:translate(39px);
                    opacity: 0;
                }
            }

            @-webkit-keyframes cssload-loading-ani1{
                100%{
                    -webkit-transform:translate(39px);
                    opacity: 0;
                }
            }

            @-moz-keyframes cssload-loading-ani1{
                100%{
                    -moz-transform:translate(39px);
                    opacity: 0;
                }
            }

            @keyframes cssload-loading-ani2{
                100%{
                    transform:translate(19px);
                    opacity: 1;
                }
            }

            @-o-keyframes cssload-loading-ani2{
                100%{
                    -o-transform:translate(19px);
                    opacity: 1;
                }
            }

            @-ms-keyframes cssload-loading-ani2{
                100%{
                    -ms-transform:translate(19px);
                    opacity: 1;
                }
            }

            @-webkit-keyframes cssload-loading-ani2{
                100%{
                    -webkit-transform:translate(19px);
                    opacity: 1;
                }
            }

            @-moz-keyframes cssload-loading-ani2{
                100%{
                    -moz-transform:translate(19px);
                    opacity: 1;
                }
            }

            @keyframes cssload-loading-ani3{
                100%{
                    transform:translate(19px);
                }
            }

            @-o-keyframes cssload-loading-ani3{
                100%{
                    -o-transform:translate(19px);
                }
            }

            @-ms-keyframes cssload-loading-ani3{
                100%{
                    -ms-transform:translate(19px);
                }
            }

            @-webkit-keyframes cssload-loading-ani3{
                100%{
                    -webkit-transform:translate(19px);
                }
            }

            @-moz-keyframes cssload-loading-ani3{
                100%{
                    -moz-transform:translate(19px);
                }
            }
            @media only screen and (min-width: 481px) and (max-width: 640px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin: auto;
                    margin-left: -160px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -149px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -65px;
                }
                #progressbar li:nth-child(3):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(7):before {
                    width: 37px;
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -36px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #progressbar li:nth-child(7):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    background: #FFF;
                }
                #progressbar li:nth-child(6):after {
                    background: #F15757;
                }
                #pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
                    display: none;
                }
            }
            @media only screen and (min-width: 321px) and (max-width: 480px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin: auto;
                    margin-left: -220px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -149px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -65px;
                }
                #progressbar li:nth-child(3):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(7):before {
                    width: 37px;
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -36px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    background: #FFF;
                }
                #progressbar li:nth-child(7):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    background: #F15757;
                }
                li#pb8{
                    width: 0px;
                }
                #pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
                    display: none;
                }
            }
            @media only screen and (max-width: 320px){
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin: auto;
                    margin-left: -240px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -149px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -65px;
                }
                #progressbar li:nth-child(3):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(7):before {
                    width: 37px;
                    content: ". .";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -28px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    background: #FFF;
                }
                #progressbar li:nth-child(7):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    background: #F15757;
                }
                li#pb8{
                    width: 0px;
                }
                li#pb7{
                    display: none;
                }
                #pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
                    display: none;
                }
            }
        </style>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TF99FW');</script>
        <!-- End Google Tag Manager -->

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>

                <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <li id="pb6" class="active"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">SUCCESS</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
        <div class="wrapper">
            <div class="previewWrapper">
                <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <div class="preview-left-panel" id="confirmation-details">
                    <h1>CONFIRMATION OF DETAILS</h1>
                    <h3 class="preview-h3">SUBSCRIBER'S PARTICULARS</h3>
                    <div class="preview-entry-container">
                        <div class="preview-text">SALUTATION</div>
                        <div class="preview-entry" id='salutation'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">FIRST NAME</div>
                        <div class="preview-entry" id='firstName'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">LAST NAME</div>
                        <div class="preview-entry" id='lastName'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">IC/PASSPORT</div>
                        <div class="preview-entry" id='nric'></div>
                    </div>
                    <div class="preview-entry-container preview-entry-container-address">
                        <div class="preview-text">ADDRESS</div>
                        <div class="preview-entry" id='address'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">DATE OF BIRTH</div>
                        <div class="preview-entry" id='dob'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">GENDER</div>
                        <div class="preview-entry" id='gender'></div>
                    </div>
                    <div class="preview-entry-container">
                        <div class="preview-text">NATIONALITY</div>
                        <div class="preview-entry" id='nationality'></div>
                    </div>

                    <div> <h3 class="preview-h3">PLAN DETAILS</h3>
                        <div class="preview-entry-container preview-plan-details-wide" style="margin-bottom:0px;">
                            <div class="preview-text">SUBSCRIPTION</div>
                            <div class="preview-entry subscription-text" id='subscription'></div>
                        </div>
						<p class="gst-note" style="font-family: 'open_sansregular';font-size: 12px;  color: #cbcbcb;     margin-left: 8px;">All prices are subject to 6% GST.</p>
                        <!-- <div class="preview-entry-container preview-plan-details">
                            <div class="preview-text">PROMOTIONAL OFFER</div>
                            <div class="preview-entry" id='promo'></div>
                        </div>
                       <div class="preview-entry-container preview-plan-details">
                            <div class="preview-text">HARDWARE TOP-UP OPTIONS</div>
                            <div class="preview-entry" id='router'></div>
                        </div>
                        <div class="preview-entry-container preview-plan-details" id="plan-details-fee">
                            <div class="preview-text">INSTALLATION CHARGE FEE</div>
                            <div class="preview-entry" id='adminFee'>RM100.00</div>
                                                      <div class="preview-text">STAMP DUTY FEE</div>
                            <div class="preview-entry" id='stampFee'>RM10.00</div>
                        </div>
                        <div class="preview-entry-container preview-remarks">
                            <div class="preview-text">REMARKS</div>
                            <div class="preview-entry remarks-text" id='remarksField'></div>
                        </div>
    
                        <h3 class="preview-h3">VALUE ADDED SERVICE</h3>
                        <div class="preview-entry-container preview-vas preview-vas-onevoice">
                            <div class="preview-text">ONE-VOICE @ $3.95</div>
                            <div class="preview-entry" id='oneVoice'></div>
                        </div>
                        <div class="preview-entry-container preview-vas preview-vas-onevoice">
                            <div class="preview-text">ONE-VOICE One Time Charge</div>
                            <div class="preview-entry" id='oneVoiceOneTimeCharge'></div>
                        </div>
                        <div class="preview-entry-container preview-vas preview-vas-number-non-display">
                            <div class="preview-text">NUMBER NON-DISPLAY @ $5.35</div>
                            <div class="preview-entry" id='numNonDisplay'></div>
                        </div>
                        <div class="preview-entry-container preview-vas preview-vas-fiber-guard-mod">
                            <div class="preview-text">FIBER GUARD @ $19.95/mth (Moderate)</div>
                            <div class="preview-entry" id='fiberGuardMod'></div>
                        </div>
                        <div class="preview-entry-container preview-vas preview-vas-fiber-guard-high">
                            <div class="preview-text">FIBER GUARD @ $19.95/mth (High)</div>
                            <div class="preview-entry" id='fiberGuardHigh'></div>
                        </div>
    
                        <div class="preview-entry-container preview-vas preview-vas-installationCharge">
                            <div class="preview-text">INSTALLATION CHARGE @ $80 per trip</div>
                            <div class="preview-entry" id='installationCharge'></div>
                        </div>
    
                        <div class="preview-entry-container preview-vas preview-vas-mediaPlayer tv1">
                            <div class="preview-text">ViewQwest TV Media Player @ $188</div>
                            <div class="preview-entry" id='mediaPlayer'></div>
                        </div>
                                            <div class="preview-entry-container preview-vas preview-vas-mediaPlayer tv2">
                            <div class="preview-text">ViewQwest TV Media Player with Daiyo Antenna Bundle @ $198</div>
                            <div class="preview-entry" id='mediaPlayer2'></div>
                        </div>
                        <!--Remove Eleven Sports
                        <div class="preview-entry-container preview-vas preview-vas-elevenSportsNetwork">
                            <div class="preview-text" id="elevenSportsNetworkText"></div>
                            <div class="preview-entry" id='elevenSportsNetwork'></div>
                        </div>-->
                    </div>
                    <div class="amountPayableWrapper" style="display:none;">
                        <h3 class="preview-h3">AMOUNT PAYABLE</h3>
                        <div class="amountPayableSubHeader">Charged upon application submission</div>
                        <div class="amountPayablePreviewWrapper">
                            <div class="amountPayablePreview">
                                <div class="preview-entry-container">
                                    <span style="font-family:'open_sansbold';" id="amountPayablePreviewTitle">Installation Charge Fee: </span><br/>
                                    <span style="color:#bbbdbe;" id="">Waived</span>
                                </div>
                                <div class="preview-entry-container" style="display: none;">
                                    <span style="font-family:'open_sansbold';" id="amountPayablePreviewTitle">Stamp Duty Fee:</span><br/>
                                    <span style="color:#bbbdbe;" id="">RM10.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="amountPayableFootnote" style="display:none;">
                        The above mentioned fees will be automatically deducted from your credit card within the next 7 days of submitting this application.
                        Your monthly recurring fee will not be charged until service is installed
                    </div>
                    <h3 class="preview-h3">DOCUMENT UPLOAD</h3>
                    <div id="loading" style="visibility:hidden; position:absolute;">
                        <div class="uploadingText" style="text-align:center; font-size:20px; color:#FFF;">Your file is being uploaded and should be done in a while</div>
                        <div class="cssload-container" style="margin-top:5px; margin-bottom:0px;">
                            <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                            <button type="button" id="cancelButton" style="margin-top:40px;">CANCEL</button>
                        </div>
                    </div>
                    <div class="uploadNRICwrapper" id="ic-container">
                        <div class="uploadNRICwrapperLeft">
                            <form method="post" id="fileinfo" enctype="multipart/form-data" name="fileinfo">
                                <label class="uploadNRIClabel">Upload your IC <span style="color:#f15855;">(FRONT)</span>:</label><br>
                                <input id="file" type="file" name="file" class="uploadNRICButton"/>
                                <div class="uploadConfirmFront" style="display:none; font-family:'open_sansbold'; color:#f15855; padding:10px 0px 2px 0px;"></div>
                                <div id="output"></div>
                            </form>
                        </div>
                        <div class="uploadNRICwrapperRight">
                            <form method="post" id="fileinfo2" enctype="multipart/form-data" name="fileinfo2">
                                <label class="uploadNRIClabel">Upload your IC <span style="color:#f15855;">(BACK)</span>:</label><br>
                                <input id="file2" type="file" name="file2" class="uploadNRICButton"/>
                                <div class="uploadConfirmBack" style="display:none; font-family:'open_sansbold'; color:#f15855; padding:10px 0px 2px 0px;"></div>
                                <div id="output"></div>
                            </form>
                        </div>
                    </div><!-- //uploadNRICwrapper -->

                    <!-- passport container -->
                    <div class="uploadNRICwrapper" id="passport-doc">
                        <div class="uploadNRICwrapperLeft">
                            <form method="post" id="passportinfo" enctype="multipart/form-data" name="fileinfo">
                                <label class="uploadNRIClabel">Upload your Passport <span style="color:#f15855;">(FRONT)</span>:</label><br>
                                <input id="passport1" type="file" name="file" class="uploadNRICButton"/>
                                <div class="uploadConfirmPassport" style="display:none; font-family:'open_sansbold'; color:#f15855; padding:10px 0px 2px 0px;"></div>
                                <div id="output"></div>
                            </form>
                        </div>
                    </div>
                    <div id="errorPopUp" class="white-popup mfp-hide">
                        <script type="text/javascript">
                            $("#errorPopUp").load("includes/errorPopUp.php")
                        </script>
                    </div>
					<div class="registration-button"><input type="button" id="prev" value="Previous" onclick="goBack()"/></div>
                    <input type="button" id="next" class="previewNextButton" value="Next">
                </div><!-- //preview-left-panel -->

                <!-- <input onClick="proceedToTerms()" type="button" id="next" class="previewNextButton" value="Next"> -->
				 <div id="verify-token-div"></div>

                <!-- FEEDBACK FORM -->
                <div class="feedbackFormWrapper">
                    <div class="feedbackFormDivider"></div>
                    <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                    <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                    <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                        <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                    </div><!-- //feedbackFormContent -->
                </div><!-- //feedbackFormWrapper -->

            </div><!-- //previewWrapper -->
        </div><!-- //wrapper -->

    </body>

    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
        function feedbackFormSubmit() {
            $('#feedbackFormGeneric').verify({
                if (result) {
                    var firstName = $('#firstName').val();
                    var lastName = $('#lastName').val();
                    var email = $('#email').val();
                    var phone = $('#phone').val();
                    var message = $('#message').val();
                    var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
                    $.ajax({
                        url: "feedbackFormEngine.php",
                        data: param,
                        type: "POST",
                        success: (function (data) {
                            swal({
                                title: "Thank You!",
                                text: "Email sent successfullly.",
                                showConfirmButton: false,
                                type: "success"
                            });
                            //window.location.href=window.location.href;
                        }),
                        complete: function () {
                            window.location.href = window.location.href;
                        }
                    })
                }
            })
        }

        $("#feedbackFormContent").load("feedbackForm.php");
        $('.feedbackFormLink').magnificPopup({
            type: 'inline',
            midClick: true
        });
    </script>

    <script>
        $('.open-popup-link').magnificPopup({
            type: 'inline',
            midClick: true
        });
    </script>
    <script>
        $(document).ready(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-red',
                radioClass: 'iradio_flat-red'
            });
        });
    </script>

</html>
<script type="text/javascript">
    $(document).ready(function () {
		$("#ic-container").css("display", "none");
        $("#passport-doc").css("display", "none");
        $("#creditcard-doc").css("display", "none");
        var oldToken = "<?php echo $_SESSION['securitytoken']; ?>";
        var checkNationality = "<?php echo $_SESSION['nationality']; ?>";
        var newToken = $("#token-field").val();
		if (checkNationality === "Foreigner") {
			$("#ic-container").css("display", "none");
			$("#passport-doc").css("display", "block");
		} else {
			$("#ic-container").css("display", "block");
			$("#passport-doc").css("display", "none");
		}
       


    });

    
    
</script>