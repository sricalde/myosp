var SelectedDates = {};
var $availableTimeSlots = null;

$(document).ready(function() {
	$( "#datepicker" ).datepicker({
		dateFormat: 'dd-mm-yy',
		altFormat: "dd-mm-yy",     
		beforeShowDay: function(date) {
			return [false, '', ''];
		},
		onSelect: function(selectedDate) {
			//alert(selectedDate);
			$("#installationDateStr").val(selectedDate);
			selectAnotherAvailableDay();
		}
	});
	
	var buildingType = getVqValue('reType');
	var action = getVqValue('reAction');
	var fcheckURL = "https://vops.viewqwest.com/vops/checkAvailableTimeSlot.json?apiKey=985C1381-436C-4836-9A23-4ADD69622C43&timeSlotCategory=B&orderType=schedule1" + "&buildingType=" + buildingType + "&action=" + action;
	var initDate = "";
	var firstHit = false;
	
	$.ajax({    
	  	url: fcheckURL,
	  	dataType: 'json',
	  	method: 'POST',
	  	success: function(results){
			$availableTimeSlots = results;
			SelectedDates = {};
			
			var selectedInstallationDateStillAvailable = false;
			//alert("$availableTimeSlots.availableTimeSlot.length-->"+$availableTimeSlots.availableTimeSlot.length);
	  
			var dStr = results.availableTimeSlot[0].date;   
			// retrieve current 3 days in advance
			var d = new Date(dStr.substring(0,4),(dStr.substring(4,6)) - 1, dStr.substring(6));
			var d1 = new Date();
			var d2 = new Date();
			var d3 = new Date();
		
			d1.setDate(d.getDate() + 1);
			d2.setDate(d1.getDate() + 1);
			d3.setDate(d2.getDate() + 1);
			recent2 = ((d.getMonth() + 1) < 10? "0"+(d.getMonth() + 1):(d.getMonth() + 1)) + '/' + (d.getDate() < 10? "0"+d.getDate():d.getDate()) + '/' + d.getFullYear();
			recent3 = ((d1.getMonth() + 1) < 10? "0"+(d1.getMonth() + 1):(d1.getMonth() + 1)) + '/' + (d1.getDate() < 10? "0"+d1.getDate():d1.getDate()) + '/' + d1.getFullYear();
			recent4 = ((d2.getMonth() + 1) < 10? "0"+(d2.getMonth() + 1):(d2.getMonth() + 1)) + '/' + (d2.getDate() < 10? "0"+d2.getDate():d2.getDate()) + '/' + d2.getFullYear();
			recent5 = ((d3.getMonth() + 1) < 10? "0"+(d3.getMonth() + 1):(d3.getMonth() + 1)) + '/' + (d3.getDate() < 10? "0"+d3.getDate():d3.getDate()) + '/' + d3.getFullYear();
		
			if($availableTimeSlots.availableTimeSlot != null && $availableTimeSlots.availableTimeSlot.length > 0){
				for (var i = 0; i < $availableTimeSlots.availableTimeSlot.length; i++) {
					var $timeSlot = $availableTimeSlots.availableTimeSlot[i];                    
					var dateStr = $timeSlot.date;
					  
					dateStr = dateStr.substring(4,6) + "/" + dateStr.substring(6) + "/" + dateStr.substring(0,4);
					// alert(recent2 + recent3 + recent4 + recent5);
					if (dateStr.includes(recent2) || dateStr.includes(recent3) || dateStr.includes(recent4) || dateStr.includes(recent5)){
					}
					else{
						if (!firstHit){         
							firstHit = true;
							initDate  = dateStr.substring(0,2) + "-" + dateStr.substring(3,5)+"-"+dateStr.substring(8);
							$("#datepicker").datepicker("setDate", new Date(initDate));
							$( "#datepicker" ).datepicker( "refresh" );
						}
						SelectedDates[dateStr] = dateStr;	
					}
				}
				
				//SelectedDates['06/13/2016'] = '06/13/2016';
				//var dStr = results.availableTimeSlot[0].date;                 
				//alert(dStr);          
				
				$( "#datepicker" ).datepicker("change",{
					beforeShowDay: function(date) { 
						var search = ((date.getMonth() + 1) < 10? "0"+(date.getMonth() + 1):(date.getMonth() + 1)) + '/' + (date.getDate() < 10? "0"+date.getDate():date.getDate()) + '/' + date.getFullYear();
						var recent2 = '';
						var recent3 = '';
						var recent4 = '';
						var recent5 = '';
						if (search in SelectedDates) {
							return [true, 'Highlighted', (SelectedDates[search] || '')];				    
						} 
						else{
							return [false, '', ''];
						}
					}                        
				});
				selectAnotherAvailableDay();
			}
			else{
				var msg = "";
				
				if($availableTimeSlots.errorCode != null){
					msg += "Error : " + $availableTimeSlots.errorCode + "\n";
				}
				if($availableTimeSlots.reason != null){
					msg += "Error Reason : " + $availableTimeSlots.reason + "\n";
				}
				if(msg != ""){
					msg += "Please try again.";
					alert(msg);
				}
			}
		}
	});  
});
	
	function selectAnotherAvailableDay(){
		if($availableTimeSlots != null){
			$('#installationTimeSlot option').remove();
			for (var i = 0; i < $availableTimeSlots.availableTimeSlot.length; i++) {
				var $timeSlot = $availableTimeSlots.availableTimeSlot[i];
				var date = $timeSlot.date;
				
				date = date.substring(6) + "-" + date.substring(4,6) + "-" + date.substring(0,4);
				
				var action = getVqValue('reAction');
				if($("#installationDateStr").val() == date){
					if(action == "SubmitNoAppointment" 
						|| action == "SubmitNoAppointmentReclass"
						|| action == "CannotSubmit"){
						$('#installationTimeSlot option').remove();
						$('#installationTimeSlot').append('<option value="NA" title="NA">NA</option>');
					}
					else{
						var timeSlot = $timeSlot.timeSlot;
						$('#installationTimeSlot').append('<option value="'+timeSlot+'" title="'+timeSlot+'">'+timeSlot+'</option>');
					}                            
				}                        
			}
		}
	}
			  
	function gotoPreview(){      
		if($("#installationDateStr").val() == "" || ($("#installationTimeSlot").val() == "")){
			swal({
				title: "No Installation Date Selected",
				text: "Please select your preferred Installation Date and Time to proceed.",
				type: "warning",
				confirmButtonText: "OK" 
				});
		}
		else{    
			setVqValue('installationDate', $("#installationDateStr").val()); 
			setVqValue('installationTimeSlot', $("#installationTimeSlot").val());
			window.location.href = "preview.php";
		}
	}
			
        

