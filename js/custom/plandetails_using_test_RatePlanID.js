$(window).unload(function() {
  $('select option').remove();
});

var routerPlan = "";
var routerPrice = "";
var routerLabel = "";

/*****## START OF RATE PLAN IDs DECLARATION ##*****/

/** SUBSCRIPTION RATE PLAN IDs **/
var plan_1Gbps_3MonthsFree_Monthly = "2c92a0ff56fe33f00157096198652a2e";//new variable
var plan_1Gbps_3MonthsFree_Annually = "2c92a0fd588608ba01588aa9edc40fce";//new variable

var plan_2Gbps_3MonthsFree_Monthly = "2c92a0fd56fe26b601570961b52d68e6";//new variable
var plan_2Gbps_3MonthsFree_Annually = "2c92a0ff5886110201588aa9fcf3608c";//new variable

var plan_1Gbps_9MonthsFree= "2c92a0fd5886091101588aa364d541f8";//new variable
var plan_2Gbps_9MonthsFree= "2c92a0ff588610e901588aa2b05e280a";//new variable

var plan_entertainmentPack = "2c92a0fd5886091101588aa5d90145c7";//new variable

var freeFreedomDNS ="2c92a0fd54cd2ff30154e5f82be9403e";//new variable
var vqInstallationCharge = "2c92a0fd560d13290156115785c95569";//new variable

/** ADMIN FEE RATE PLAN ID **/
var serviceActivationFee ="2c92a0ff588610e801588adfe18359f4";

/** ROUTER RATE PLAN IDs **/
var router1 = "2c92a0fd560d132301561159163d57ba";
//var router1_1Gbps = "2c92a0ff560d311b0156109336c14c52"; //ASUS RT-AC1200G+ Monthly
//var router1_2Gbps = "2c92a0fd560d132301561159163d57ba"; //Free 1x Asus RT-AC1200G+
var router2 = "2c92a0ff54cd4ae40154e60b8c736f68"; //ASUS RT-AC88U
var router3 ="2c92a0fd55822bcc01559a3a545b3179"; //ASUS RT-AC5300
var router4 = "No Router";
//var router5 = "2c92a0ff560d311b0156115a304970a5"; //2x Unifi AP AC Lite
//var router6 = "2c92a0fd560d13880156115aea0331d6"; //2x Unifi AP AC Pro

/** ROUTER PRICES **/
var router1Price = "Free 1 Unit"; //Free 1x Asus RT-AC1200G+
//var router1Price2Gbps = "Free 1 Unit"; //Free 1x Asus RT-AC1200G+
var router2Price = "$188"; //ASUS RT-AC88U
var router3Price = "$239"; //ASUS RT-AC5300
var router4Price = "N/A";
var router5Price = "Free 1 Unit"; //ViewQwest 4K TV
//var router6Price = "$299"; //2x Unifi AP AC Pro

var router1Label = "FREE 1x ASUS RT-AC1200G+";
//var router1Label1Gbps = "ASUS RT-AC1200G+ Monthly";
//var router1Label2Gbps = "FREE 1x ASUS RT-AC1200G+";
var router2Label = "ASUS RT-AC88U";
var router3Label = "ASUS RT-AC5300";
var router4Label = "";
var router5Label = "ViewQwest 4K Media Player";
//var router6Label = "2x Unifi AP AC Pro";

/** VAS RATE PLAN IDs **/
var oneVoice = "2c92a0fd54cd30610154e60152e93875";
var numNonDisplay = "2c92a0fd54cd2ff40154e6032f620545";
var oneVoiceActivationFee = "2c92a0ff56c0277e0156e518b2db5f09";//new variable
var fiberGuardMod = "2c92a0fe54cd4af00154e60475312fa6";
var fiberGuardHigh = "2c92a0fd54cd2ff40154e605d32d0be3";
var elevenSportsNetwork = "2c92a0fe56c027860156c114ecb316f0";
var mediaPlayer = "2c92a0fd56fe270b0156ff090deb77d5";//new variable
var vq4KTVmediaPlayerFree = "2c92a0ff588e53380158906529507f6d";//new variable

/*****## END OF RATE PLAN IDs DECLARATION ##*****/

//console.log(getVqValue('customerUID'));
//console.log(getVqValue('salesStaffName'));
//console.log(getVqValue('salesLocation'));

var plan = getVqValue('plan');

$(document).ready(function() {

	/* For Router Condition Statement
	if(plan == "1Gbps_3MonthsFree" || "1Gbps_EntPack"){
		$("#routerPriceVal1-2SN").show();
		//$(".routerSelContainer5").hide();
		//$(".routerSelContainer6").hide();
		$(".router2gbps").hide();
		//router1 = router1_1Gbps;
		//router1Label = router1Label1Gbps;
	}
	else{
		//$("#router2, #router3, #router4, #router5, #router6").addClass("routerName2gbps");
		$("#router2, #router3, #router4").addClass("routerName2gbps");
		router1 = router1_2Gbps;
		router1Label = router1Label2Gbps;
	}
	*/

	/* PLAN - This is WIREFRAME for Plan Details*/
	if(plan === "1Gbps_3MonthsFree"){
		$(".plan1gbps").show();
		$("#planDetails_1Gbps_3Months, .planSubHeader").show();
		$(".elevenSportNetworkText").text("6 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)");
		$(".plan1gbps_EntPack").remove();
	   $(".plan2gbpsSN").remove();
	   $(".plan2gbpsSN_EntPack").remove();
	   $("#1GbpsMonthlyH4_3MonthsFree").hide();
	   $("#1GbpsAnnualH4_3MonthsFree").hide();
		//$("#planDetails1Gbps_1").remove();
	   //$("#planDetails1Gbps_2").remove();
	}
	if(plan === "1Gbps_EntPack"){
		$(".plan1gbps_EntPack").show();
		$("#planDetails_1Gbps_EntPack, .planSubHeader").show();
		$(".plan2gbpsSN").remove();
		$(".plan1gbps").remove();
		$(".plan2gbpsSN_EntPack").remove();
		$(".oneVoiceWrapper").hide();
		$(".elevenSportsWrapper").hide();
		$("#1GbpsMonthlyH4_3MonthsFree").hide();
		$("#1GbpsAnnualH4_3MonthsFree").hide();
    	//$("#planDetails1Gbps_1").hide();
    	//$("#planDetails1Gbps_2").hide();
	}
	if(plan === "2Gbps_3MonthsFree") {
		$(".plan2gbpsSN").show();
		$("#planDetails_2Gbps_3Months, .planSubHeader").show();
		$(".plan1gbps").remove();
		$(".plan1gbps_EntPack").remove();
		$(".plan2gbpsSN_EntPack").remove();
		$(".elevenSportNetworkText").text("9 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)");
	}
	if(plan === "2Gbps_EntPack") {
		$(".plan2gbpsSN_EntPack").show();
		$("#planDetails_2Gbps_EntPack, .planSubHeader").show();
		$(".plan1gbps").remove();
		$(".plan2gbpsSN").remove();
		$(".plan1gbps_EntPack").remove();
		$(".oneVoiceWrapper").hide();
		$(".elevenSportsWrapper").hide();
		$(".installationChargeWrapper").hide();
	}

	var vopsCharges = getVqValue('reCharges');
	if(vopsCharges == "220"){
		$("#divFtp").show();
		$("#ftpCharge").text(vopsCharges);
	}
	else if(vopsCharges == "450"){
		$("#divFtp").show();
		$("#ftpCharge").text(vopsCharges);
	}
	else {
		$("#divFtp").hide();
	}


	/* ROUTER OPTIONS */
	$('.routerSel1').click(function() {
		$('.routerSel1').addClass("routerSelected1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		//$('.routerHead6').removeClass("routerSelHead6");
		$('.routerHead1').addClass("routerSelHead1");
		if($('.routerSel1').hasClass("routerSelected1")){
			routerPlan = router1;
			routerLabel = router1Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel2').click(function() {
		$('.routerSel2').addClass("routerSelected2");
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		//$('.routerHead6').removeClass("routerSelHead6");
		$('.routerHead2').addClass("routerSelHead2");
		if($('.routerSel2').hasClass("routerSelected2")){
			routerPlan = router2;
			routerPrice = router2Price;
			routerLabel = router2Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel3').click(function() {
		$('.routerSel3').addClass("routerSelected3");
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		//$('.routerHead6').removeClass("routerSelHead6");
		$('.routerHead3').addClass("routerSelHead3");
		if($('.routerSel3').hasClass("routerSelected3")){
			routerPlan = router3;
			routerPrice = router3Price;
			routerLabel = router3Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel4').click(function() {
		$('.routerSel4').addClass("routerSelected4");
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead5').removeClass("routerSelHead5");
		//$('.routerHead6').removeClass("routerSelHead6");
		$('.routerHead4').addClass("routerSelHead4");
		if($('.routerSel4').hasClass("routerSelected4")){
			routerPlan = router4;
			routerPrice = router4Price;
			routerLabel = router4Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel5').click(function() {
		$('.routerSel5').addClass("routerSelected5");
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		//$('.routerHead6').removeClass("routerSelHead6");
		$('.routerHead5').addClass("routerSelHead5");
		if($('.routerSel5').hasClass("routerSelected5")){
			routerPlan = vq4KTVmediaPlayerFree;
			routerPrice = router5Price;
			routerLabel = router5Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	/*
	$('.routerSel6').click(function() {
		$('.routerSel6').addClass("routerSelected6");
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		$('.routerHead6').addClass("routerSelHead6");
		if($('.routerSel6').hasClass("routerSelected6")){
			routerPlan = router6;
			routerPrice = router6Price;
			routerLabel = router6Label;
		}
		else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
		console.log(routerPlan);
		console.log(routerPrice);
	});
	*/
});

function gotoPreferredInstallation(){
	var productRatePlanId = "";
	var upfrontProductRatePlanId = "";

	/** SUBSCRIPTION RATE PLAN ID **/
	var subscriptionValue = $("#subscription").val();
	if(plan === "1Gbps_3MonthsFree"){
		switch(subscriptionValue){
			case "1GB-1"://Monthly Payment Options
				productRatePlanId += plan_1Gbps_3MonthsFree_Monthly + "_" + freeFreedomDNS;
			break;

			case "1GB-2"://Annually Payment Options
				productRatePlanId += plan_1Gbps_3MonthsFree_Annually + "_" + freeFreedomDNS;
			break;
		}
	}

	else if(plan === "1Gbps_EntPack"){
		switch(subscriptionValue){
			case "1GB-1_EntPack"://Monthly Payment Options
				productRatePlanId += plan_1Gbps_9MonthsFree + "_" + plan_entertainmentPack;
			break;
		}
	}

	else if(plan === "2Gbps_3MonthsFree"){
		switch(subscriptionValue){
			case "2SN-1"://Monthly Payment Options
				productRatePlanId += plan_2Gbps_3MonthsFree_Monthly + "_" + freeFreedomDNS;
			break;

			case "2SN-2"://Annually Payment Options
				productRatePlanId += plan_2Gbps_3MonthsFree_Annually + "_" + freeFreedomDNS;
			break;
		}
	}

	else if(plan === "2Gbps_EntPack"){
		switch(subscriptionValue){
			case "2SN-1_EntPack"://Monthly Payment Options
				productRatePlanId += plan_2Gbps_9MonthsFree + "_" + plan_entertainmentPack;
			break;
		}
	}

	/** VALUE ADDED SERVICES **/
	if($("#oneVoice").is(":checked")){
		$("#oneVoice").val(oneVoice);
		productRatePlanId += "_" + oneVoice + "_" + oneVoiceActivationFee;
	}

	if($("#numNonDisplay").is(":checked")){
		$("#numNonDisplay").val(numNonDisplay);
		productRatePlanId += "_" + numNonDisplay;
	}

	if($("#installationCharge").is(":checked")){
		$("#installationCharge").val(vqInstallationCharge);
		productRatePlanId += "_" + vqInstallationCharge;
	}

	if($("#mediaPlayer").is(":checked")){
		$("#mediaPlayer").val(mediaPlayer);
		productRatePlanId += "_" + mediaPlayer;
	}

	if($("#elevenSportsNetwork").is(":checked")){
		$("#elevenSportsNetwork").val(elevenSportsNetwork);
		productRatePlanId += "_" + elevenSportsNetwork;
	}

	/** VALUE ADDED SERVICES
	var products = document.getElementsByName("productRatePlanId");
	for(var i = 0; i < products.length; i++){
		if(products[i].checked)
			productRatePlanId += "_" + products[i].value;
			if( productRatePlanId.charAt( 0 ) === '_' ) {
				productRatePlanId = productRatePlanId.slice( 1 );
			}
	}
	**/

	if($("#fiberGuard").val() == "fiberGuardMod"){
		productRatePlanId += "_" + fiberGuardMod;
	}
	else if($("#fiberGuard").val() == "fiberGuardHigh"){
		productRatePlanId += "_" + fiberGuardHigh;
	}

	/*
	var vopsCharges = getVqValue('reCharges');
	if(vopsCharges == "220"){
		upfrontProductRatePlanId += "2c92a0fd54cd2ff40154e5f1567542f5";
	}
	else if(vopsCharges == "450"){
		upfrontProductRatePlanId += "2c92a0fc54cd3f980154e5f0a055107b";
	}
	*/

	/*Admin fee $8
	if(upfrontProductRatePlanId == ""){
		upfrontProductRatePlanId += serviceActivationFee;
	}
	else{
		upfrontProductRatePlanId += "_" + serviceActivationFee;
	}*/
	/*
	if(routerPlan == router1){
		if(router1 == router1_1Gbps){
			productRatePlanId += "_" + router1;
		}
		else{
			upfrontProductRatePlanId += "_" + router1;
		}
	}*/

	/** Service Activation Fee - $53.50 **/
	if(upfrontProductRatePlanId == ""){
		upfrontProductRatePlanId += serviceActivationFee;
	}
	else{
		upfrontProductRatePlanId += "_" + serviceActivationFee;
	}

	/** ROUTER RATE PLAN ID **/
	switch(routerPlan){
		case router1:
		productRatePlanId += "_" + router1;
		break;

		case router2:
		productRatePlanId += "_" + router2;
		break;

		case router3:
		productRatePlanId += "_" + router3;
		break;
		/*
		case router4:
		productRatePlanId += "_" + router4;
		break;
		*/
		case vq4KTVmediaPlayerFree:
		productRatePlanId += "_" + vq4KTVmediaPlayerFree;
		break;

		//case router6:
		//upfrontProductRatePlanId += "_" + router6;
		//break;
	}

	if($("#subscription").val() == ""){
		swal({
			title: "No Subscription Billing Selected",
			text: "Please select monthly or annual subscription billing to proceed.",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
	else if($("#fiberGuard").val() == ""){
		swal({
			title: "No Fiber Guard Selected",
			text: "Please select either Fiber Guard Moderate, Fiber Guard High or No Fiber Guard to proceed.",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
	else{
		setVqValue('upfrontProductRatePlanId', upfrontProductRatePlanId);
		setVqValue('productRatePlanId', productRatePlanId);
		setVqValue('router', routerPlan);
		setVqValue('routerLabel', routerLabel);
		setVqValue('routerPrice', routerPrice);
		setVqValue('subscription', $("#subscription").val());
		setVqValue('fiberGuard', $("#fiberGuard").val());
		setVqValue('oneVoice', $("#oneVoice").val());
		setVqValue('numNonDisplay', $("#numNonDisplay").val());
		setVqValue('installationCharge', $("#installationCharge").val());
		setVqValue('mediaPlayer', $("#mediaPlayer").val());
		setVqValue('elevenSportsNetwork', $("#elevenSportsNetwork").val());
		setVqValue('remarksField', $("#remarksField").val());
		//window.location.href = "preferred-installation.php";
		window.location.href = "preview.php";
	}
	console.log(productRatePlanId);
	console.log(upfrontProductRatePlanId);
}
