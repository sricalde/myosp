
	var prepopulateFields = {

	};



	var blobPdf;

	var base64dataPdf;

	var base64dataname;

	var tokenStr = "";

	var signatureStr = "";

	var keyStr = "";

	var paymethodId = "";

	var redirectUrl = "";

	var creditCardCountry = "";

	var creditCardState = "";

	var creditCardAddress1 = "";

	var creditCardAddress2 = "";

	var creditCardCity = "";

	var creditCardPostalCode = "";

	var creditCardPhoneNumber = "";

	var creditCardHolderName = "";

	var creditCardEmail = "";

	var nricFile1 = "";

	var nricFileName1 = "";

	var nricFile2 = "";

	var nricFileName2 = "";

	var customFieldsStr  ="";

	var billToContactStr  ="";

	var subscriptionStr = "";


	function getNRICFrontFile() {
		// get the NRIC file from Server which has been saved in the preview.html and parse into base64 code
		// The filename below can be retrieved from cookies/session
		var xhr = new XMLHttpRequest();

		xhr.open('GET', 'nricUpload/' + getVqValue('nricFileName1'), true);
		xhr.responseType = 'blob';
		xhr.onload = function(e) {
			if (this.status == 200) {
				// get binary data as a response
				var blobPdf = this.response;

				var reader = new window.FileReader();
				reader.readAsDataURL(blobPdf);
				reader.onloadend = function() {
					nricFileName1 = getVqValue('nricFileName1'); // NRIC Front
					nricFile1 = reader.result.substr(reader.result.indexOf(',') + 1);
					console.log(nricFile1);
				}

				reader.onerror = function() {
					// error due to HTTP PDF content unable to load
					redirectUrl = currentURL + "/callback/errorpaymentmethod.php";
					window.location.replace(redirectUrl);
				}
			}
		};

		xhr.send();
		
	}

	function getNRICBackFile() {
		// get the NRIC file from Server which has been saved in the preview.html and parse into base64 code
		// The filename below can be retrieved from cookies/session
		var xhr = new XMLHttpRequest();

		xhr.open('GET', 'nricUpload/' + getVqValue('nricFileName2'), true);
		xhr.responseType = 'blob';
		xhr.onload = function(e) {
			if (this.status == 200) {
				// get binary data as a response
				var blobPdf = this.response;

				var reader = new window.FileReader();
				reader.readAsDataURL(blobPdf);
				reader.onloadend = function() {
					nricFileName2 = getVqValue('nricFileName2'); // NRIC Back
					nricFile2 = reader.result.substr(reader.result.indexOf(',') + 1);
				}

				reader.onerror = function() {
					// error due to HTTP PDF content unable to load
					redirectUrl = currentURL + "/callback/errorpaymentmethod.php";
					window.location.replace(redirectUrl);
				}
			}
		};

		xhr.send();
	}

	
	var sf_FullName__c = getVqValue('salutation') + getVqValue('firstName') + getVqValue('lastName');
	var sf_Salutation = getVqValue('salutation') ;
        var $sf_BillingAddress = getVqValue('address1') + getVqValue('address2');
	var sf_FirstName = getVqValue('firstName');
	var sf_LastName = getVqValue('lastName');
	var sf_Gender__c = getVqValue('gender');
	var sf_NRIC_Passport_FIN_No__pc = getVqValue('nric');
	var sf_Phone = getVqValue('phone');
	var sf_PersonMobilePhone = getVqValue('mobile');
	var sf_PersonEmail = getVqValue('email');
	var sf_PersonBirthdate = getVqValue('dob');
	var sf_Nationality__c = 'Malaysian';
	var sf_Status__c = 'Active';
	var sf_RecordTypeId = '012900000003dcU';
	var sf_PersonMailingCountry = 'Malaysia';
	var sf_PersonMailingPostalCode =  getVqValue('postal');
	var sf_PersonOtherCountry = 'Malaysia';
	var sf_PersonOtherPostalCode = getVqValue('postal');
	var sf_Signup_ID__c = getVqValue('customerUID');
	// combineQuery(0,'zr_hpmCreditCardPaymentMethodId', paymethodId);
	var zr_hpmCreditCardPaymentMethodId = paymethodId;
	var zr_name = getVqValue('firstName') + getVqValue('lastName');
	var zr_currency = 'SGD';
	var zr_CustomerType__c = 'Residential'; // Production and development value may differ
	var zr_BillingPlatformUsername__c = getVqValue('email');
	var zr_billCycleDay = '1';
	var zr_batch =  'Batch4';
	var zr_paymentTerm = 'Net 7 Days';  // Production and development value may differ
	var zr_invoiceTemplateId = '4028e48836e7e287013702decab51c0e';  // Production and development value may differ
	var zr_invoiceCollect = 'true';
	var zr_firstName = getVqValue('firstName');
	var zr_lastName = getVqValue('lastName');
	var zr_country = creditCardCountry;
	var zr_workEmail = creditCardEmail;
	var zr_workPhone = creditCardPhoneNumber;
	var zr_address1 =  creditCardAddress1;
	//combineQuery(1,'zr_address2', creditCardAddress2);
	var zr_city = creditCardCity;
	//combineQuery(1,'zr_state',creditCardState );
	var zr_zipCode = creditCardPostalCode;



	var dat = new Date();
	

	var zr_termType =  'EVERGREEN';

	var zr_contractEffectiveDate = dat.toISOString().substring(0, 10);
	var zr_contractEffectiveDate = dat.toISOString().substring(0, 10);
	
	var zr_productRatePlanId =  getVqValue('productRatePlanId');
	var zr_upfrontProductRatePlanId = getVqValue('upfrontProductRatePlanId');



	var fd = new FormData();



	// PDF File

	fd.append('attachmentFile', base64dataPdf);

	fd.append('attachmentFileName', base64dataname);



	// NRIC Front

	fd.append('nricFile1',nricFile1);

		// console.log(nricFile1);



	fd.append('nricFileName1',nricFileName1);

		// console.log(nricFileName1);



	// NRIC Back

	fd.append('nricFile2',nricFile2);

		// console.log(nricFile2);



	fd.append('nricFileName2',nricFileName2);

		// console.log(nricFileName2);



	fd.append('customFields',customFieldsStr);

		// console.log(customFieldsStr);



	fd.append('billToContact',billToContactStr);

		// console.log(billToContactStr);
		//alert('bill to contact email ' + billToContactStr);



	fd.append('subscription',subscriptionStr);

	// console.log(subscriptionStr);
	// console.log('sf_FullName__c : ' + sf_FullName__c + '\n' + 'sf_Salutation: ' + sf_Salutation + '\n' +'sf_FirstName: ' + sf_FirstName + '\n' + 'sf_LastName: ' +sf_LastName);
	// console.log('nricFileName1');
	


