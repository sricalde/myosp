$(document).ready(function () {
	$("#phone, #ccNumber, #ccYear, #ccMonth, #mobile").keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		// allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
		return (
			key == 8 ||
			key == 9 ||
			key == 46 ||
			(key >= 37 && key <= 40) ||
			(key >= 48 && key <= 57) ||
			(key >= 96 && key <= 105));
	});
	// $("#reType").val(getVqValue('reType'));
	// console.log(getVqValue('reType'));
	// var address = getVqValue('reBlock') + " " + getVqValue('reStreet') + (getVqValue('reBuilding') == "" ? "" : " " + getVqValue('reBuilding')) + " " + getVqValue('reUnit');
	// $("#postal").val(getVqValue('postal'));
	// console.log("Registration postal: " + getVqValue('postal'));
	// $("#address").val(address);
});




function gotoPlanDetails() {
        var checkNationality = $("#nationality option:selected" ).val();
	var plan = getVqValue('plan');
	var nric = $("#nric").val();
	var phone = $("#phone").val();
	var mobile = $("#mobile").val();
	var email = $("#email").val();
	var fname = $("#firstName").val();
	var lname = $("#lastName").val();
	var dob = $("#dob").val();
        var ccNumber = $("#ccNumber").val();
        var ccMonth = $("#ccMonth").val();
        var ccYear = $("#ccYear").val();
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        var checkIC = /^\d{6}-\d{2}-\d{4}$/;
        var checkPassport = /^[a-zA-Z0-9]*$/;
        var checkPhoneNum = /^(\+?6?01)[0|1|2|3|4|6|7|8|9]\-*[0-9]{7,8}$/;
        var checkLandline = /^[0-9]{9,11}$/;
        var checkCCNumber = /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;

        
       
        

	if (fname == "" || lname == "" || dob == "") {
		swal({
			title: "Incomplete Form",
			text: "All fields are required. Please complete all details to proceed.",
			type: "warning",
			confirmButtonText: "OK"
		});
	} 
         
        else if(checkNationality === 'Malaysian/Permanent Resident' && !checkIC.test(nric)){
		swal({
			title: "Wrong IC Format",
			text: "Please provide a valid IC Number.",
			type: "warning",
			confirmButtonText: "OK"
		});
        }
        else if(checkNationality === 'Foreigner' && !checkPassport.test(nric)){
		swal({
			title: "Wrong Passport Format",
			text: "Please provide a valid Passport Number.",
			type: "warning",
			confirmButtonText: "OK"
		});
        } 
		else if(checkNationality === 'Foreigner' && nric == ""){
		swal({
			title: "Wrong Passport Format",
			text: "Please provide a valid Passport Number.",
			type: "warning",
			confirmButtonText: "OK"
		});
        } 
		else if(checkNationality != 'Foreigner' && checkNationality != 'Malaysian/Permanent Resident'){
		swal({
			title: "Residency status is not specify",
			text: "Please choose your Residency Status.",
			type: "warning",
			confirmButtonText: "OK"
		});
        }
	
	else if (!checkLandline.test(phone)) {
		swal({
			title: "Invalid phone number",
			text: "Numbers must be 9-11 digits including area code.",
			type: "warning",
			confirmButtonText: "OK"
		});
	} else if (!checkPhoneNum.test(mobile)) {
		swal({
			title: "Invalid mobile number",
			text: "Invalid mobile number",
			type: "warning",
			confirmButtonText: "OK"
		});
	} else if (email == "") {
		swal({
			title: "Email Address Empty",
			text: "Please provide a valid email address.",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
        else if (!filter.test(email)) {
		swal({
			title: "Wrong Email Format",
			text: "Please provide a valid email address.",
			type: "warning",
			confirmButtonText: "OK"
		});
	} 
        else if (nric != "") {
		console.log("API");
		var nricCheckURL = "https://vops.viewqwest.com/vops/checkSalesforceObject.getAccount?apiKey=985C1381-436C-4836-9A23-4ADD69622C43&nric=" + nric;
		$.ajax({
			url: nricCheckURL, 
			dataType: 'json',
			method: 'POST',
			async: false,
			success: function (results) {
				if (results.success != undefined && results.success == true) {
					swal({
						title: "Existing Customer",
						text: "Our system shows that you are an existing customer. Please contact our customer support.",
						type: "warning",
						confirmButtonText: "OK"
					});
				} else {
					setVqValue('salutation', $("#name_salutationacc2").val());
					setVqValue('firstName', $("#firstName").val());
					setVqValue('lastName', $("#lastName").val());
					setVqValue('nric', $("#nric").val());
					setVqValue('dob', $("#dob").val());
					setVqValue('nationality', $("#nationality").val());
					setVqValue('address1', $("#address1").val());
					setVqValue('address2', $("#address2").val());
					setVqValue('postal', $("#postal").val());
					setVqValue('phone', $("#phone").val());
					setVqValue('mobile', $("#mobile").val());
					setVqValue('email', $("#email").val());
					setVqValue('plan', plan);
					setVqValue('gender', $("input[name='gender']:checked").val());
					setVqValue('cardName', $("#ccName").val());
					setVqValue('cardNum', $("#ccNumber").val());
					setVqValue('expYear', $("#ccYear").val());
					setVqValue('expMonth', $("#ccMonth").val());
					//setVqValue('areaCode', $("#areaCode").val());
					//setVqValue('referralNric', $("#referralNric").val());
					//window.location.href = "plandetails.php";
					window.location.href = "preview.php";
				}
			}
		});
	}
}
