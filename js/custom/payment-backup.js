bajb_backdetect.OnBack = function()
{
var onBack = confirm("Your data will be lost. Are you sure you want to go back ?");
  if (onBack== true){
	alert("Your data will be lost");
  }else{
	  
	window.history.forward()	
   }
}

var prepopulateFields = {
};

var blobPdf;
var base64dataPdf;
var base64dataname;
var tokenStr = "";
var signatureStr = "";
var keyStr = "";
var paymethodId = "";
var redirectUrl = "";
var creditCardCountry = "";
var creditCardState = "";
var creditCardAddress1 = "";
var creditCardAddress2 = "";
var creditCardCity = "";
var creditCardPostalCode = "";
var creditCardPhoneNumber = "";
var creditCardHolderName = "";
var creditCardEmail = "";
var nricFile1 = "";
var nricFileName1 = "";
var nricFile2 = "";
var nricFileName2 = "";

// Sample params for rendering iframe on the client side

function callback(response) {

	if (response.success) {
		paymethodId = response.refId;
		creditCardCountry = response.creditCardCountry;
		creditCardState = response.creditCardState;
		creditCardAddress1 = response.creditCardAddress1;
		creditCardAddress2 = response.creditCardAddress2;
		creditCardCity = response.creditCardCity;
		creditCardPostalCode = response.creditCardPostalCode;
		creditCardPhoneNumber = response.phone;
		creditCardHolderName = response.creditCardHolderName;
		creditCardEmail = response.email;

		createAccount();

	}
	else {
		if (response.responseFrom == "Response_From_Submit_Page") {
			// Handle the error response when the Payment Page submission failed.
			redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.html?success=false&errorcode=" + response.errorCode + "&errorMessage=" + response.errorMessage;
		}
		else {
			// Handle the error response when the Payment Page request failed.
			redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.html?success=false&errorcode=" + response.errorCode + "&errorMessage=" + response.errorMessage;
		}
		//removeAllVqValue();
		window.location.replace(redirectUrl);
	}
	

}

var customFieldsStr  ="";
var billToContactStr  ="";
var subscriptionStr = "";
function combineQuery(type, str, value)
	{
		if (type ==0)
		{
			if (customFieldsStr == "")
				customFieldsStr = str + "=" + value;
			else
				customFieldsStr += "|" + str + "=" + value;
		}
		else if (type ==1)
		{
			if (billToContactStr == "")
				billToContactStr = str + "=" + value;
			else
				billToContactStr += "|" + str + "=" + value;
		}
		else
		{
			if (subscriptionStr == "")
				subscriptionStr = str + "=" + value;
			else
				subscriptionStr += "|" + str + "=" + value;
		}
	}

		
function createAccount()
{
	//setVqValue('creditCardCountry', creditCardCountry);
	//setVqValue('creditCardAddress1', creditCardAddress1);
	//setVqValue('creditCardPostalCode', creditCardPostalCode);
	//setVqValue('creditCardPhoneNumber', creditCardPhoneNumber);
	//setVqValue('creditCardEmail', creditCardEmail);
	
	//var installAddress = encodeURIComponent(getVqValue('reBlock') + " " + getVqValue('reStreet') + (getVqValue('reBuilding') == "" ? "" : " " + getVqValue('reBuilding')) + getVqValue('reUnit'));
	//var billingAddress = encodeURIComponent(getVqValue('reBlock') + " " + getVqValue('reStreet') + (getVqValue('reBuilding') == "" ? "" : " " + getVqValue('reBuilding')) + getVqValue('reUnit'));
	//var ccAddress1 = getVqValue('creditCardAddress1');
	//var ccAddress1encoded = encodeURIComponent(ccAddress1);
  
  var installAddress = getVqValue('reBlock') + " " + getVqValue('reStreet') + (getVqValue('reBuilding') == "" ? "" : " " + getVqValue('reBuilding')) + getVqValue('reUnit');
	var billingAddress = getVqValue('reBlock') + " " + getVqValue('reStreet') + (getVqValue('reBuilding') == "" ? "" : " " + getVqValue('reBuilding')) + getVqValue('reUnit');
  
	var createAccURL = "https://vops.viewqwest.com/vops/salesforceZuora.json?apiKey=985C1381-436C-4836-9A23-4ADD69622C43";
	
	combineQuery(0,'sf_FullName__c', getVqValue('salutation') + getVqValue('firstName') + getVqValue('lastName'));
	combineQuery(0,'sf_Salutation', getVqValue('salutation') );
	combineQuery(0,'sf_FirstName', getVqValue('firstName'));
	combineQuery(0,'sf_LastName', getVqValue('lastName'));
	combineQuery(0,'sf_Salutation', getVqValue('salutation'));
	combineQuery(0,'sf_Gender__c', getVqValue('gender'));
	combineQuery(0,'sf_NRIC_Passport_FIN_No__pc', getVqValue('nric'));
	combineQuery(0,'sf_Phone', getVqValue('phone'));
	combineQuery(0,'sf_PersonMobilePhone', getVqValue('mobile'));
	combineQuery(0,'sf_PersonEmail', getVqValue('email'));
	combineQuery(0,'sf_PersonBirthdate', getVqValue('dob'));
	combineQuery(0,'sf_Property_Type__c', getVqValue('reType'));
	combineQuery(0,'sf_Nationality__c', getVqValue('nationality'));
	//combineQuery(0,'sf_Race__c', '');
	//combineQuery(0,"Preferred Installation Date " + "%26" + " Time:"  + getVqValue('installationDate') + " " + getVqValue('installationTimeSlot'));
	//combineQuery(0,'sf_Referral_NRIC__c', getVqValue('referralNric') );
	combineQuery(0,'sf_Sign_up_Date__c', getVqValue('dateString'));
	combineQuery(0,'sf_Status__c', 'Active');
	combineQuery(0,'sf_RecordTypeId', '012900000003dcU' );
	combineQuery(0,'sf_PersonMailingCountry', 'Singapore');
	combineQuery(0,'sf_PersonMailingStreet', installAddress);
	combineQuery(0,'sf_PersonMailingPostalCode', getVqValue('postal'));
	combineQuery(0,'sf_PersonOtherCountry', 'Singapore');
	combineQuery(0,'sf_PersonOtherStreet', installAddress);
	combineQuery(0,'sf_PersonOtherPostalCode', getVqValue('postal'));
	combineQuery(0,'sf_Signup_ID__c', getVqValue('customerUID'));
	combineQuery(0,'sf_Sales_Staff__c', getVqValue('salesStaffName'));
	combineQuery(0,'sf_Sign_Up_Location__c', getVqValue('salesLocation'));
	combineQuery(0,'sf_Promoter_Name__c', getVqValue('promoterName'));
	combineQuery(0,'sf_Description', getVqValue('remarksField'));
	
	combineQuery(0,'zr_hpmCreditCardPaymentMethodId', paymethodId);
	combineQuery(0,'zr_name', getVqValue('firstName') + getVqValue('lastName'));
	combineQuery(0,'zr_currency', 'SGD');
	combineQuery(0,'zr_CustomerType__c', 'Residential'); // Production and development value may differ
	combineQuery(0,'zr_BillingPlatformUsername__c', getVqValue('email'));
	combineQuery(0,'zr_billCycleDay', '1');
	combineQuery(0,'zr_batch', 'Batch4');
	combineQuery(0,'zr_paymentTerm', ' Net 7 Days');  // Production and development value may differ
	combineQuery(0,'zr_invoiceTemplateId', '4028e48836e7e287013702decab51c0e');  // Production and development value may differ
	combineQuery(0,'zr_invoiceCollect', 'true');
	
	combineQuery(1,'zr_firstName', getVqValue('firstName'));
	combineQuery(1,'zr_lastName', getVqValue('lastName'));
	combineQuery(1,'zr_country', creditCardCountry);
	combineQuery(1,'zr_workEmail', creditCardEmail);
	combineQuery(1,'zr_workPhone', creditCardPhoneNumber);
	//combineQuery(1,'zr_address1', ccAddress1);
	combineQuery(1,'zr_address1', creditCardAddress1);
	//combineQuery(1,'zr_address2', creditCardAddress2);
	//combineQuery(1,'zr_city', creditCardCity);
	//combineQuery(1,'zr_state',creditCardState );
	combineQuery(1,'zr_zipCode', creditCardPostalCode);
	
	var dat = new Date();
	
	combineQuery(2,'zr_termType', 'EVERGREEN');
	combineQuery(2,'zr_contractEffectiveDate', dat.toISOString().substring(0, 10));
	combineQuery(2,'zr_productRatePlanId', getVqValue('productRatePlanId'));
	combineQuery(2,'zr_upfrontProductRatePlanId', getVqValue('upfrontProductRatePlanId'));
			
	var fd = new FormData();
	
	// PDF File
	fd.append('attachmentFile', base64dataPdf);
	fd.append('attachmentFileName', base64dataname);
	
	// NRIC Front
	fd.append('nricFile1',nricFile1);
		console.log(nricFile1);
	
	fd.append('nricFileName1',nricFileName1);
		console.log(nricFileName1);
	
	// NRIC Back
	fd.append('nricFile2',nricFile2);
		console.log(nricFile2);	
		
	fd.append('nricFileName2',nricFileName2);
		console.log(nricFileName2);
		
	fd.append('customFields',customFieldsStr);
		console.log(customFieldsStr);
		
	fd.append('billToContact',billToContactStr);
		console.log(billToContactStr);
		
	fd.append('subscription',subscriptionStr);
	console.log(subscriptionStr);

	//alert(createAccURL);

	$.ajax({
		url: createAccURL,
		dataType: 'json',
		method: 'POST',
		async: true,
		timeout: 200000,
		data: fd,
		processData: false,
		contentType: false,
		success: function(results) {
			if (results.success != undefined && results.success == true) {
				redirectUrl = "https://signup.vqbn.com/callback/success.php";
				//removeAllVqValue();
				window.location.replace(redirectUrl);

			} else
			{
				redirectUrl = "https://signup.vqbn.com/callback/errorpayment.php";
				//removeAllVqValue();
				window.location.replace(redirectUrl);
			}

		},
		error: function(xhr, textStatus, errorThrown) {
			redirectUrl = "https://signup.vqbn.com/callback/errorpayment.php";
      console.log(JSON.stringify(xhr));
			console.log(JSON.stringify(xhr.responseText));
      console.log(textStatus);
      console.log(errorThrown);
			//removeAllVqValue();
			window.location.replace(redirectUrl);
		}
	});
}

function getNRICFrontFile()
{
	// get the NRIC file from Server which has been saved in the preview.html and parse into base64 code
	// The filename below can be retrieved from cookies/session
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'nricUpload/' + getVqValue('nricFileName1'), true);
	xhr.responseType = 'blob';
	xhr.onload = function(e) {
		if (this.status == 200) {
			// get binary data as a response
			var blobPdf = this.response;

			var reader = new window.FileReader();
			reader.readAsDataURL(blobPdf);
			reader.onloadend = function() {
				nricFileName1 = getVqValue('nricFileName1'); // NRIC Front
				nricFile1 = reader.result.substr(reader.result.indexOf(',') + 1);
			}

			reader.onerror = function() {
				// error due to HTTP PDF content unable to load
				redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.html?";
				window.location.replace(redirectUrl);
			}
		}				
	};

	xhr.send();
}

function getNRICBackFile()
{
	// get the NRIC file from Server which has been saved in the preview.html and parse into base64 code
	// The filename below can be retrieved from cookies/session
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', 'nricUpload/' + getVqValue('nricFileName2'), true);
	xhr.responseType = 'blob';
	xhr.onload = function(e) {
		if (this.status == 200) {
			// get binary data as a response
			var blobPdf = this.response;

			var reader = new window.FileReader();
			reader.readAsDataURL(blobPdf);
			reader.onloadend = function() {
				nricFileName2 = getVqValue('nricFileName2'); // NRIC Back
				nricFile2 = reader.result.substr(reader.result.indexOf(',') + 1);
			}

			reader.onerror = function() {
				// error due to HTTP PDF content unable to load
				redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.html?";
				window.location.replace(redirectUrl);
			}
		}				
	};

	xhr.send();
}

function loadToken()
{
	// get the PDF from Server which has been saved in the preview.html and parse into base64 code
	// The filename below can be retrieved from cookies/session
	var xhr = new XMLHttpRequest();
	var pdf_filename = 'VQ_' + getVqValue('nric') +'.pdf'; // for generic file name (added: 2016/02/12) 
	
	xhr.open('GET', 'temp/' + pdf_filename, true);
	xhr.responseType = 'blob';
	xhr.onload = function(e) {
		if (this.status == 200) {
			// get binary data as a response
			var blobPdf = this.response;

			var reader = new window.FileReader();
			reader.readAsDataURL(blobPdf);
			reader.onloadend = function() {
				base64dataname = pdf_filename;
				base64dataPdf = reader.result;
				base64dataPdf = base64dataPdf.replace('data:application/pdf;base64,', '');
			}

			reader.onerror = function() {
				// error due to HTTP PDF content unable to load
				redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.html?";
				window.location.replace(redirectUrl);
			}
		}				
	};

	xhr.send();
	
	// get the NRIC file from Server which has been saved in the preview.html 
	// and parse into base64 code
	getNRICFrontFile(); // Front
	getNRICBackFile(); // Back
	
	// retrieve the security token
	var fcheckURL = "https://vops.viewqwest.com/vops/getZuoraToken.json?apiKey=985C1381-436C-4836-9A23-4ADD69622C43&pageId=" + "2c92a0fe549a3b910154a82e606a753a"
			+ "&method=POST&uri=" + "https://www.zuora.com/apps/PublicHostedPageLite.do";
			
	$.ajax({
		url: fcheckURL,
		dataType: 'json',
		method: 'POST',
		async: false,
		timeout: 30000,
		success: function(results) {

			if (results.success != undefined && results.success == true) {
				tokenStr = results.token;
				keyStr = results.key;
				signatureStr = results.signature;

				var prepopulateFields = {
					 creditCardCountry:"SGP",
				};	 

				var params = {
					tenantId: 1809,
					id: "2c92a0fe549a3b910154a82e606a753a", //ID for signup
					//id: "2c92a0fc567e2d0c015691d72ed71edd", //ID for signup-off
					//id: "2c92a0fd567e2d54015691d528165354", //ID for signup-off-testing
					token: tokenStr,
					signature: signatureStr,
					style: "inline",
					key: keyStr,
					submitEnabled: "true",
					locale: "fr_FR",
					url: "https://www.zuora.com/apps/PublicHostedPageLite.do",
					currency: "SGD",
					paymentGateway: "CyberSource Gateway" //payment gateway name
				};
				Z.render(
						params,
						prepopulateFields,
						callback
						);
			} else {
				//removeAllVqValue();
				redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.php";
				window.location.replace(redirectUrl);
			}

		},
		error: function()
		{
			// do something
			//removeAllVqValue();
			redirectUrl = "https://signup.vqbn.com/callback/errorpaymentmethod.php";
			window.location.replace(redirectUrl);
		}
	});
	
}

function loadHostedPage() {

	// load the security token
	loadToken();
}