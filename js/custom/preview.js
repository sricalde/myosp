function proceedToPayment() {
    swal({
        title: "Online Signup Portal",
        text: "We will now redirect you to payment page.  Please note that clicking the BACK BUTTON will remove all the details that you have entered.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "OK",
        closeOnConfirm: false
    },
            function () {
                gotoPayment();
            });
}
;
function acceptTermsandUploadNRIC() {
    sweetAlert("ERROR", "Please accept our Terms and Conditions to proceed.", "error");
}
;
function acceptTerms() {
    sweetAlert("Accept Terms and Conditions", "You must accept our Terms and Conditions to proceed.", "error");
}
;
function uploadPassport() {
    sweetAlert("Upload Passport", "Please upload your Passport (Front) to proceed.", "error");
}
;

function uploadNRIC() {
    sweetAlert("Upload NRIC", "Please upload your NRIC (Front and Back) to proceed.", "error");
}
;
function uploadNRICfront() {
    sweetAlert("Upload NRIC (FRONT)", "Please upload your NRIC (FRONT) to proceed.", "error");
}
;
function uploadNRICback() {
    sweetAlert("Upload NRIC (BACK)", "Please upload your NRIC (BACK) to proceed.", "error");
}
;
function uploadSuccess() {
    swal("Upload Successful", "Your file has been uploaded.", "success");
}
;
function fileExists() {
    sweetAlert("File Exists", "Sorry, file already exists. Please upload another file.", "error");
}
;
function fileTooLarge() {
    sweetAlert("File Too Large", "Sorry, your file is too large for upload.  File should not exceed 5mb in size.", "error");
}
;
function fileTooLarge2() {
    sweetAlert("Upload Not Responding", "Sorry, upload is taking too long to respond.  Could be your file is too large for upload.  File should not exceed 5mb in size.", "error");
}
;
function wrongFileFormat() {
    sweetAlert("Wrong File Format", "Sorry, only JPG, JPEG, PNG & GIF files are allowed.", "error");
}
;

function goBack() {
    window.history.back();
}
//upload NRIC front
function showLoading() {
    $("#loading").css("visibility", "visible");
}
function hideLoading() {
    $("#loading").css("visibility", "hidden");
}
function goBack() {
    window.history.back();
}

var x = '';
var y = '';

$(document).ready(function () {
    $("#file").change(function () {
        // function submitForm() {
        showLoading();
        $(".uploadNRICwrapper").css("visibility", "hidden");
        //console.log("submit event");

        $("#cancelButton").click(function () {
            hideLoading();
            fileTooLarge2();
            $(".uploadNRICwrapper").css("visibility", "visible");
            $("#file").val("");
        });

        var fd = new FormData(document.getElementById("fileinfo"));

        fd.append("label", "WEBUPLOAD");
        $.ajax({
            url: "uploadNRICfront.php",
            type: "POST",
            data: fd,
            processData: false,
            contentType: false
        }).done(function (data) {
            //console.log("PHP Output:");
            //$("#error").text(data);
            x = data;

            if (x == '4') {
                var file1 = document.querySelector('#file').files[0];
                var reader = new FileReader();
                var uploadedMessageFront = $(".uploadConfirmFront").show();
                var uploadedFilenameFront = document.getElementById("file").files[0].name;
                //var nricfile1 = "";

                reader.readAsDataURL(file1);
                reader.onloadend = function () {
                    setVqValue('nricFileName1', file1.name);
                    uploadSuccess();
                    $("#file").css("display", "none");
                    $(uploadedMessageFront).text(uploadedFilenameFront + " " + "UPLOADED");
                    hideLoading();
                    $(".uploadNRICwrapper").css("visibility", "visible");
                };
            } else if (x == '2') {
                fileTooLarge();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#file").val("");
            } else if (x == '3') {
                wrongFileFormat();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#file").val("");
            }
        });
    });

    $("#file2").change(function () {
        // function submitForm() {
        showLoading();
        $(".uploadNRICwrapper").css("visibility", "hidden");

        //console.log("submit event");
        $("#cancelButton").click(function () {
            hideLoading();
            fileTooLarge2();
            $(".uploadNRICwrapper").css("visibility", "visible");
            $("#file2").val("");
        });

        var fd = new FormData(document.getElementById("fileinfo2"));
        fd.append("label", "WEBUPLOAD");
        $.ajax({
            url: "uploadNRICback.php",
            type: "POST",
            data: fd,
            processData: false,
            contentType: false
        }).done(function (data) {
            //console.log("PHP Output:");
            //$("#error").text(data);
            y = data;
            //alert (x);

            if (y == '4') {
                var file2 = document.querySelector('#file2').files[0];
                var reader = new FileReader();
                var uploadedMessageBack = $(".uploadConfirmBack").show();
                var uploadedFilenameBack = document.getElementById("file2").files[0].name;
                //var nricfile2 = "";

                reader.readAsDataURL(file2);
                reader.onloadend = function () {
                    setVqValue('nricFileName2', file2.name);
                    uploadSuccess();
                    $("#file2").css("display", "none");
                    $(uploadedMessageBack).text(uploadedFilenameBack + " " + "UPLOADED");
                    hideLoading();
                    $(".uploadNRICwrapper").css("visibility", "visible");
                };
            } else if (y == '2') {
                fileTooLarge();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#file2").val("");
            } else if (y == '3') {
                wrongFileFormat();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#file2").val("");
            }
        });
    });


    //passport upload
    $("#passport1").change(function () {
        // function submitForm() {
        showLoading();
        $(".uploadNRICwrapper").css("visibility", "hidden");
        //console.log("submit event");

        $("#cancelButton").click(function () {
            hideLoading();
            fileTooLarge2();
            $(".uploadNRICwrapper").css("visibility", "visible");
            $("#passport1").val("");
        });

        var fd = new FormData(document.getElementById("passportinfo"));

        fd.append("label", "WEBUPLOAD");
        $.ajax({
            url: "uploadPassportFront.php",
            type: "POST",
            data: fd,
            processData: false,
            contentType: false
        }).done(function (data) {
            //console.log("PHP Output:");
            //$("#error").text(data);
            x = data;

            if (x == '4') {
                var file1 = document.querySelector('#passport1').files[0];
                var reader = new FileReader();
                var uploadedMessageFront = $(".uploadConfirmPassport").show();
                var uploadedFilenameFront = document.getElementById("passport1").files[0].name;
                //var nricfile1 = "";

                reader.readAsDataURL(file1);
                reader.onloadend = function () {
                    setVqValue('passportFileName1', file1.name);
                    uploadSuccess();
                    $("#passport1").css("display", "none");
                    $(uploadedMessageFront).text(uploadedFilenameFront + " " + "UPLOADED");
                    hideLoading();
                    $(".uploadNRICwrapper").css("visibility", "visible");
                };
            } else if (x == '2') {
                fileTooLarge();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#passport1").val("");
            } else if (x == '3') {
                wrongFileFormat();
                hideLoading();
                $(".uploadNRICwrapper").css("visibility", "visible");
                $("#passport1").val("");
            }
        });
    });

}); 
var plan = getVqValue('plan');
console.log(plan);

$(document).ready(function () {
    // 1Gbps $49.90
    // 1Gbps-SN $69.90
    // 1Gbps $888 $888
    // 2Gbps $1299 $1299

    if (plan == "2Gbps $1299")
    {
        $('#amountPayablePreviewTitle').text('Bundle Charge:');
        $('#amountPayablePreviewFee').text('$1299.00');
        $('#plan-details-fee').remove();
        $(".amountPayableFootnote").text('The above mentioned fees will be automatically deducted from your credit card within the next 7 days of submitting this application.');
    } else if (plan == "1Gbps $888")
    {
        $('#amountPayablePreviewTitle').text('Bundle Charge:');
        $('#amountPayablePreviewFee').text('$888.00');
        $(".amountPayableFootnote").text('The above mentioned fees will be automatically deducted from your credit card within the next 7 days of submitting this application.');
        $('#plan-details-fee').remove();
    }

    // Update netflix text description
    if (getVqValue('netflix') == "YES") {
        if (getVqValue('netflixOption') == "USA")
            $('#netflix').text("Yes. I watch Netflix US");
        else
            $('#netflix').text("Yes. I watch Netflix Singapore");
    } else
        $('#netflix').text("No. I don't watch netflix");

    $("#salutation").text(getVqValue('salutation'));
    $("#firstName").text(getVqValue('firstName'));
    $("#lastName").text(getVqValue('lastName'));
    $("#nric").text(getVqValue('nric'));
    $("#dob").text(getVqValue('dob'));
    $("#nationality").text(getVqValue('nationality'));
    //$("#referralNric").text(getVqValue('referralNric'));
    $("#gender").text(getVqValue('gender'));
    //$("#installationDate").text(getVqValue('installationDate'));
    //$("#installationTimeSlot").text(getVqValue('installationTimeSlot'));
    $("#address").text(getVqValue('address1') + ", " + getVqValue('address2') + " " + 'Malaysia' + " " + getVqValue('postal'));
    //$("#ftpCharge").text(getVqValue('reCharges'));

    /* SUBSCRIPTION */
    $("#subscription").text(getVqValue('plan'));
    var subscriptionValue = getVqValue('plan');
    var nationalityValue = getVqValue('nationality');
    console.log(subscriptionValue);
    console.log(nationalityValue);

    switch (subscriptionValue) {
        case "100Mbps - RM148":
            $("#subscription").text('100Mbps - RM148/mth');
            setVqValue("productRatePlanID", "2c92a0fc5e5094aa015e5b61e99549e7");
			setVqValue("planPrice", "RM148/Month");
			setVqValue("planSpeed", "100Mbps");
			setVqValue("planContact", "24 Month Contract");
			setVqValue("planIncludes", "<b>Free Rental</b>  NETGEAR<sup>&reg;</sup> AC1200 Smart WiFi Router (worth RM320) <br/> <b>Free</b> Dynamic Public IP Address <br/><b>24/7</b> Tech Support");
            break;

        case "300Mbps - RM188":
            $("#subscription").text('300Mbps - RM188/mth');
            setVqValue("productRatePlanID", "2c92a0fe5e509490015e5b63050d3ae3");
			setVqValue("planPrice", "RM188/Month");
			setVqValue("planSpeed", "300Mbps");
			setVqValue("planContact", "24 Month Contract");
			setVqValue("planIncludes", "<b>Free Rental</b>  NETGEAR<sup>&reg;</sup> AC1200 Smart WiFi Router (worth RM320) <br/> <b>Free</b> Dynamic Public IP Address <br/><b>24/7</b> Tech Support");
            break;

        case "500Mbps - RM298":
            $("#subscription").text('500Mbps - RM298/mth');
            setVqValue("productRatePlanID", "2c92a0fe5e50948b015e5b63b05e74ae");
			setVqValue("planPrice", "RM298/Month");
			setVqValue("planSpeed", "500Mbps");
			setVqValue("planContact", "24 Month Contract");
			setVqValue("planIncludes", "<b>Free Rental</b>  NETGEAR<sup>&reg;</sup> AC1200 Smart WiFi Router (worth RM320) <br/> <b>Free</b> Dynamic Public IP Address <br/><b>24/7</b> Tech Support");

            break;
    }
    setVqValue("installationCharges", "2c92c0f95e0da917015e138bdd9159d4");
    setVqValue("routerBundle", "2c92a0ff5e50a5f6015e5b64d1f359e0");
    console.log(getVqValue('productRatePlanID'));
    console.log(getVqValue('installationCharges'));
    console.log(getVqValue('routerBundle'));
    console.log(getVqValue('planPrice'));
    console.log(getVqValue('planSpeed'));
    console.log(getVqValue('planIncludes'));
    console.log(getVqValue('planContact'));


    /* PROMO */
    $("#promo").text(getVqValue('promo'));

    /* ROUTER */
    $("#router").text(getVqValue('routerLabel'));
    if (getVqValue('routerLabel') == "") {
        $("#router").text("No Router");
    }
    $("#routerName").text(getVqValue('routerLabel'));
    if (getVqValue('routerLabel') == "") {
        $("#routerName").text("Router");
    }
    $("#routerPrice").text(getVqValue('routerPrice'));
    if (getVqValue('routerPrice') == "") {
        $("#routerPrice").text("N/A");
    }

    /* ONE VOICE */
    $("#oneVoice").text(getVqValue('oneVoice'));
    if (getVqValue('oneVoice') != "") {
        $("#oneVoice").text('Selected');
        $("#oneVoiceOneTimeCharge").text('$50');
    } else {
        $(".preview-vas-onevoice").hide();
    }
    $("#numNonDisplay").text(getVqValue('numNonDisplay'));
    if (getVqValue('numNonDisplay') != "") {
        $("#numNonDisplay").text('Selected');
    } else {
        $(".preview-vas-number-non-display").hide();
    }

    /* INSTALLATION CHARGE */
    $("#installationCharge").text(getVqValue('installationCharge'));
    if (getVqValue('installationCharge') != "") {
        $("#installationCharge").text('Selected');
    } else {
        $(".preview-vas-installationCharge").hide();
    }

    /* REMARKS */
    $("#remarksField").text(getVqValue('remarksField'));
    if (getVqValue('remarksField') != "") {
        $("#remarksField").val();
    } else {
        $(".preview-remarks").hide();
    }

    /* MEDIA PLAYER */
    $("#mediaPlayer").text(getVqValue('mediaPlayer'));
    $("#mediaPlayer2").text(getVqValue('mediaPlayer2'));

    if (getVqValue('mediaPlayer') != "") {

        $("#mediaPlayer").text('Selected');

    } else {

        $(".tv1").hide()

    }
    if (getVqValue('mediaPlayer2') != "") {

        $("#mediaPlayer2").text('Selected');

    } else {
        $(".tv2").hide();
    }

    // console.log('Player 1: '+ getVqValue('mediaPlayer'));
    // console.log('Player 2: '+ getVqValue('mediaPlayer2'));
    // console.log('Eleven: '+ getVqValue('elevenSportsNetwork'));
    /* ELEVEN SPORTS NETWORK */
    // console.log(plan);

    $("#elevenSportsNetwork").text(getVqValue('elevenSportsNetwork'));
    if (plan == "1Gbps $49.90") {
        $("#elevenSportsNetworkText").text('ELEVEN SPORTS NETWORK (6 Months)');
    } else if (plan == "2Gbps-SN $69.90") {
        $("#elevenSportsNetworkText").text('ELEVEN SPORTS NETWORK (9 Months)');
    } else {
        $("#elevenSportsNetworkText").text('ELEVEN SPORTS NETWORK (Monthly)');
    }
    if (getVqValue('elevenSportsNetwork') != "") {
        $("#elevenSportsNetwork").text('Selected');
    } else {
        $(".preview-vas-elevenSportsNetwork").hide();
    }

    /* FIBER GUARD */
    $("#fiberGuard").text(getVqValue('fiberGuard'));
    if (getVqValue('fiberGuard') == "fiberGuardMod") {
        $("#fiberGuardMod").text('Selected');
        $(".preview-vas-fiber-guard-high").hide();
    } else {
        $(".preview-vas-fiber-guard-mod").hide();
    }
    $("#fiberGuard").text(getVqValue('fiberGuard'));
    if (getVqValue('fiberGuard') == "fiberGuardHigh") {
        $("#fiberGuardHigh").text('Selected');
        $(".preview-vas-fiber-guard-mod").hide();
    } else {
        $(".preview-vas-fiber-guard-high").hide();
    }

    // go to next page
    $('#next').click(function () {
        var value = $('#docstoupload').find(":selected").text();
        var checkCC = $(".uploadConfirmCreditCard").is(":visible");
        console.log(checkCC);
        if (value == 'IC') {
            if ((x != '4') && (y != '4')) {
                uploadNRIC();
            } else if (x != '4') {
                uploadNRICfront();
            } else if (y != '4') {
                uploadNRICback();
            } else {
                proceedToTerms();
            }
        } else {
            if ((x != '4')) {
                uploadPassport();
            } else {
                proceedToTerms();
            }
        }
    });


    //check docs to upload

});

function proceedToTerms() {
    // VQ to generate PDF here and save into server
    var formData = new FormData();

    formData.append("vq_salutation", getVqValue('salutation'));

    formData.append("vq_first_name", getVqValue('firstName'));
    formData.append("vq_last_name", getVqValue('lastName'));
    formData.append("vq_nric_passport", getVqValue('nric'));
    formData.append("vq_dob", getVqValue('dob'));
    formData.append("vq_gender", getVqValue('gender'));
    formData.append("vq_nationality", getVqValue('nationality'));
    //formData.append("vq_referralNric", getVqValue('referralNric'));
    //formData.append("vq_installationDate", getVqValue('installationDate'));
    //formData.append("vq_installationTimeSlot", getVqValue('installationTimeSlot'));
    formData.append("vq_phone", getVqValue('phone'));
    formData.append("vq_mobile", getVqValue('mobile'));
    formData.append("vq_email", getVqValue('email'));
    formData.append("vq_address", getVqValue('address1') + ", " +  getVqValue('address2') + " " + 'MALAYSIA' + " " + getVqValue('postal'));
    formData.append("vq_plan", getVqValue('plan'));
    formData.append("vq_plan_name", getVqValue('plan_name'));
    formData.append("vq_plan_desc", getVqValue('plan_desc'));
    //formData.append("vq_ftpCharge", getVqValue('reCharges'));
    formData.append("vq_adminFee", getVqValue('adminFee'));
    formData.append("vq_subscription", getVqValue('subscription'));
    formData.append("vq_promo", getVqValue('promo'));
    formData.append("vq_router", getVqValue('router'));
    formData.append("vq_routerLabel", getVqValue('routerLabel'));
    formData.append("vq_routerPrice", getVqValue('routerPrice'));
    formData.append("vq_oneVoice", getVqValue('oneVoice'));
    formData.append("vq_oneVoiceOneTimeCharge", getVqValue('oneVoiceOneTimeCharge'));
    formData.append("vq_numNonDisplay", getVqValue('numNonDisplay'));
    formData.append("vq_fiberGuard", getVqValue('fiberGuard'));
    //formData.append("vq_fiberGuardMod", getVqValue('fiberGuard'));
    //formData.append("vq_fiberGuardHigh", getVqValue('fiberGuard'));
    formData.append("vq_mediaPlayer", getVqValue('mediaPlayer'));
    formData.append("vq_mediaPlayer2", getVqValue('mediaPlayer2'));
    formData.append("vq_installationCharge", getVqValue('installationCharge'));
    formData.append("vq_elevenSportsNetwork", getVqValue('elevenSportsNetwork'));
    formData.append("vq_remarksField", getVqValue('remarksField'));
    formData.append("vq_netflix", getVqValue('netflix'));
    formData.append("vq_netflixOption", getVqValue('netflixOption'));
    formData.append("vq_price", getVqValue('planPrice'));
    formData.append("vq_speed", getVqValue('planSpeed'));
    formData.append("vq_contract", getVqValue('planContact'));
    formData.append("vq_freebies", getVqValue('planIncludes'));

    $.ajax({
        url: "pdf_creator.php",
        dataType: 'html',
        method: 'POST',
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        success: function (results) {
            // proceed to payment page
			$('#verify-token-div').load("sendemail.php");
			window.location.href = "terms-and-conditions.php";
			
			// console.log(formData);
        },
        error: function (xhr, textStatus, errorThrown) {
            // error here
            console.log('error....');
        }
    });
}
