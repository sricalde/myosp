$(window).unload(function() {
  $('select option').remove();
});
var routerPlan = "";
var routerPrice = "";
var routerLabel = "";
var promoPlan = "No Promo Offer";

/*****## START OF RATE PLAN IDs DECLARATION ##*****/
var currentLocation = window.location.href;
var currentURL = "";
if(currentLocation == "http://devmysignup.vqbn.com/vqosp-my/plandetails.php"){
	/** SUBSCRIPTION RATE PLAN IDs **/
	var Monthly_1Gbps = "2c92a0ff54cd4ae20154e5e734ca05ef"; //TEST
	var Upfront_1Gbps_888 = "2c92a0ff5aad0ca7015ac705ab325de8"; //TEST

	var Monthly_2Gbps = "2c92a0ff54cd4ae40154e5e983c202ec"; //TEST
	// var Upfront_2Gbps_1299 = "2c92a0fe5aacfac3015ac7037a654bd8"; //TEST
	var Upfront_2Gbps_1299 = "2c92a0fe5c218f0f015c2e3b47327ffd"; //TEST

	var cny17_88Rebate = "2c92a0fe5925c68e01594a7cc7212df7"; //TEST
	// var plan_1Gbps_9MonthsFree= "2c92a0fc588608ac01588b2b922b0d54"; //TEST

	/** INSTALLATION CHARGES **/
	var vqInstallationCharge = "2c92a0fd560d13290156115785c95569"; //TEST
	
	var serviceActivationFee_UpFront_1GB ="2c92a0ff5aad0ca7015ac705ab325de8"; // TEST
	var serviceActivationFee_UpFront_2GB ="2c92a0fe5c218f0f015c2e3b47327ffd"; // TEST

	/** ADMIN FEE RATE PLAN ID - $53.50 **/
	var serviceActivationFee ="2c92a0ff588610e801588adfe18359f4"; // TEST

	/** ROUTER RATE PLAN IDs **/
  /*
   * @params: default, bill rebate, router rebate
   */
	var router1Arr = ["2c92a0fd56c018c30156e506727205ac", "2c92a0fd5b42e39b015b6691eefc2d9f", "2c92a0fd560d132301561159163d57ba"]; //Asus RT-AC1200G+ - TEST
	var router2Arr = ["2c92a0ff56c027640156e52e16113424", "2c92a0ff5b42e3ae015b66922b1e47a0", "2c92a0fd5b42e39b015b66914cc82ae1"]; //ASUS RT-AC88U - TEST
	var router3Arr = ["2c92a0fe56c027860156e52d73bc2cb3", "2c92a0ff5b42e3ad015b6692580250bb", "2c92a0ff5b42e3ae015b66918283458b"]; //ASUS RT-AC5300 - TEST
	var router4Arr = ["2c92a0fc5aacfadd015ac70815e2499b", "2c92a0fd5b42e39b015b6691c0162cb6", "2c92a0fc5b42d2c9015b6690eaa5092b"]; //ASUS BRT-BRT-AC5828 - TEST

        var router1 = router1Arr[0];
        var router2 = router2Arr[0];
        var router3 = router3Arr[0];
        var router4 = router4Arr[0];
        var router5 = "No Router";

	/** VAS RATE PLAN IDs **/
	var freeFreedomDNS ="2c92a0ff59d9d544015a459778394e2d"; //TEST
        var freeFreedomDNS_3monthsFree ="2c92a0fd54cd2ff30154e5f82be9403e"; //TEST
        var freeFreedomDNS_9monthsFree ="2c92a0fe5aacfac3015ac70458244f02"; //TEST
        var freeFreedomDNS_12monthsFree ="2c92a0ff59d9d544015a459778394e2d"; //TEST
        var oneVoice = "2c92a0fd54cd30610154e60152e93875"; //TEST
        var oneVoice_12monthsFree = "2c92a0fe5a458b78015a4598f5c10ff8"; //TEST
	var numNonDisplay = "2c92a0fd54cd2ff40154e6032f620545"; //TEST
	var oneVoiceActivationFee = "2c92a0ff56c0277e0156e518b2db5f09"; // TEST
	var fiberGuardMod = "2c92a0fe54cd4af00154e60475312fa6"; //TEST
	var fiberGuardHigh = "2c92a0fd54cd2ff40154e605d32d0be3"; //TEST
        var elevenSportsNetworkMonthly = "2c92a0ff56c0277e0156e4d1c2df3280"; //Monthly - TEST
	var elevenSportsNetwork_6monthsFree = "2c92a0fe56c027860156c114ecb316f0"; //6 months free - TEST
	var elevenSportsNetwork_9monthsFree = "2c92a0ff588610e901588aa698103075"; //9 months free - TEST
	var vq4KTVmediaPlayerFree = "2c92a0ff588e53380158906529507f6d"; //TEST
	var vq4KTVmediaPlayer_topUp88 = "2c92a0fd56fe270b0156ff090deb77d5"; //TEST
	var vq4KTVmediaPlayer_topUp188 = "2c92a0fe5b42d2b0015b6693b97e187c"; //TEST
	var vq4KTVmediaPlayer_topUp198 = "2c92a0fe5c494468015c5c9f6fb82494"; //TEST

}

/***** deleted else condition for live ids ***********/

/*****## END OF RATE PLAN IDs DECLARATION ##*****/

/**
 * ROUTER PRICES
 *
 * @params: default, bill rebate, router rebate
 */
var router1PriceArr = ["$99", "$199", "FREE"]; //Asus RT-AC1200G+
var router2PriceArr = ["$329", "$429", "$229"]; //ASUS RT-AC88U
var router3PriceArr = ["$369", "$469", "$269"]; //ASUS RT-AC5300
var router4PriceArr = ["$459", "$559", "$359"]; //ASUS BRT-828

var router1Price = router1PriceArr[0]; //Asus RT-AC1200G+
var router2Price = router2PriceArr[0]; //ASUS RT-AC88U
var router3Price = router3PriceArr[0]; //ASUS RT-AC5300
var router4Price = router4PriceArr[0]; //ASUS BRT-828
var router5Price = "N/A";

/** ROUTER LABEL **/
var router1Label = "ASUS RT-AC1200G+";
var router2Label = "ASUS RT-AC88U";
var router3Label = "ASUS RT-AC5300";
var router4Label = "ASUS BRT-828";
var router5Label = "No Router";

var plan = getVqValue('plan');

$(document).ready(function() {
  /*
   * Toggle Netflix Form Field
   */
  $('input#netflixUSA').on('ifChecked', function(){
    setVqValue("netflixOption", "USA");
    //setVqValue("netflixOption", "");
    $('#netflixSG').iCheck('uncheck');
  });
  $('input#netflixSG').on('ifChecked', function(){
    setVqValue("netflixOption", "Singapore");
    //setVqValue("netflixOption", "");
    $('#netflixUSA').iCheck('uncheck');
  });

	/* PLAN - This is WIREFRAME for Plan Details*/
	if(plan === "1Gbps $49.90"){
     // Set the plan name of the plan
     setVqValue('plan_name', 'Monthly - 1Gbps Bundle');
     // Set the plan description
     var desc = "- 24 Months Subscription to Fibernet 1Gbps<br />"
     + "- Free 1x Static IP<br />"
     + "- Free Modem Rental<br />"
     + "- Free Delivery & Self-Installation<br />"
     + "- Free 3 Months Freedom DNS (optional @ $10.70/mth thereafter)<br />"
     + "- $200 bill rebate or $200 router discount";
     setVqValue('plan_desc', desc);

     // Plan detail sections
		 $("#planDetails_1Gbps_1").show();
     $("#1GbpsMonthlyH4_1").show();

     // Dropdown background
     $('#subscription').css('background-color', 'rgb(235, 235, 228)'); // change the background to grey color

     // Remove unnecessary options
	   $(".plan1gbps_2").remove();
	   $(".plan2gbps").remove();
	   $(".plan2gbpsSN").remove();
	   $(".24_months").remove();

     // ASUS BRT 828 Note
     $('#asus_brt_828_note').show();

     // Show "Plan detail"
     $(".planSubHeader").show();

     // Add service activation fee
     $(".oneTimeFeePrice").text("$53.50");
	}
  else if(plan === "2Gbps-SN $69.90"){
    // Set the name of the plan
    setVqValue('plan_name', 'Monthly - 2Gbps Single Network Bundle');
    // Set the plan description
    var desc = "- 24 Months Subscription to Fibernet 2Gbps Single Network<<br />"
                  + "- Free 1x Static IP<br />"
                  + "- Free Modem Rental<br />"
                  + "- Free Delivery & Installation<br />"
                  + "- Free 3 months Freedom DNS (optional @ $10.70/mth thereafter)<br />"
                  + "- $200 bill rebate or $200 router discount";
    setVqValue('plan_desc', desc);

    // Plan detail sections
    $("#planDetails_2GbpsSN").show();
    $("#2GbpsSNMonthlyH4").show();

    // Dropdown background
    $('#subscription').css('background-color', 'rgb(235, 235, 228)'); // change the background to grey color

    // Remove unnecessary options
    $(".plan1gbps_1").remove();
    $(".plan1gbps_2").remove();
    $(".plan2gbps").remove();
	$(".24_months").remove();

    // ASUS BRT 828 Note
    $('#asus_brt_828_note').show();

    // Show "Plan detail"
    $(".planSubHeader").show();

    // Add service activation fee
    $(".oneTimeFeePrice").text("$53.50");

    // Change the eleven sport text
    $('.elevenSportNetworkText').text('9 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)');

	} else if (plan === "1Gbps $888") {
		// Set the name of the plan
		setVqValue('plan_name', '1Gbps Upfront Plan');
		// Set the plan description
		var desc = "- 24 months Subscription to Fibernet 1Gbps<br />"
					  + "- 9 Months Free Freedom DNS (optional @$10.70/mth thereafter)<br/>"
					  + "- $100 Router Discount (upon signup – valid on select router models)<br/>"
					  + "- Free Modem Rental<br/>"
					  + "- Free Delivery & Self-Installation<br/>"
					  + "- Free 1x Static IP Address<br/>"
					  + "- Registration Fee ($53.50) waived";
		setVqValue('plan_desc', desc);

		// Plan detail sections
		$("#planDetails_1Gbps_2").show();
		$("#1GbpsMonthlyH4_2").show();

		// Dropdown background
		$('#subscription').css('background-color', 'rgb(235, 235, 228)'); // change the background to grey color

		// Remove unnecessary options
		$(".plan1gbps_1").remove();
		$(".plan2gbps_2").remove();
		$(".plan2gbps").remove();
		$(".plan2gbpsSN").remove();
		$(".monthly").remove();
		$(".activationFeeText").html('<span class="waived">Service Activation Fee: $53.50</span> <b>WAIVED</b>');
		$("#promotion-offer").remove();

		// ASUS BRT 828 Note
		$('#asus_brt_828_note').show();

		// Show "Plan detail"
		$(".planSubHeader").show();

		// Add service activation fee
		$(".oneTimeFeePrice").text("$53.50");
		$(".tv5").text("ViewQwest TV 5 - Media Player");
		

		// Change the eleven sport text
		$('.elevenSportNetworkText').text('9 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)');
	} else if (plan === "2Gbps $1299") {
		// Set the name of the plan
		setVqValue('plan_name', '2Gbps Upfront Plan');
		// Set the plan description
		var desc = "- 24 months Subscription to Fibernet 2Gbps – Single Network<br />"
					  + "- 12 Months Free Freedom DNS (optional @$10.70/mth thereafter)<br/>"
					  + "- 12 Months Free OneVoice (optional @$3.95/mth thereafter)<br/>"
					  + "- $100 Router Discount (upon signup – valid on select router models)<br/>"
					  + "- Free Modem Rental<br/>"
					  + "- Free Delivery & VQ Installation<br/>"
					  + "- Free 1x Static IP Address<br/>"
					  + "- Registration Fee ($53.50) waived";
		setVqValue('plan_desc', desc);

		// Plan detail sections
		$("#2GbpsMonthlyH4").show();
		$("#planDetails_2Gbps").show();

		// Dropdown background
		$('#subscription').css('background-color', 'rgb(235, 235, 228)'); // change the background to grey color

		// Remove unnecessary options
		$(".plan1gbps_1").remove();
		$(".plan2gbps_2").remove();
		$(".plan1gbps_2").remove();
		$(".plan2gbpsSN").remove();
		$(".monthly").remove();
		$(".activationFeeText").html('<span class="waived">Service Activation Fee: $53.50</span> <b>WAIVED</b>');
		$("#promotion-offer").remove();
		$(".oneVoiceWrapper").remove();
		$(".installationChargeWrapper").remove();
		$(".vasInstallPanelLeft").remove();
		$(".vasInstallPanelRight").css('margin-left','0px');

		// ASUS BRT 828 Note
		$('#asus_brt_828_note').show();

		// Show "Plan detail"
		$(".planSubHeader").show();

		// Add service activation fee
		$(".oneTimeFeePrice").text("$53.50");
		$(".tv5").text("ViewQwest TV 5 - Media Player");
		

		// Change the eleven sport text
		$('.elevenSportNetworkText').text('9 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)');
	}
	var vopsCharges = getVqValue('reCharges');
	if(vopsCharges == "220"){
		$("#divFtp").show();
		$("#ftpCharge").text(vopsCharges);
	}
	else if(vopsCharges == "450"){
		$("#divFtp").show();
		$("#ftpCharge").text(vopsCharges);
	}
	else {
		$("#divFtp").hide();
	}
  function update_plan(){
    if($('.routerHead1').hasClass("routerSelHead1")){
			routerPlan = router1;
			routerLabel = router1Label;
			routerPrice = router1Price;
		}
    else if($('.routerHead2').hasClass("routerSelHead2")){
			routerPlan = router2;
			routerLabel = router2Label;
			routerPrice = router2Price;
		}
    else if($('.routerHead3').hasClass("routerSelHead3")){
			routerPlan = router3;
			routerLabel = router3Label;
			routerPrice = router3Price;
		}
    else if($('.routerHead4').hasClass("routerSelHead4")){
			routerPlan = router4;
			routerPrice = router4Price;
			routerLabel = router4Label;
		}
    else{
			routerPlan = "No Router";
			routerPrice = "N/A";
		}
    console.log(routerPlan);
		console.log(routerPrice);
  }
  /* Promotional Offer */

  $('.promoSel1').click(function() {
    if($('.promoHead1').hasClass("promoSelHead1")){
      $('.promoHead1').removeClass("promoSelHead1");
      // set to default
      $('#routerPriceVal1').text('Top-up $99');

      // Comment added because ASUS RT-AC88U is out of stock
      $('#routerPriceVal2').text('Top-up $329');
      //$('#routerPriceVal2').text('Out of Stock');

      $('#routerPriceVal3').text('Top-up $369');
      $('#routerPriceVal4').text('Top-up $459');
      $('#routerDesc').text("");
      router1Price = router1PriceArr[0]; //Asus RT-AC1200G+
      router2Price = router2PriceArr[0]; //ASUS RT-AC88U
      router3Price = router3PriceArr[0]; //ASUS RT-AC5300
      router4Price = router4PriceArr[0]; //ASUS BRT-828

      router1 = router1Arr[0];
      router2 = router2Arr[0];
      router3 = router3Arr[0];
      router4 = router4Arr[0];
      promoPlan = "No Promo Offer";
      console.log('No Promo Offer');
      update_plan();
    }
    else{
      $('.promoHead1').addClass("promoSelHead1");
      $('.promoHead2').removeClass("promoSelHead2");
      $('.promoHead3').removeClass("promoSelHead3");
      $('#routerDesc').text("");
      $('#routerPriceVal1').text('Top-up $199');

      // Comment added because ASUS RT-AC88U is out of stock
      $('#routerPriceVal2').text('Top-up $429');
      //$('#routerPriceVal2').text('Out of Stock');

      $('#routerPriceVal3').text('Top-up $469');
      $('#routerPriceVal4').text('Top-up $559');
      router1Price = router1PriceArr[1]; //Asus RT-AC1200G+
      router2Price = router2PriceArr[1]; //ASUS RT-AC88U
      router3Price = router3PriceArr[1]; //ASUS RT-AC5300
      router4Price = router4PriceArr[1]; //ASUS BRT-828

      router1 = router1Arr[1];
      router2 = router2Arr[1];
      router3 = router3Arr[1];
      router4 = router4Arr[1];

      promoPlan = "$200 Bill Rebate";
      console.log('Bill rebate');
      update_plan();
    }
  });
  $('.promoSel2').click(function() {
    if($('.promoHead2').hasClass("promoSelHead2")){
      $('.promoHead2').removeClass("promoSelHead2");
      // set to default
      $('#routerPriceVal1').text('Top-up $99');

      // Comment added because ASUS RT-AC88U is out of stock
      $('#routerPriceVal2').text('Top-up $329');
      //$('#routerPriceVal2').text('Out of Stock');


      $('#routerPriceVal3').text('Top-up $369');
      $('#routerPriceVal4').text('Top-up $459');
      $('#routerDesc').text("");
      router1Price = router1PriceArr[0]; //Asus RT-AC1200G+
      router2Price = router2PriceArr[0]; //ASUS RT-AC88U
      router3Price = router3PriceArr[0]; //ASUS RT-AC5300
      router4Price = router4PriceArr[0]; //ASUS BRT-828

      router1 = router1Arr[0];
      router2 = router2Arr[0];
      router3 = router3Arr[0];
      router4 = router4Arr[0];
      promoPlan = "No Promo Offer";
      console.log('No Promo Offer');
      update_plan();
    }
    else{
      $('.promoHead2').addClass("promoSelHead2");
      $('.promoHead1').removeClass("promoSelHead1");
      $('.promoHead3').removeClass("promoSelHead3");
      $('#routerDesc').text("The pricing for the routers below is with the $200 router discount already applied");
      $('#routerPriceVal1').text('FREE');

      // Comment added because ASUS RT-AC88U is out of stock
      $('#routerPriceVal2').text('Top-up $229');
      //$('#routerPriceVal2').text('Out of Stock');

      $('#routerPriceVal3').text('Top-up $269');
      $('#routerPriceVal4').text('Top-up $359');
      router1Price = router1PriceArr[2]; //Asus RT-AC1200G+
      router2Price = router2PriceArr[2]; //ASUS RT-AC88U
      router3Price = router3PriceArr[2]; //ASUS RT-AC5300
      router4Price = router4PriceArr[2]; //ASUS BRT-828

      router1 = router1Arr[2];
      router2 = router2Arr[2];
      router3 = router3Arr[2];
      router4 = router4Arr[2];

      promoPlan = "$200 Router Discount";
      console.log('Router rebate');
      update_plan();
    }
  });

	/* ROUTER OPTIONS */
	$('.routerSel1').click(function() {
    $('.routerHead1').addClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");

    routerPlan = router1;
    routerLabel = router1Label;
    routerPrice = router1Price;

		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel2').click(function() {
    $('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		$('.routerHead2').addClass("routerSelHead2");

    routerPlan = router2;
    routerLabel = router2Label;
    routerPrice = router2Price;

    console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel3').click(function() {
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').removeClass("routerSelHead5");
		$('.routerHead3').addClass("routerSelHead3");

    routerPlan = router3;
    routerPrice = router3Price;
    routerLabel = router3Label;

		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel4').click(function() {
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead5').removeClass("routerSelHead5");
		$('.routerHead4').addClass("routerSelHead4");

    routerPlan = router4;
    routerPrice = router4Price;
    routerLabel = router4Label;

		console.log(routerPlan);
		console.log(routerPrice);
	});
	$('.routerSel5').click(function() {
		$('.routerHead1').removeClass("routerSelHead1");
		$('.routerHead2').removeClass("routerSelHead2");
		$('.routerHead3').removeClass("routerSelHead3");
		$('.routerHead4').removeClass("routerSelHead4");
		$('.routerHead5').addClass("routerSelHead5");

    routerPlan = router5;
    routerPrice = router5Price;
    routerLabel = router5Label;

		console.log(routerPlan);
		console.log(routerPrice);
	});
});

function gotoPreferredInstallation(){
	var productRatePlanId = "";
	var upfrontProductRatePlanId = "";

	/** SUBSCRIPTION RATE PLAN ID **/
	//var subscriptionValue = $("#subscription").val();

	switch(plan){
		case "100Mbps - RM148":
			/*
			- Free 3 months Freedom DNS
			*/
			productRatePlanId += Monthly_1Gbps + "_" + freeFreedomDNS_3monthsFree;
			break;

		case "300Mbps - RM188":
		  /*
			- Free 3 months Freedom DNS
		  */
		  productRatePlanId += Monthly_2Gbps + "_" + freeFreedomDNS_3monthsFree;
		  break;
		
		case "500Mbps - RM247":
		  /*
			- Free 9 months Freedom DNS
		  */
		  productRatePlanId += freeFreedomDNS_9monthsFree;
		  break;
		
	}
	
	/** VALUE ADDED SERVICES **/
	
	if($("#oneVoice").is(":checked")){
      $("#oneVoice").val(oneVoice);
      productRatePlanId += "_" + oneVoice + "_" + oneVoiceActivationFee;
	}

	if($("#numNonDisplay").is(":checked")){
		$("#numNonDisplay").val(numNonDisplay);
		productRatePlanId += "_" + numNonDisplay;
	}

	if($("#installationCharge").is(":checked")){
		$("#installationCharge").val(vqInstallationCharge);
		productRatePlanId += "_" + vqInstallationCharge;
	}

	if($("#mediaPlayer").is(":checked")){
		$("#mediaPlayer").val(vq4KTVmediaPlayer_topUp188);
		productRatePlanId += "_" + vq4KTVmediaPlayer_topUp188;
	}
	if($("#mediaPlayer2").is(":checked")){
		$("#mediaPlayer2").val(vq4KTVmediaPlayer_topUp198);
		productRatePlanId += "_" + vq4KTVmediaPlayer_topUp198;
	}

	if($("#elevenSportsNetwork").is(":checked")){
		if(plan == "1Gbps $49.90"){
			$("#elevenSportsNetwork").val(elevenSportsNetwork_6monthsFree);
			productRatePlanId += "_" + elevenSportsNetwork_6monthsFree;
		}
    else if(plan == "2Gbps-SN $69.90"){
      $("#elevenSportsNetwork").val(elevenSportsNetwork_9monthsFree);
      productRatePlanId += "_" + elevenSportsNetwork_9monthsFree;
    }
	}
 
  if($("input[name=netflix]:checked").val() == 'Yes'){
    setVqValue("netflix", "YES");
	}
  else{
    setVqValue("netflix", "NO");
  }

	if($("#fiberGuard").val() == "fiberGuardMod"){
		productRatePlanId += "_" + fiberGuardMod;
	}
	else if($("#fiberGuard").val() == "fiberGuardHigh"){
		productRatePlanId += "_" + fiberGuardHigh;
	}

  console.log('Plan: ' + plan);
	/** Service Activation Fee - $53.50 **/
	if(plan == "1Gbps $49.90" || plan == "2Gbps-SN $69.90"){
		if(upfrontProductRatePlanId == ""){
			upfrontProductRatePlanId += serviceActivationFee;
		}
		else{
			upfrontProductRatePlanId += "_" + serviceActivationFee;
		}
	} 
	
	if(plan == "1Gbps $888"){
		if(upfrontProductRatePlanId == ""){
			upfrontProductRatePlanId += Upfront_1Gbps_888;
		}
		else{
			upfrontProductRatePlanId += Upfront_1Gbps_888;
		}
	} 
	if(plan == "2Gbps $1299"){
		if(upfrontProductRatePlanId == ""){
			upfrontProductRatePlanId += Upfront_2Gbps_1299;
		}
		else{
			upfrontProductRatePlanId += Upfront_2Gbps_1299;
		}
	}
	
	

	/** ROUTER RATE PLAN ID **/
	switch(routerPlan){
		case router1:
  		productRatePlanId += "_" + router1;
  		break;

		case router2:
  		productRatePlanId += "_" + router2;
  		break;

		case router3:
  		productRatePlanId += "_" + router3;
  		break;

		case router4:
  		productRatePlanId += "_" + router4;
  		break;
	}

	if( productRatePlanId.charAt( 0 ) === '_' ) {
		productRatePlanId = productRatePlanId.slice( 1 );
	}
	else if( upfrontProductRatePlanId.charAt( 0 ) === '_' ) {
		upfrontProductRatePlanId = upfrontProductRatePlanId.slice( 1 );
	}

	if($("#subscription").val() == ""){
		swal({
			title: "No Subscription Billing Selected",
			text: "Please select monthly or annual subscription billing to proceed.",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
	else if($("#fiberGuard").val() == ""){
		swal({
			title: "No Fiber Guard Selected",
			text: "Please select either Fiber Guard Moderate, Fiber Guard High or No Fiber Guard to proceed.",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
  else if($("input[name=netflix]:checked").val() == 'Yes' && ! $("#netflixUSA").is(":checked") && !$("#netflixSG").is(":checked")){
		swal({
			title: "No netflix selected",
			text: "Please select either Netflix US or Netflix Singapore",
			type: "warning",
			confirmButtonText: "OK"
		});
	}
	else{
		setVqValue('upfrontProductRatePlanId', upfrontProductRatePlanId);
		setVqValue('productRatePlanId', productRatePlanId);
		setVqValue('promo', promoPlan);
		setVqValue('router', routerPlan);
		setVqValue('routerLabel', routerLabel);
		setVqValue('routerPrice', routerPrice);
		setVqValue('subscription', $("#subscription").val());
		setVqValue('fiberGuard', $("#fiberGuard").val());
		setVqValue('oneVoice', $("#oneVoice").val());
		setVqValue('numNonDisplay', $("#numNonDisplay").val());
		setVqValue('installationCharge', $("#installationCharge").val());
		setVqValue('mediaPlayer', $("#mediaPlayer").val());
		setVqValue('mediaPlayer2', $("#mediaPlayer2").val());
		setVqValue('elevenSportsNetwork', $("#elevenSportsNetwork").val());
		setVqValue('remarksField', $("#remarksField").val());
		window.location.href = "preview.php";
	}
	console.log("Monthly Payment (productRatePlanId) - " + productRatePlanId);
	console.log("OneTime/Annual Payment (upfrontProductRatePlanId) - " + upfrontProductRatePlanId);
}
