// JavaScript Document
<script>
function proceedToPayment() {
		$( "#proceedToPayment" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			  gotoPayment();
			}
		  }
		});
	};
	function acceptTermsandUploadNRIC() {
		$( "#acceptTermsandUploadNRIC" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function acceptTerms() {
		$( "#acceptTerms" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function uploadNRIC() {
		$( "#uploadNRIC" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function uploadNRICfront() {
		$( "#uploadNRICfront" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function uploadNRICback() {
		$( "#uploadNRICback" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function uploadSuccess() {
		$( "#uploadSuccess" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function fileExists() {
		$( "#fileExists" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function fileTooLarge() {
		$( "#fileTooLarge" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
	function wrongFileFormat() {
		$( "#wrongFileFormat" ).dialog({
		  modal: true,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};
</script>