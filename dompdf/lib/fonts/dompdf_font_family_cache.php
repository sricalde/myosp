<?php return array (
  'sans-serif' => array(
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => DOMPDF_FONT_DIR . 'Symbol',
    'bold' => DOMPDF_FONT_DIR . 'Symbol',
    'italic' => DOMPDF_FONT_DIR . 'Symbol',
    'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
  ),
  'serif' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSans-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSans-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSans-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans',
  ),
  'dejavu sans light' => array(
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' => array(
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed',
  ),
  'dejavu sans mono' => array(
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerif',
  ),
  'dejavu serif condensed' => array(
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed',
  ),
  'open_sansbold' => array(
    'normal' => DOMPDF_FONT_DIR . '988379df9a0524e25e58f667023e5abe',
  ),
  'open_sansbold_italic' => array(
    'normal' => DOMPDF_FONT_DIR . 'af69f46f95edb274eff7644d117e3f54',
  ),
  'open_sansextrabold' => array(
    'normal' => DOMPDF_FONT_DIR . 'b7954411f35f51c42cdaa5e07060976a',
  ),
  'open_sansextrabold_italic' => array(
    'normal' => DOMPDF_FONT_DIR . '395a721901c330ee0ad5af87c935d7e7',
  ),
  'open_sansitalic' => array(
    'normal' => DOMPDF_FONT_DIR . '5209a0e3dee79122b84a85459497ed3e',
  ),
  'open_sanslight' => array(
    'normal' => DOMPDF_FONT_DIR . '8eb26649846b3cd9a99f2cdb33610d49',
  ),
  'open_sanslight_italic' => array(
    'normal' => DOMPDF_FONT_DIR . '1831c0ce5b6d443405e47861302d502e',
  ),
  'open_sansregular' => array(
    'normal' => DOMPDF_FONT_DIR . 'df0d9ba95ff745ed6b5e38a69f75dffa',
  ),
  'open_sanssemibold' => array(
    'normal' => DOMPDF_FONT_DIR . 'd630576f6d3d092bd2355a64a5d3125b',
  ),
  'open_sanssemibold_italic' => array(
    'normal' => DOMPDF_FONT_DIR . 'c5a1457f40d9fb2031b0a7e82623a342',
  ),
  'proxima_nova_rgregular' => array(
    'normal' => DOMPDF_FONT_DIR . 'b596a27f048fe84256a246a0fea4e0cc',
  ),
  'trend_slabone' => array(
    'normal' => DOMPDF_FONT_DIR . 'f052fa020365a4c1fc0f75e3cb9382fa',
  ),
  'proxima_nova_rgsemibold' => array(
    'normal' => DOMPDF_FONT_DIR . '82561691dd970ae15331e20c2f24874b',
  ),
  'proxima_nova_rgbold' => array(
    'normal' => DOMPDF_FONT_DIR . 'ba908dd5a7ae1c0d9a293c654c0cb983',
  ),
  'robotobold_italic' => array(
    'normal' => DOMPDF_FONT_DIR . 'c88a55758cd820befa385ff839bd33bc',
  ),
  'helvetica_neuemedium' => array(
    'normal' => DOMPDF_FONT_DIR . 'f970d2ebcea584bf702857a3323a9f0a',
  ),
  'helvetica_neuemediumcond' => array(
    'normal' => DOMPDF_FONT_DIR . '7190d6ac46892c53d3614406c20403e0',
  ),
  'helvetica_neuemediumcondobl' => array(
    'normal' => DOMPDF_FONT_DIR . 'cb817292966dbb50e2849d6b05a48b2b',
  ),
  'helvetica_neuemediumext' => array(
    'normal' => DOMPDF_FONT_DIR . '9e2f9a4da4cd081319c49eb2273f0bf9',
  ),
  'helvetica_neuemediumextobl' => array(
    'normal' => DOMPDF_FONT_DIR . '77720c462cb2861ee860d4f540996c63',
  ),
  'helvetica_neuemediumitalic' => array(
    'normal' => DOMPDF_FONT_DIR . '7f698a5a9ae575b23b61b63db9685d8f',
  ),
  'helvetica_neueroman' => array(
    'normal' => DOMPDF_FONT_DIR . '7bcfc847f08e8c5445eb612484e84472',
  ),
  'helvetica_neuethin' => array(
    'normal' => DOMPDF_FONT_DIR . 'f478ebe2df902be2c434a0ae21f309c7',
  ),
  'helvetica_neuethincond' => array(
    'normal' => DOMPDF_FONT_DIR . '544a2dd1b8209364aaa912eab8a3093a',
  ),
  'helvetica_neuethincondobl' => array(
    'normal' => DOMPDF_FONT_DIR . '3ad82b89054594424efb3323be7aa1c0',
  ),
  'helvetica_neuethinext' => array(
    'normal' => DOMPDF_FONT_DIR . '6bf98183e02cd9c6e08430e1ee9769cd',
  ),
  'helvetica_neuethinextobl' => array(
    'normal' => DOMPDF_FONT_DIR . 'd051c194579d18925b607f6a96fc6fc9',
  ),
  'helvetica_neuethinitalic' => array(
    'normal' => DOMPDF_FONT_DIR . '9d2b427f233633dd3feeddd4946b670c',
  ),
  'helvetica_neueultraligcond' => array(
    'normal' => DOMPDF_FONT_DIR . '3e5f3b924005fe995c5cd0e94384b849',
  ),
  'helvetica_neueultraligcondobl' => array(
    'normal' => DOMPDF_FONT_DIR . '9e3bb6ca456afc6c261e1c07b0faf6e5',
  ),
  'helvetica_neueultraligext' => array(
    'normal' => DOMPDF_FONT_DIR . '04178580dbae16d954ff2e409d386493',
  ),
  'helvetica_neueultraligextobl' => array(
    'normal' => DOMPDF_FONT_DIR . 'e8beaf7779f813a3caa238f7caf34d44',
  ),
  'helvetica_neueultralight' => array(
    'normal' => DOMPDF_FONT_DIR . '000b49521685fa9d644165bc5879274c',
  ),
  'helvetica_neueultralightital' => array(
    'normal' => DOMPDF_FONT_DIR . '898e02697d318c56e5fe8320a37f1cfc',
  ),
) ?>