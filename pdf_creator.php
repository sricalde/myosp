<?php
	require_once("dompdf/dompdf_config.inc.php");
	$dompdf = new DOMPDF();
	ob_start();

	// get subscriber's info
	$vq_first_name = isset($_POST['vq_first_name']) ? trim($_POST['vq_first_name']) : '';
	$vq_last_name = isset($_POST['vq_last_name']) ? trim($_POST['vq_last_name']) : '';
	$vq_salutation = isset($_POST['vq_salutation']) ? trim($_POST['vq_salutation']) : '';
	$vq_gender = isset($_POST['vq_gender']) ? trim($_POST['vq_gender']) : '';
	$vq_dob = isset($_POST['vq_dob']) ? trim($_POST['vq_dob']) : '';
	$vq_email = isset($_POST['vq_email']) ? trim($_POST['vq_email']) : '';
	$vq_mobile = isset($_POST['vq_mobile']) ? trim($_POST['vq_mobile']) : '';
	$vq_phone = isset($_POST['vq_phone']) ? trim($_POST['vq_phone']) : '';
	$vq_nationality = isset($_POST['vq_nationality']) ? trim($_POST['vq_nationality']) : '';
	$vq_nric_passport = isset($_POST['vq_nric_passport']) ? trim($_POST['vq_nric_passport']) : '';
	$vq_address = isset($_POST['vq_address']) ? trim($_POST['vq_address']) : '';
	$vq_plan = isset($_POST['vq_plan']) ? trim($_POST['vq_plan']) : '';
	$vq_plan_name = isset($_POST['vq_plan_name']) ? trim($_POST['vq_plan_name']) : '';
	$vq_plan_desc = isset($_POST['vq_plan_desc']) ? trim($_POST['vq_plan_desc']) : '';
	$vq_subscription = isset($_POST['vq_subscription']) ? trim($_POST['vq_subscription']) : '';	$vq_price = isset($_POST['vq_price']) ? trim($_POST['vq_price']) : '';	$vq_speed = isset($_POST['vq_speed']) ? trim($_POST['vq_speed']) : '';	$vq_contract = isset($_POST['vq_contract']) ? trim($_POST['vq_contract']) : '';	$vq_freebies = isset($_POST['vq_freebies']) ? trim($_POST['vq_freebies']) : '';
	include('templates/pdf_template.php'); // default template (old version)
	$html_content = ob_get_contents();

	ob_end_clean();

	$vq_nric_passport = !empty($vq_nric_passport) ? $vq_nric_passport : 'PDF';
	$pdf_name = "VQ_".$vq_nric_passport;
	$dompdf->load_html($html_content);
	$dompdf->render();
	$pdf = $dompdf->output();
	$file_location = "temp/".$pdf_name.".pdf";
	file_put_contents($file_location,$pdf);
?>
