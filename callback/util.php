<?php session_start();
  $action = $_POST['action'];
  $name = $_POST['key'];
  if(isset($_POST['val']))
    $value = $_POST['val'];
  if(isset($action)){
    switch($action){
      case 'get':
        echo $_SESSION[$name];
        break;
      case 'set':
        $_SESSION[$name] = $value;
        break;
      case 'removeOne':
        unset($_SESSION[$name]);
        break;
      case 'removeAll':
        session_unset();
        break;
    }
  }
?>
