<!doctype html>
<html>
<head>
	<link href="../css/css-mobile-small.css" rel="stylesheet" type="text/css">
	<link href="../css/css-mobile.css" rel="stylesheet" type="text/css">
	<link href="../css/css-smartphone.css" rel="stylesheet" type="text/css">
	<link href="../css/css-tablet.css" rel="stylesheet" type="text/css">
	<link href="../css/css-notebook.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/css-desktop.css"/>
	<link rel="stylesheet" type="text/css" href="../fonts/fonts.css"/>
   
	<script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">
   <!--
   function Redirect()
   {
       window.location="https://mysignup.viewqwest.com/";
   }
   setTimeout('Redirect()', 2000);
   //-->
</script>
</head>
<body>
<div class="callback-wrapper">
	<div class="callback-container">
		<div class="callback-image"><img src="../images/success-img.png" width="auto" height="auto"></div>
        <div class="callback-message">
        		Email Sent Successfully.  Thank you for your feedback.
        </div>
	</div>
</div>
</body>
</html>
