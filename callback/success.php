<?php
session_start();
?>
<!DOCTYPE html>
<html>
   <head>
      <link href="../css/css-mobile-small.css" rel="stylesheet" type="text/css">
       <link href="../css/css-mobile.css" rel="stylesheet" type="text/css">
       <link href="../css/css-smartphone.css" rel="stylesheet" type="text/css">
       <link href="../css/css-tablet.css" rel="stylesheet" type="text/css">
       <link href="../css/css-notebook.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" type="text/css" href="../css/css-desktop.css"/>
      <link rel="stylesheet" type="text/css" href="../fonts/fonts.css"/>	   	   <script src="../jquery-1.12.0.js"></script>		<script src="../jquery.cookie.js"></script>        <script src="../util.js"></script>        <script src="../js/jquery-ui.min.js"></script>        <script src="../assets/icheck-1.x/icheck.js"></script>      
   </head>
   
   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
   new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
   })(window,document,'script','dataLayer','GTM-TF99FW');</script>
   <!-- End Google Tag Manager -->
   
   <body>
   
   	<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      
      <div class="callback-wrapper">
         <div class="callback-container">
             <div id="cc_payment_transaction" style="display: none;">
    Please Wait
</div>
            <div class="callback-image"><img src="../images/success-img.png" width="auto" height="auto"></div>
              <div class="callback-message">
               Welcome onboard! You have successfully signed up for ViewQwest's Fiber Broadband Services. 
                  Our staff will be getting in touch with you within 3 working days to confirm your installation date & time.
              </div>
              <div class="callback-message operatingHours">
               Operating Hours: 9am-6pm (Mon to Sat) <br>
                  Phone: <a href="tel:+60327750100">+60 3 2775 0100</a> <br>
                  <a href="mailto:sales.my@viewqwest.com">Email: sales.my@viewqwest.com</a>
              </div>
         </div>
      </div>
   </body>
</html>