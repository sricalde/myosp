<?php session_start(); ?>
<!DOCTYPE html>
<html>
   <head>
      <link href="../css/css-mobile-small.css" rel="stylesheet" type="text/css">
		<link href="../css/css-mobile.css" rel="stylesheet" type="text/css">
		<link href="../css/css-smartphone.css" rel="stylesheet" type="text/css">
		<link href="../css/css-tablet.css" rel="stylesheet" type="text/css">
		<link href="../css/css-notebook.css" rel="stylesheet" type="text/css">
		<link href="../css/css-desktop.css" rel="stylesheet" type="text/css">
      <link href="../fonts/fonts.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="../js/jquery-ui.min.css"/>
      <link href="../assets/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet" type="text/css"/>
      <link href="../assets/sweetalert-master/dist/sweetalert.css" rel="stylesheet" type="text/css"/>

      <script src="../jquery-1.12.0.js"></script>
		<script src="../jquery.cookie.js"></script>
		<script src="../util.js"></script>
		<script src="../js/jquery-ui.min.js"></script>
		<script src="../js/verify.notify.min.js"></script>
      <script src="../assets/sweetalert-master/dist/sweetalert.min.js"></script>
		<script src="../assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
   </head>

   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
   new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
   })(window,document,'script','dataLayer','GTM-TF99FW');</script>
   <!-- End Google Tag Manager -->

   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

      <div class="callback-wrapper">
         <div class="callback-container">
            <div class="callback-image"><img src="../images/failure-img.png" width="auto" height="auto"></div>
              <div class="callback-message">
              	<div>Oops! Something Unexpected seems to have happened. Please contact us so we can assist you!</div>
               <!-- FEEDBACK FORM -->
               <div class="feedbackFormWrapper">
                 <div class="feedbackFormButton feedbackFormButtonErrorCallback feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                 <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                    <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                 </div><!-- //feedbackFormContent -->
               </div><!-- //feedbackFormWrapper -->
              </div>
         </div>
      </div>
   </body>
   <!-- FEEDBACK FORM -->
    <script type="text/javascript">
		function feedbackFormSubmit(){
			$('#feedbackFormGeneric').verify({
				if (result) {
					var firstName = $('#firstName').val();
					var lastName = $('#lastName').val();
					var email = $('#email').val();
					var phone = $('#phone').val();
					var message = $('#message').val();
					var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
					$.ajax({
						url: "../feedbackFormEngine.php",
						data: param,
						type: "POST",
						success:(function(data) {
							swal({
									title: "Thank You!",
									text: "Email sent successfullly.",
									showConfirmButton: false,
									type: "success"
								});
								//window.location.href=window.location.href;
						}),
						complete:function(){
							window.location.href=window.location.href;
						}
					})
				}
			})
		}

		$("#feedbackFormContent").load("../feedbackForm_ErrorCallback.php");
		$('.feedbackFormLink').magnificPopup({
			type:'inline',
			midClick: true
		});
	</script>
</html>
