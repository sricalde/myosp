<?php

session_start();
require_once './config.php';
$account_response = array();
$nric = $_POST['nric'];


$loginurl = "https://login.salesforce.com/services/oauth2/token";
$params = "grant_type=password"
        . "&client_id=" . CLIENT_ID
        . "&client_secret=" . CLIENT_SECRET
        . "&username=" . SF_USERNAME
        . "&password=" . SF_PASSWORD . SF_TOKEN;
$curl = curl_init($loginurl);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ($status != 200) {
    die("Error: call to URL failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
}

curl_close($curl);


$response = json_decode($json_response, true);
if (isset($_REQUEST['action'])) {
    $access_token = base64_decode($_REQUEST['act']);
    $instance_url = base64_decode($_REQUEST['iu']);
} else {
    $access_token = $response['access_token'];
    $instance_url = $response['instance_url'];
}

if (!isset($access_token) || $access_token == "") {
    die("Error - access token missing from response!");
}

if (!isset($instance_url) || $instance_url == "") {
    die("Error - instance URL missing from response!");
}

$_SESSION['access_token'] = $access_token;
$_SESSION['instance_url'] = $instance_url;

//var_dump($response);
CreateSFAccount($access_token, $instance_url, $nric);

function CreateSFAccount($access_token, $instance_url, $nric) {
    $url = $instance_url . "/services/data/v20.0/sobjects/Account";
    $sf_FullName__c = $_SESSION['salutation'] . ' ' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
    $sf_Salutation = $_SESSION['salutation'];
    $sf_FirstName = $_SESSION['firstName'];
    $sf_LastName = $_SESSION['lastName'];
    $sf_Gender__c = $_SESSION['gender'];
    $sf_NRIC_Passport_FIN_No__pc =  $nric;
    // $sf_NRIC_Passport_FIN_No__pc = 'Test111dfsdsd4141';
    $sf_Phone = $_SESSION['phone'];
    $sf_PersonMobilePhone = $_SESSION['mobile'];
    $sf_PersonEmail = $_SESSION['email'];
    $sf_PersonBirthdate = $_SESSION['dob'];
    $sf_Nationality__c = 'Malaysian';
    $sf_Status__c = 'Active';
    $sf_RecordTypeId = '012900000003dcU';
    $sf_PersonMailingCountry = 'Malaysia';
    $sf_PersonMailingPostalCode = $_SESSION['postal'];
    $sf_PersonMailingStreet = $_SESSION['address1'];
    $sf_PersonMailingCity = $_SESSION['address2'];
	$sf_PersonOtherPostalCode = $_SESSION['postal'];
    $sf_PersonOtherStreet = $_SESSION['address1'];
    $sf_PersonOtherCity = $_SESSION['address2'];
    $sf_PersonOtherCountry = 'Malaysia';
    $sf_Signup_ID__c = $_SESSION['customerUID'];
    $sf_BillingAddress = $_SESSION['address1'] . ' ' . $_SESSION['address2'];
    $sf_CardholderName = $_SESSION['cardName'];
    $sf_CardNumber = $_SESSION['cardNum'];
    $sf_ExpMonth = $_SESSION['expMonth'];
    $sf_ExpYear = $_SESSION['expYear'];
	date_default_timezone_set('Asia/Singapore');
	$sf_date = date('Y-m-d');

    $content = json_encode(array(
        "FullName__c" => $sf_FullName__c,
        "Salutation" => $sf_Salutation,
        "FirstName" => $sf_FirstName,
        "LastName" => $sf_LastName,
        "Address__c" => $sf_BillingAddress,
        "Gender__c" => $sf_Gender__c,
        "Sign_up_Date__c" => $sf_date,
        "NRIC_Passport_FIN_No__pc" => $sf_NRIC_Passport_FIN_No__pc,
        // "NRIC_Passport_FIN_No__pc" => 'Test13',
        "Phone" => $sf_Phone,
        "PersonMobilePhone" => $sf_PersonMobilePhone,
        "PersonEmail" => $sf_PersonEmail,
        "PersonBirthdate" => $sf_PersonBirthdate,
        "Nationality__c" => $sf_Nationality__c,
        "Status__c" => $sf_Status__c,
        "RecordTypeId" => $sf_RecordTypeId,
        "PersonMailingPostalCode" => $sf_PersonMailingPostalCode,
        "PersonMailingStreet" => $sf_PersonMailingStreet,
        "PersonMailingCity" => $sf_PersonMailingCity,
        "PersonMailingCountry" => $sf_PersonMailingCountry,
		"PersonOtherPostalCode" => $sf_PersonOtherPostalCode,
        "PersonOtherStreet" => $sf_PersonOtherStreet,
        "PersonOtherCity" => $sf_PersonOtherCity,
        "PersonOtherCountry" => $sf_PersonOtherCountry,
        "Signup_ID__c" => $sf_Signup_ID__c,
		"Cardholder_Name__c" => $sf_CardholderName,
		"Card_Number__c" => $sf_CardNumber,
		"Expiration_Month_MM__c" => $sf_ExpMonth,
		"Expiration_Year_YYYY__c" => $sf_ExpYear,
		"Country_of_Residence__c" => "Malaysia",
		"Entity__c" => "VQSB"
    ));

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json"
    ));
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
    $json = json_decode($json_response, true);

    $crm_id = $json['id'];

	if($crm_id == NULL || $crm_id == null || $crm_id == "") {
		$account_response = array('responseCode' => 'failed');
		
	} else {
		$account_response = array('responseCode' => 'success');
		CreateZAccount($crm_id);
	}
	echo json_encode($account_response);
 
}

function CreateZAccount($crm_id) {
    $icPath = 'nricUpload';
    $passportPath = 'passportUpload';
    $ccPath = 'creditCardUpload';
	$zr_url = "https://api.zuora.com/rest/v1/accounts";
    $username = USERNAME;
    $password = PASSWORD;
    $ratePlanId = $_SESSION['productRatePlanID'];
    $installationCharges = $_SESSION['installationCharges'];
    $routerBundle = $_SESSION['routerBundle'];

    $zr_content = '{
  "name":"' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'] . '",
  "currency":"MYR",
  "notes":"MY Account",
  "billCycleDay":1,
  "crmId":"' . $crm_id . '",
  "autoPay":"false",
  "Entity__c":"VQSB",
  "paymentTerm":"Net 7 Days",
  "billToContact":{
    "address1":"' . $_SESSION['address1'] . '",
    "address2":"' . $_SESSION['address2'] . '",
    "city":"",
    "country":"Malaysia",
    "county":"",
    "fax":"",
    "firstName":"' . $_SESSION['firstName'] . '",
    "homePhone":"",
    "lastName":"' . $_SESSION['lastName'] . '",
    "mobilePhone":"' . $_SESSION['phone'] . '",
    "nickname":"",
    "otherPhone":"",
    "personalEmail":"' . $_SESSION['email'] . '",
    "zipCode":"",
    "state":"",
    "workEmail":"' . $_SESSION['email'] . '",
    "workPhone":"' . $_SESSION['phone'] . '"
  },
  "soldToContact":{
    "address1":"' . $_SESSION['address1'] . '",
    "address2":"' . $_SESSION['address2'] . '",
    "city":"",
    "country":"Malaysia",
    "county":"",
    "fax":"",
    "firstName":"' . $_SESSION['firstName'] . '",
    "homePhone":"",
    "lastName":"' . $_SESSION['lastName'] . '",
    "mobilePhone":"' . $_SESSION['phone'] . '",
    "nickname":"",
    "otherPhone":"",
    "personalEmail":"",
    "zipCode":"",
    "state":"",
    "workEmail":"' . $_SESSION['email'] . '",
    "workPhone":""
  },
 "hpmCreditCardPaymentMethodId": "4028e488361ddbfc01362c21e9033aca",
  "invoiceCollect": false

}
';

    $zr_curl = curl_init($zr_url);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($zr_curl, CURLOPT_HEADER, false);
    curl_setopt($zr_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept:application/json', 'Content-Length: ' . strlen($zr_content)));
    curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($zr_curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($zr_curl, CURLOPT_POSTFIELDS, $zr_content);
    curl_setopt($zr_curl, CURLOPT_USERPWD, "$username:$password");

    $json_response = curl_exec($zr_curl);
    curl_close($zr_curl);
    $json = json_decode($json_response);
    foreach ($json as $name => $value) {
        if ($name == 'accountId') {
            $account_id = $value;
        }
        else if ($name == 'accountNumber') {
            $account_number = $value;
        }
    }
	// var_dump($json);
    CreateSubscription($account_id, $ratePlanId, $routerBundle);
    
    $checkUpload = $_SESSION['nationality'];
    $ccFilename = $_SESSION['creditCardFileName1'];
    if($checkUpload == 'Foreigner'){
        $passportFilename = $_SESSION['passportFileName1'];

        UploadImage($crm_id, $passportPath, $passportFilename);
        // UploadImage($crm_id, $ccPath, $ccFilename);
		SF_SAF_Attachment_File($crm_id); 
		// SF_CC_Attachment_Field($crm_id);
    }
    else{
        $icFrontFilename = $_SESSION['nricFileName1'];
        $icBackFilename = $_SESSION['nricFileName2'];
        UploadImage($crm_id, $icPath, $icFrontFilename);
        UploadImage($crm_id, $icPath, $icBackFilename);
        // UploadImage($crm_id, $ccPath, $ccFilename);
		SF_SAF_Attachment_File($crm_id); 
		// SF_CC_Attachment_Field($crm_id);
    }

	require_once 'accountCreationEmail.php';
	
	if (!$mail->send()) {
		// $account_response = array('responseCode' => '1');
		// var_dump($account_response);
	} else {
		// $account_response = array('responseCode' => '2');
		// var_dump($account_response);
	}
    //AddCCRefPayment($account_id);
}

function AddCCRefPayment($account_id) {
    $zr_payment_token = "https://rest.zuora.com/v1/action/create";

    $username = USERNAME;
    $password = PASSWORD;
    $payment_token = $_SESSION['payment_token'];
    echo $payment_token;
//$payment_token = '5032837188546780803524';
    $zr_content = '{
   "objects": [
    {
      "AccountId": "' . $account_id . '",
      "TokenId": "' . $payment_token . '",
      "Type": "CreditCardReferenceTransaction"
    }
  ],
  "type": "PaymentMethod"
}';

    $zr_curl = curl_init($zr_payment_token);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($zr_curl, CURLOPT_HEADER, false);
    curl_setopt($zr_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept:application/json', 'Content-Length: ' . strlen($zr_content)));
    curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($zr_curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($zr_curl, CURLOPT_POSTFIELDS, $zr_content);
    curl_setopt($zr_curl, CURLOPT_USERPWD, "$username:$password");

    curl_exec($zr_curl);
    curl_close($zr_curl);

//var_dump($json_response);
    echo '<br/>';
    echo '<br/>';
}

function UpdatePaymentToken($old_token) {
    
}

function CreateSubscription($account_id, $ratePlanId, $routerBundle){
	$zr_payment_token = "https://rest.zuora.com/v1/action/subscribe";

	$username = USERNAME;
    $password = PASSWORD;
    $zr_content = '{
  subscribes":[
      {
         "Account":{
            "Id":"'.$account_id.'"
         },
         "SubscribeOptions":{
            "GenerateInvoice":false,
            "ProcessPayments":false
         },
         "SubscriptionData":{
            "RatePlanData":[
               {
                  "RatePlan":{
                     "ProductRatePlanId":"'.$ratePlanId.'"
                  }
               },
               {
                  "RatePlan":{
                     "ProductRatePlanId":"'.$routerBundle.'"
                  }
               }
            ],
            "Subscription":{
               "TermType":"EVERGREEN"
            }
         }
      }
   " ]
}';

    $zr_curl = curl_init($zr_payment_token);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($zr_curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($zr_curl, CURLOPT_HEADER, false);
    curl_setopt($zr_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept:application/json', 'Content-Length: ' . strlen($zr_content)));
    curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($zr_curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($zr_curl, CURLOPT_POSTFIELDS, $zr_content);
    curl_setopt($zr_curl, CURLOPT_USERPWD, "$username:$password");

    $json_response = curl_exec($zr_curl);
    curl_close($zr_curl);
    // $json = json_decode($json_response);
	// foreach ($json as $name => $value) {
        // if ($name == 'accountId') {
            // $account_id = $value;
        // }
    // }
    // var_dump($json);
    //AddCCRefPayment($account_id);
}

function UploadImage($crm_id, $path, $filename) {
	
	$access_token = $_SESSION['access_token'];
	$instance_url = $_SESSION['instance_url'];
	$sf_attachment_name = $filename;
	$path = '../'.$path.'/'.$sf_attachment_name;
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data =  base64_encode(file_get_contents($path));
	
	$url = $instance_url . "/services/data/v20.0/sobjects/Attachment";
	$sf_Account_ID__c = $crm_id;
	
	$content = json_encode(array(
		
			"Name" =>	$sf_attachment_name,
			"Body"	=> $data,
			"ParentId" => $sf_Account_ID__c,
			"ContentType" =>  "image/".$type,
		)
	);
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json",
		"Content-Length: " . strlen($content)
    ));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
	
}
function SF_CC_Attachment_Field($crm_id) {
	
	$access_token = $_SESSION['access_token'];
	$instance_url = $_SESSION['instance_url'];
	$url = $instance_url . "/services/data/v20.0/sobjects/AttachmentC__c";
	$sf_Account_ID__c = $crm_id;
	
	$content = json_encode(array(
		
			"Name" =>	"Credit Card Front Image",
			"Account_Name__c" => $sf_Account_ID__c,
			"Description__c" => "For billing purpose.",
		)
	);
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json",
		"Content-Length: " . strlen($content)
    ));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
	$json = json_decode($json_response, true);
	$attachment_id = $json['id'];
	
	SF_CC_Attachment_File($attachment_id);
}
function SF_CC_Attachment_File($attachment_id) {
	
	$access_token = $_SESSION['access_token'];
	$instance_url = $_SESSION['instance_url'];
	$sf_attachment_name = $_SESSION['creditCardFileName1'];
	$path = '../creditCardUpload/'.$sf_attachment_name;
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data =  base64_encode(file_get_contents($path));
	
	$url = $instance_url . "/services/data/v20.0/sobjects/Attachment";
	$sf_Account_ID__c = $attachment_id;
	
	$content = json_encode(array(
		
			"Name" =>	$sf_attachment_name,
			"Body"	=> $data,
			"ParentId" => $sf_Account_ID__c,
			"ContentType" =>  "image/".$type,
			
	));
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json",
		"Content-Length: " . strlen($content)
    ));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
	
}
function SF_SAF_Attachment_File($crm_id) {
	
	$access_token = $_SESSION['access_token'];
	$instance_url = $_SESSION['instance_url'];
	$nric = $_SESSION['nric'];
	$pdf_filename = 'VQ_'.$nric.'.pdf'; // for generic file name (added: 2016/02/12)
	$path = '../temp/'.$pdf_filename;
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data =  base64_encode(file_get_contents($path));

	
	$url = $instance_url . "/services/data/v20.0/sobjects/Attachment";
	$sf_Account_ID__c = $crm_id;
	
	$content = json_encode(array(
		
			"Name" =>	$pdf_filename,
			"Body"	=> $data,
			"ParentId" => $sf_Account_ID__c,
			"ContentType" =>  "application/".$type,
			
	));
	$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json",
		"Content-Length: " . strlen($content)
    ));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
	
}
?>

