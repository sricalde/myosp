<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
    	<script src="SpryAssets/SpryDOMUtils.js" type="text/javascript"></script>
        <script src="js/custom/registration.js" type="text/javascript"></script>
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>
    	<style>
			@media only screen and (min-width: 481px) and (max-width: 640px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -115px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -80px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(6):before {
				width: 60px;
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -38px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				background: #F15757;
			}
			#pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt7{
				display: none;
			}
			}
			@media only screen and (min-width: 321px) and (max-width: 480px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -175px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -80px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(6):before {
				width: 60px;
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -38px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				background: #F15757;
			}
			li#pb7{
				width: 0px;
			}
			li#pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt7{
				display: none;
			}
			}
			@media only screen and (max-width: 320px){
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -185px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -80px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(5):before {
				width: 60px;
				content: ". . .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -32px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				background: #F15757;
			}
			li#pb6 {
				width: 0px;
			}
			#pb7, #pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt5, #pbtxt6{
				display: none;
			}
			}
		</style>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-NFJJ3B4');</script>
      <!-- End Google Tag Manager -->

      <script>
			$.ajax({
			url:'UIDGenerator.php',
			type:'POST',
			data:"json",
		   	success:function(data)
		   	{
					cusUID = data;
					$("#customerUID").val(cusUID);
					setVqValue('customerUID', $("#customerUID").val());
					console.log(getVqValue('customerUID'));
				}
			});
		</script>

    </head>
    <body>
		<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFJJ3B4"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

      <!--
      Start of DoubleClick Floodlight Tag: Please do not remove
      Activity name of this tag: Signup Portal - Reg page testing
      URL of the webpage where the tag is expected to be placed: https://signup.viewqwest.com/registration.html
      This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
      Creation Date: 01/10/2017
      -->
      <script type="text/javascript">
			var axel = Math.random() + "";
			var a = axel * 10000000000000;
			document.write('<iframe src="https://5691183.fls.doubleclick.net/activityi;src=5691183;type=resid002;cat=signu008;u2=[' + customerUID + '];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
      </script>
      <noscript>
      	<iframe src="https://5691183.fls.doubleclick.net/activityi;src=5691183;type=resid002;cat=signu008;u2=[" + customerUID + "];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
      </noscript>
      <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

    	<div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
                
                <li id="pb3" class="active"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">PAYMENT</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
    	<div class="wrapper">
        	<div class="registrationWrapper">
                <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <h1 class="registration-h1">SUBSCRIBER'S DETAILS</h1>
                <div style="display:none;"><input type="text" id="customerUID" placeholder="Customer UID" size="50" value=""/></div>
                <div class="registration-left-panel">
                    <!--<h4 class="registration-h4">Please fill in your particulars</h4>-->
                    <div>
                    <select class="registration-select" title="Salutation" id="name_salutationacc2" style="-moz-appearance:none;">
                        <option value="Mr.">Mr.</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Mrs.">Mrs.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="Prof.">Prof.</option>
                    </select>
                    </div>
                    <div>
                    <input class="registration-input-type" type="text" id="firstName" placeholder="First Name" size="50"/>
                    <input class="registration-input-type" type="text" id="lastName" placeholder="Last Name" size="50"/>
                    <input class="registration-input-type" type="text" id="nric" placeholder="NRIC/Passport" size="50"/>
                    <input  class="registration-input-type" type="text" id="dob" placeholder="Date of Birth (YYYY-MM-DD)" size="50"/>
    </div>
                    <div>
                    <select class="registration-select" id="nationality" style="-moz-appearance:none;">
                        <option value="Singaporean">Singaporean</option>
                        <option value="Afghan">Afghan</option>
                        <option value="Albanian">Albanian</option>
                        <option value="Algerian">Algerian</option>
                        <option value="American">American</option>
                        <option value="Andorran">Andorran</option>
                        <option value="Angolan">Angolan</option>
                        <option value="Antiguans">Antiguans</option>
                        <option value="Argentinean">Argentinean</option>
                        <option value="Armenian">Armenian</option>
                        <option value="Australian">Australian</option>
                        <option value="Austrian">Austrian</option>
                        <option value="Azerbaijani">Azerbaijani</option>
                        <option value="Bahamian">Bahamian</option>
                        <option value="Bahraini">Bahraini</option>
                        <option value="Bangladeshi">Bangladeshi</option>
                        <option value="Barbadian">Barbadian</option>
                        <option value="Barbudans">Barbudans</option>
                        <option value="Batswana">Batswana</option>
                        <option value="Belarusian">Belarusian</option>
                        <option value="Belgian">Belgian</option>
                        <option value="Belizean">Belizean</option>
                        <option value="Beninese">Beninese</option>
                        <option value="Bhutanese">Bhutanese</option>
                        <option value="Bolivian">Bolivian</option>
                        <option value="Bosnian">Bosnian</option>
                        <option value="Brazilian">Brazilian</option>
                        <option value="British">British</option>
                        <option value="Bruneian">Bruneian</option>
                        <option value="Bulgarian">Bulgarian</option>
                        <option value="Burkinabe">Burkinabe</option>
                        <option value="Burmese">Burmese</option>
                        <option value="Burundian">Burundian</option>
                        <option value="Cambodian">Cambodian</option>
                        <option value="Cameroonian">Cameroonian</option>
                        <option value="Canadian">Canadian</option>
                        <option value="Cape Verdean">Cape Verdean</option>
                        <option value="Central African">Central African</option>
                        <option value="Chadian">Chadian</option>
                        <option value="Chilean">Chilean</option>
                        <option value="Chinese">Chinese</option>
                        <option value="Colombian">Colombian</option>
                        <option value="Comoran">Comoran</option>
                        <option value="Congolese">Congolese</option>
                        <option value="Costa Rican">Costa Rican</option>
                        <option value="Croatian">Croatian</option>
                        <option value="Cuban">Cuban</option>
                        <option value="Cypriot">Cypriot</option>
                        <option value="Czech">Czech</option>
                        <option value="Danish">Danish</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominican">Dominican</option>
                        <option value="Dutch">Dutch</option>
                        <option value="East Timorese">East Timorese</option>
                        <option value="Ecuadorean">Ecuadorean</option>
                        <option value="Egyptian">Egyptian</option>
                        <option value="Emirian">Emirian</option>
                        <option value="Equatorial Guinean">Equatorial Guinean</option>
                        <option value="Eritrean">Eritrean</option>
                        <option value="Estonian">Estonian</option>
                        <option value="Ethiopian">Ethiopian</option>
                        <option value="Fijian">Fijian</option>
                        <option value="Filipino">Filipino</option>
                        <option value="Finnish">Finnish</option>
                        <option value="French">French</option>
                        <option value="Gabonese">Gabonese</option>
                        <option value="Gambian">Gambian</option>
                        <option value="Georgian">Georgian</option>
                        <option value="German">German</option>
                        <option value="Ghanaian">Ghanaian</option>
                        <option value="Greek">Greek</option>
                        <option value="Grenadian">Grenadian</option>
                        <option value="Guatemalan">Guatemalan</option>
                        <option value="Guinea-Bissauan">Guinea-Bissauan</option>
                        <option value="Guinean">Guinean</option>
                        <option value="Guyanese">Guyanese</option>
                        <option value="Haitian">Haitian</option>
                        <option value="Herzegovinian">Herzegovinian</option>
                        <option value="Honduran">Honduran</option>
                        <option value="Hungarian">Hungarian</option>
                        <option value="I-Kiribati">I-Kiribati</option>
                        <option value="Icelander">Icelander</option>
                        <option value="Indian">Indian</option>
                        <option value="Indonesian">Indonesian</option>
                        <option value="Iranian">Iranian</option>
                        <option value="Iraqi">Iraqi</option>
                        <option value="Irish">Irish</option>
                        <option value="Israeli">Israeli</option>
                        <option value="Italian">Italian</option>
                        <option value="Ivorian">Ivorian</option>
                        <option value="Jamaican">Jamaican</option>
                        <option value="Japanese">Japanese</option>
                        <option value="Jordanian">Jordanian</option>
                        <option value="Kazakhstani">Kazakhstani</option>
                        <option value="Kenyan">Kenyan</option>
                        <option value="Kittian and Nevisian">Kittian and Nevisian</option>
                        <option value="Kuwaiti">Kuwaiti</option>
                        <option value="Kyrgyz">Kyrgyz</option>
                        <option value="Laotian">Laotian</option>
                        <option value="Latvian">Latvian</option>
                        <option value="Lebanese">Lebanese</option>
                        <option value="Liberian">Liberian</option>
                        <option value="Libyan">Libyan</option>
                        <option value="Liechtensteiner">Liechtensteiner</option>
                        <option value="Lithuanian">Lithuanian</option>
                        <option value="Luxembourger">Luxembourger</option>
                        <option value="Macedonian">Macedonian</option>
                        <option value="Malagasy">Malagasy</option>
                        <option value="Malawian">Malawian</option>
                        <option value="Malaysian">Malaysian</option>
                        <option value="Maldivan">Maldivan</option>
                        <option value="Malian">Malian</option>
                        <option value="Maltese">Maltese</option>
                        <option value="Marshallese">Marshallese</option>
                        <option value="Mauritanian">Mauritanian</option>
                        <option value="Mauritian">Mauritian</option>
                        <option value="Mexican">Mexican</option>
                        <option value="Micronesian">Micronesian</option>
                        <option value="Moldovan">Moldovan</option>
                        <option value="Monacan">Monacan</option>
                        <option value="Mongolian">Mongolian</option>
                        <option value="Moroccan">Moroccan</option>
                        <option value="Mosotho">Mosotho</option>
                        <option value="Motswana">Motswana</option>
                        <option value="Mozambican">Mozambican</option>
                        <option value="Namibian">Namibian</option>
                        <option value="Nauruan">Nauruan</option>
                        <option value="Nepalese">Nepalese</option>
                        <option value="New Zealander">New Zealander</option>
                        <option value="Nicaraguan">Nicaraguan</option>
                        <option value="Nigerian">Nigerian</option>
                        <option value="Nigerien">Nigerien</option>
                        <option value="North Korean">North Korean</option>
                        <option value="Northern Irish">Northern Irish</option>
                        <option value="Norwegian">Norwegian</option>
                        <option value="Omani">Omani</option>
                        <option value="Pakistani">Pakistani</option>
                        <option value="Palauan">Palauan</option>
                        <option value="Panamanian">Panamanian</option>
                        <option value="Papua New Guinean">Papua New Guinean</option>
                        <option value="Paraguayan">Paraguayan</option>
                        <option value="Peruvian">Peruvian</option>
                        <option value="Polish">Polish</option>
                        <option value="Portuguese">Portuguese</option>
                        <option value="Qatari">Qatari</option>
                        <option value="Romanian">Romanian</option>
                        <option value="Russian">Russian</option>
                        <option value="Rwandan">Rwandan</option>
                        <option value="Saint Lucian">Saint Lucian</option>
                        <option value="Salvadoran">Salvadoran</option>
                        <option value="Samoan">Samoan</option>
                        <option value="San Marinese">San Marinese</option>
                        <option value="Sao Tomean">Sao Tomean</option>
                        <option value="Saudi">Saudi</option>
                        <option value="Scottish">Scottish</option>
                        <option value="Senegalese">Senegalese</option>
                        <option value="Serbian">Serbian</option>
                        <option value="Seychellois">Seychellois</option>
                        <option value="Sierra Leonean">Sierra Leonean</option>
                        <option value="Slovakian">Slovakian</option>
                        <option value="Slovenian">Slovenian</option>
                        <option value="Solomon Islander">Solomon Islander</option>
                        <option value="Somali">Somali</option>
                        <option value="South African">South African</option>
                        <option value="South Korean">South Korean</option>
                        <option value="Spanish">Spanish</option>
                        <option value="Sri Lankan">Sri Lankan</option>
                        <option value="Sudanese">Sudanese</option>
                        <option value="Surinamer">Surinamer</option>
                        <option value="Swazi">Swazi</option>
                        <option value="Swedish">Swedish</option>
                        <option value="Swiss">Swiss</option>
                        <option value="Syrian">Syrian</option>
                        <option value="Taiwanese">Taiwanese</option>
                        <option value="Tajik">Tajik</option>
                        <option value="Tanzanian">Tanzanian</option>
                        <option value="Thai">Thai</option>
                        <option value="Togolese">Togolese</option>
                        <option value="Tongan">Tongan</option>
                        <option value="Trinidadian or Tobagonian">Trinidadian or Tobagonian</option>
                        <option value="Tunisian">Tunisian</option>
                        <option value="Turkish">Turkish</option>
                        <option value="Tuvaluan">Tuvaluan</option>
                        <option value="Ugandan">Ugandan</option>
                        <option value="Ukrainian">Ukrainian</option>
                        <option value="Uruguayan">Uruguayan</option>
                        <option value="Uzbekistani">Uzbekistani</option>
                        <option value="Venezuelan">Venezuelan</option>
                        <option value="Vietnamese">Vietnamese</option>
                        <option value="Welsh">Welsh</option>
                        <option value="Yemenite">Yemenite</option>
                        <option value="Zambian">Zambian</option>
                        <option value="Zimbabwean">Zimbabwean</option>
                    </select>
                    </div>
                    <!--
                    <div>
                        <input class="registration-input-type" type="text" id="referralNric" placeholder="Referral NRIC" size="50"/>
                    </div>
                    -->
                    <div class="registration-gender-input">
                        <div class="registration-gender-input-header">Gender:</div>
                        <div class="registration-input-radio">
                            <input checked="" type="radio" name="gender" value="Male"/> Male
                        </div>
                        <div class="registration-input-radio">
                            <input class="registration-input-radio" type="radio" name="gender" value="Female"/> Female
                        </div>
                    </div>
                    <div id="results" style="vertical-align: top; padding-left: 100px;"></div>
                </div><!-- registration-left-panel -->

                <div class="registration-right-panel">
                    <div>
                        <select class="registration-select" id="reType" disabled="" style="-moz-appearance:none;">
                            <option value="COM">Commercial High Rise</option>
                            <option value="COL">Commercial Low Rise</option>
                            <option value="IND">Industrial</option>
                            <option value="LND">Landed</option>
                            <option value="HDB">HDB</option>
                            <option value="APT">Condominium</option>
                            <option value="OTH">Other</option>
                            <option value="GOV">Government</option>
                            <option value="RES">Residential shop house</option>
                            <option value="SCH">School</option>
                            <option value="EXCHANGE">Exchange</option>
                        </select>
                    </div>
                    <div><input class="registration-input-type" type="text" id="address" placeholder="Address" size="50" readonly/></div>
                    <div><input class="registration-input-type" type="text" id="postal" placeholder="Postal Code" size="50" readonly/></div>
                    <div><input class="registration-input-type" type="text" id="phone" placeholder="Phone Number" size="50"/></div>
                    <div><input class="registration-input-type" type="text" id="mobile" placeholder="Mobile Number" size="50"/></div>
                    <div><input class="registration-input-type" type="text" id="email" placeholder="Email Address" size="50"/></div>
                    <div class="registration-button"><input type="button" id="prev" value="Previous" onclick="gotoFeasibilityCheck();"/></div>
                    <div class="registration-button"><input type="button" id="next" value="Next" onclick="gotoPlanDetails();"/></div>
                </div><!-- registration-right-panel -->
        	</div><!-- //registrationWrapper -->
        </div><!-- //wrapper -->
    </body>
    <script>
		$(document).ready(function(){
		  $('input').iCheck({
			checkboxClass: 'icheckbox_flat-red',
			radioClass: 'iradio_flat-red'
		  });
		});
	</script>
</html>
