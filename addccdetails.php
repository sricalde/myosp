<?php include 'security.php';
 session_start();
?>

<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/pageLoaderLoadingBar.css" rel="stylesheet" type="text/css" />
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script type="text/javascript" src="backfix.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/custom/payment.js" type="text/javascript"></script>
        <script src="js/pageLoader.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>

        <style type="text/css">
            @media only screen and (max-width: 768px){
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -370px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -285px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -235px;
                }
                #progressbar li:nth-child(3):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -185px;
                }
                #progressbar li:nth-child(4):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -133px;
                }
                #progressbar li:nth-child(5):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -80px;
                }
                #progressbar li:nth-child(6):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    content: none;
                }
                #progressbar li:nth-child(7):after {
                    content: none;
                }
                #progressbar li:nth-child(8):after {
                    background: #F15757;
                }
                #pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4, #pbtxt5, #pbtxt6, #pbtxt7{
                    display: none;
                }
            }
        </style>

        <!-- Zuora Public javascript library -->
        <script type="text/javascript" src="https://static.zuora.com/Resources/libs/hosted/1.1.0/zuora-min.js"/></script>


</head>

<body>
    <div class="top-border"></div>
    <div class="signupFlow">
        <ul id="progressbar">
            <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>

            <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
            <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
            <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
            <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
            <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
            <li id="pb8" class="active"><span id="pbtxt8">PAYMENT</span></li>
            <div id="progressbarConnector"></div>
        </ul>
    </div><!-- //signupFlow -->

    <div class="wrapper">
        <div class="signupWrapper">
                            <div class="vq-logo">
                    <img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto">
                </div>
            <div class="pageHeader">Residential Broadband Online Signup</div>
            <hr id="top-hr">
<form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/silent/pay" method="post"/>
<!--<form id="payment_confirmation" action=" https://testsecureacceptance.cybersource.com/silent/token/create" method="post"/>-->
<!--<form id="payment_confirmation" action=" https://secureacceptance.cybersource.com/silent/token/update" method="post"/>-->
    <?php
    foreach($_REQUEST as $name => $value) {
        $params[$name] = $value;
    }
?>
<fieldset id="confirmation" style="float: left; display: block; width: 40%;">
    <h4>Billing Information</h4>
    <div id="card_details">
        <?php
            foreach($params as $name => $value) {
                echo "<div>";
                if($name == 'access_key'){
                     echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
                }
                elseif ($name=='profile_id') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
            elseif ($name=='transaction_uuid') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
            elseif ($name=='signed_field_names') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='unsigned_field_names') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='signed_date_time') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='locale') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='transaction_type') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='reference_number') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='currency') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='payment_method') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
             elseif ($name=='submit') {
                  echo "<span style='display: none;' class=\"fieldName\">" . $name . "</span><span style='display: none;' class=\"fieldValue\">" . $value . "</span>";
            }
            else{
                echo "<span class=\"fieldName\">" . $name . "</span><span class=\"fieldValue\">" . $value . "</span>";
                echo "</div>\n";
            }
            }
        ?>
    </div>
</fieldset>
    <?php
        foreach($params as $name => $value) {
            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
        }
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
    ?>
<div id="UnsignedDataSection" class="section" style="float: left; display: block; width: 40%;">
            <h4>Card Details</h4>
            <span>Card Type:</span><input type="text" name="card_type"><br/>
        <span>Card Number:</span><input type="text" name="card_number"><br/>
        <span>Card Expiration Date:</span><input type="text" name="card_expiry_date"><br/>
	</div>
<br>
<br>
<div style="width: 100%; float: left; display: block;">
  <input type="submit" id="submit" value="Confirm"/>
</div>
  <script type="text/javascript" src="jquery-1.7.min.js"></script>
  <script type="text/javascript" src="payment_form.js"></script>

</form>
        </div>
    </div>
</body>
</html>
<style type="text/css">
    #card_details span, #UnsignedDataSection span{
        font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
        color: #fff;
    }
    #UnsignedDataSection input{
        margin: 4px;
        vertical-align: middle;
        -moz-border-radius: 4px 4px 4px 4px;
        -webkit-border-radius: 4px 4px 4px 4px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
        height: 28px;
        font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
        color: #000;
    }
    fieldset{
        border: none;
    }

</style>