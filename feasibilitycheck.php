<?php session_start(); ?>

<!DOCTYPE html>

<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="assets/jquery.qtip.custom/jquery.qtip.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
		  <script src="SpryAssets/SpryDOMUtils.js" type="text/javascript"></script>
        <script src="js/custom/feasibilitycheck.js" type="text/javascript"></script>
        <script src="js/verify.notify.min.js"></script>

		  <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/jquery.qtip.custom/jquery.qtip.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <style>
			@media only screen and (min-width: 481px) and (max-width: 640px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -80px;
			}
			#progressbar li:nth-child(1):before {
				width: 60px;
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -45px;
			}
			#progressbar li:nth-child(5):before {
				width: 60px;
				content: ". . .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -25px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				background: #F15757;
			}
			#pb7, #pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt5, #pbtxt6{
				display: none;
			}
			}
			@media only screen and (min-width: 321px) and (max-width: 480px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -80px;
			}
			#progressbar li:nth-child(1):before {
				width: 60px;
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -45px;
			}
			#progressbar li:nth-child(5):before {
				width: 60px;
				content: ". . .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -25px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				background: #F15757;
			}
			#pb7, #pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt5, #pbtxt6{
				display: none;
			}
			}
			@media only screen and (max-width: 320px){
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -90px;
			}
			#progressbar li:nth-child(1):before {
				width: 60px;
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -45px;
			}
			#progressbar li:nth-child(4):before {
				width: 65px;
				content: ". . . .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -20px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				background: #F15757;
			}
			#pb5, #pb6, #pb7, #pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt4{
				display: none;
			}
			}
		</style>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TF99FW');</script>
		<!-- End Google Tag Manager -->

    </head>
    <body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

    	<div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
            	<li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
                <li id="pb2" class="active"><span id="pbtxt2">FEASIBILITY CHECK</span></li>
                <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">PAYMENT</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
        <div class="wrapper">
        	<div class="feasibilityWrapper">
                <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <h1 class="feasibility-h1">FEASIBILITY CHECK</h1>
                <h4 class="feasibility-check-h4">Use the form below to check if your place is Fiber-Ready.</h4>

                <div class="feasibility-check-left-panel">
                    <div class="postal-input"><input class="postal-input-type" type="text" id="postal" placeholder="Postal Code"></div>
                    <div class="unit-floor-input-wrapper">
                        <div class="unit-floor-input"><span style="color:#FFF">#</span> <input class="unit-floor-input-type" type="text" id="floor" placeholder="Floor Level (2 Digits)"></div>
                        <div class="unit-number-input"><span style="color:#FFF"> - </span><input class="unit-floor-input-type" type="text" id="unit" placeholder="Unit Number"></div>
                    </div>
                    <div class="landed-checkbox" id="landedChecked">
                        <input type="checkbox" id="landed" onchange="landedProp()"><span id="checkbox-landed-text">Tick here for landed premises</span><br>
    </div>
                    <input id="check-coverage-button" type="button" value="Check for Coverage" onclick="checkCoverage();">
                    <div id="errorMsg" style="color:red"></div>
                </div><!-- //feasibility-check-left-panel -->

                <div id="results" class="feasibility-check-right-panel-wrapper">
                <div class="feasibility-check-right-panel">
                    <div class="results-wrapper results-wrapper-type">
                        <div class="results-title">Type:</div>
                        <div class="results-text" id="reType"></div> 
                    </div>
                    <div class="results-wrapper results-wrapper-block">
                        <div class="results-title">Block:</div>
                        <div class="results-text" id="reBlock"></div>
                    </div>
                    <div class="results-wrapper results-wrapper-bname">
                        <div class="results-title">Building Name:</div>
                        <div class="results-text" id="reBuilding"></div>
                    </div>
                    <div class="results-wrapper results-wrapper-street">
                        <div class="results-title">Street:</div>
                        <div class="results-text" id="reStreet"></div>
                    </div>
                    <div class="results-wrapper results-wrapper-unit">
                        <div class="results-title">Unit:</div>
                        <div class="results-text" id="reUnit"></div>
                    </div>
                    <div class="results-wrapper results-wrapper-postal">
                        <div class="results-title">Postal:</div>
                        <div class="results-text" id="rePostal"></div>
                    </div>
                    <div class="results-wrapper results-wrapper-status">
                        <div class="results-title">Status:
                            <div class="qMark ui-content">
                                <a id="statusToolTip" href="#">?</a>
                                <div class="statusToolTip" style="line-height:normal;">
                                    If you already have a Fiber Termination Point (FTP) in your home, no charges will be applied.
                                    If you do not have an FTP in your home and your home is deemed as "home-passed" by Net Link Trust a one-time charge of $235.40 for High-Rise building or $481.50
                                    for Landed Property will apply. These charges are set by Net Link Trust and are inclusive of GST.
                                    <br><br>
                                    A ViewQwest customer service officer will inform you of any applicable charges upon contacting you to confirm the installation date/time.
                                </div>
                            </div>
                        </div>
                        <div class="results-text" id="reStatus"></div>
                    </div>
                </div><!-- //feasibility-check-right-panel -->
                    <input class="results-next-button" type="button" id="next" value="Next" onclick="gotoRegistration();">
                    <div class="results-message" style="color:red" id="noCoverageMsg">Your place is not Fiber-Ready, please email your queries to sales@viewqwest.com.</div>
                </div><!-- //feasibility-check-right-panel-wrapper -->

					<!-- FEEDBACK FORM -->
               <div class="feedbackFormWrapper">
                 <div class="feedbackFormDivider"></div>
                 <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                 <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                 <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                    <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                 </div><!-- //feedbackFormContent -->
               </div><!-- //feedbackFormWrapper -->

       		</div><!-- //feasibilityWrapper -->
        </div><!-- //wrapper -->
    </body>
    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
		function feedbackFormSubmit(){
			$('#feedbackFormGeneric').verify({
				if (result) {
					var firstName = $('#firstName').val();
					var lastName = $('#lastName').val();
					var email = $('#email').val();
					var phone = $('#phone').val();
					var message = $('#message').val();
					var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
					$.ajax({
						url: "feedbackFormEngine.php",
						data: param,
						type: "POST",
						success:(function(data) {
							swal({
									title: "Thank You!",
									text: "Email sent successfullly.",
									showConfirmButton: false,
									type: "success"
								});
								//window.location.href=window.location.href;
						}),
						complete:function(){
							window.location.href=window.location.href;
						}
					})
				}
			})
		}

		$("#feedbackFormContent").load("feedbackForm.php");
		$('.feedbackFormLink').magnificPopup({
			type:'inline',
			midClick: true
		});
	</script>

    <script>
		$(document).ready(function()
		 {
			 $('#statusToolTip').each(function() {
				 $(this).qtip({content: {text: $(this).next('.statusToolTip')},
					position: {my:'bottom center', at:'top center',},
					style: {classes: 'qtip-light qtip-shadow qtip-rounded'}
				 });
			 });
		 });
	</script>
  <!-- <script>
      if(getVqValue('stepTwoVisited') != "true")
      {
        window.location.href = "signup.php";
      }
      else
      {
        setVqValue('stepThreeVisited', true);
      }
  </script> -->
</html>
