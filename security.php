<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '0a0b3235da6f4d059317e689699b3ab2ec82655860c74ba59a22274ebeb2789b92e854f7318f4637b7cc85a7c57ad12f473d6000c0984891a23ccaa13e3b8f6e094f8bc7acb24b4bac1ae728c244f3aff684171c9801475888897f61cb7d7251f4540b62898945cdb9385847d9c2ed6ce7deb3dd94674a9f8f0f462f9bc1af99');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
