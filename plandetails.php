<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
        <script src="js/custom/plandetails.js"></script>
        <script src="js/verify.notify.min.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>

        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
   		<style>
      .notAllowed:hover {
        cursor: not-allowed;
      }
			@media only screen and (min-width: 481px) and (max-width: 640px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -215px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -149px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -65px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				width: 37px;
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -35px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				background: #F15757;
			}
			li#pb8 {
				width: 0px;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
				display: none;
			}
			}
			@media only screen and (min-width: 321px) and (max-width: 480px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -220px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -149px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -65px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				width: 37px;
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -35px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				background: #F15757;
			}
			li#pb8 {
				width: 0px;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
				display: none;
			}
			}
			@media only screen and (max-width: 320px){
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -230px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -149px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -65px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				width: 37px;
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -27px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				background: #F15757;
			}
			li#pb8 {
				width: 0px;
			}
			li#pb7 {
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt8{
				display: none;
			}
			}
      @media only screen and (min-width: 769px){
        .promoSel3 {
          padding: 56px 30px 57px 30px;
        }
      }
      @media only screen and (max-width: 768px){
        .promoSel3 {
          padding: 51px 30px 51px 30px;
        }
      }

		</style>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TF99FW');</script>
		<!-- End Google Tag Manager -->

    </head>
	 <body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

    	<div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
                
                <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <li id="pb4" class="active"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <!--<li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>-->
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">PAYMENT</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
    	<div class="wrapper">
        	<div class="planDetailsWrapper">
				<div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
          	<div class="pageHeader">Residential Broadband Online Signup</div>
           	<hr id="top-hr">
           	<h1 class="plan-details-h1">VIEWQWEST SERVICE PLAN DESCRIPTION</h1>
          	<h4 class="planSubHeader" style="display:none;">PLAN DETAILS</h4>

            <!-- 1Gbps $49.90 Plan Details -->
            <div class="plan1gbpsPlanDetails_1">
            	<h4 class="plan-details-h4 servicePlanHeader" id="1GbpsMonthlyH4_1" style="display:none;">Monthly - 1Gbps Bundle</h4>
            </div>
            <div class="plan-details-text plan-details-text-wrapper" id="planDetails_1Gbps_1" style="display: none">
              - 24 Months Subscription to Fibernet 1Gbps<br />
              - Free 1x Static IP<br />
              - Free Modem Rental<br />
              - Free Delivery & Self-Installation<br />
              - Free 3 Months Freedom DNS (optional @ $10.70/mth thereafter)<br />
              - $200 bill rebate or $200 router discount<br />
        	</div>

            <!-- 2Gbps SN $69.90 Plan Details -->
            <div class="plan2gbpsSNPlanDetails_1">
              <h4 class="plan-details-h4 servicePlanHeader" id="2GbpsSNMonthlyH4" style="display:none;">Monthly - 2Gbps Single Network Bundle</h4>
            </div>
            <div class="plan-details-text plan-details-text-wrapper" id="planDetails_2GbpsSN" style="display: none">
              - 24 Months Subscription to Fibernet 2Gbps Single Network<br />
              - Free 1x Static IP<br />
              - Free Modem Rental<br />
              - Free Delivery & Installation<br />
              - Free 3 months Freedom DNS (optional @ $10.70/mth thereafter)<br />
              - $200 bill rebate or $200 router discount<br />
            </div>

            <!-- 1Gbps $888 Plan Details -->
           <div class="plan1gbpsPlanDetails_2">
            	<h4 class="plan-details-h4 servicePlanHeader" id="1GbpsMonthlyH4_2" style="display:none;">1Gbps Upfront Plan</h4>
            </div>
            <div class="plan-details-text plan-details-text-wrapper" id="planDetails_1Gbps_2" style="display: none">
			  - 24 months Subscription to Fibernet 1Gbps<br />
			  - 9 Months Free Freedom DNS (optional @$10.70/mth thereafter)<br />
			  - Free Modem Rental<br />
			  - $100 Router Discount (upon signup – valid on select router models)<br />
			  - Free Delivery & Self-Installation<br />
			  - Free 1x Static IP Address<br />
			  - Registration Fee ($53.50) waived

            </div>

            <!-- 2Gbps $1299 Plan Details -->
        		<div class="plan2gbpsSNPlanDetails">
           		<h4 class="plan-details-h4 servicePlanHeader" id="2GbpsMonthlyH4" style="display:none;">2Gbps Upfront Plan</h4>
            </div>
            <div class="plan-details-text plan-details-text-wrapper" id="planDetails_2Gbps" style="display: none">
              - 24 months Subscription to Fibernet 2Gbps – Single Network<br />
              - 12 Months Free Freedom DNS (optional @$10.70/mth thereafter)<br />
              - 12 Months Free OneVoice (optional @$3.95/mth thereafter)<br />
              - Free Modem Rental<br />
              - $100 Router Discount (upon signup – valid on select router models)<br />
              - Free Delivery & VQ Installation<br />
              - Free 1x Static IP Address<br />
              - Registration Fee ($53.50) waived<br />
            </div> 

              	<br>
               <script>
             		function subsChange(){
      							var subsDropDown = document.getElementById("subscription");
      							var subsSelValue = subsDropDown.options[subsDropDown.selectedIndex].value;
      							switch(subsSelValue){
      								 case "Fibernet 1Gbps @ $888":
      									  $("#1GbpsAnnualH4_2").show();
      									  $("#1GbpsMonthlyH4_2").hide();
      									  break;

      								 case "2GB":
      									  $("#2GbpsMonthlyH4_1").show();
      									  $("#2GbpsAnnualH4_1").hide();
      									  break;

      								 case "2GBSN":
      									  $("#2GbpsAnnualH4_2").show();
      									  $("#2GbpsMonthlyH4_2").hide();
      									  break;

      								 default:
                           $("#1GbpsAnnualH4_1").hide();
                           $("#1GbpsMonthlyH4_1").hide();
      									  $("#1GbpsAnnualH4_2").hide();
      									  $("#1GbpsMonthlyH4_2").hide();
      									  $("#2GbpsMonthlyH4_1").hide();
      									  $("#2GbpsAnnualH4_1").hide();
      									  $("#2GbpsMonthlyH4_2").hide();
      									  $("#2GbpsAnnualH4_2").hide();
      									  break;
      							}
                	}
                </script>

              	<h4 class="value-added-h4">Plan Payment Options:</h4>
      			  <div style="margin-bottom:30px; margin-top:-10px;">
      					<select class="dropdown1Gbps" id="subscription" onChange="subsChange()"  style="-moz-appearance:none;">
      						<!-- <option value="" class="plan2gbpsSN $69.90">-- Please Select --</option> -->
      						<option value="1GB-1" class="plan1gbps_1" selected="">$49.90/mth</option>
      						<option value="1GB-2" class="plan1gbps_2" selected="">One-Time Payment: $888</option>
      						<option value="2GB" class="plan2gbps" selected>One-Time Payment: $1299</option>
      						<option value="2GBSN" class="plan2gbpsSN" selected>$69.90/mth</option>
      					</select>
      				</div>
                 <div id="promotion-offer">
					<h4 class="value-added-h4">Promotional Offer:</h4>
					<p style="color: #fff;">Please choose your preferred plan promotional benefit.</p>
					<div>
						<div class="routerSelContainer">
							<div class="promoHead1"></div>
							<div class="routerSelImg promoSel1"><img src="images/routers/piggy_bank.png" width="auto" height="auto"></div>
							<div class="routerPrice" id="promoPriceVal1" style="font-size: 16px; font-weight: normal;">$200 bill rebate (applied immediately upon service activation)</div>
						</div>
						<div class="routerSelContainer">
							<div class="promoHead2"></div>
							<div class="routerSelImg promoSel2"><img src="images/routers/1200G+_001.jpg" width="auto" height="auto"></div>
							<div class="routerPrice" id="promoPriceVal2" style="font-size: 16px; font-weight: normal;">$200 router discount</div>
						</div>
					</div>
				</div>
               <h4 class="value-added-h4" style="margin-top:50px;">FiberGuard:</h4>
               <div class="fiberGuardNotes">
                  <p>
                     Fiber Guard is a web filtering service that will automatically block undesirable sites based on the category and content of the website.
                  </p>
                  <p>
                     Blocked sites for <strong>"Moderate"</strong> protection include categories such as: Nudity, Violence, Drugs, Hate Speech, Pornography etc..
                  </p>
                  <p>
                     Blocked sites for <strong>"High"</strong> protection include all categories blocked under "Moderate" protection + additional categories such as Downloads & Sharing, Chats/IM, Dating & Relationships etc..
                  </p>
                  <p>
                     As per Singapore Government regulations - if you wish to opt for Fiber Guard, the first 6 months of the service will be free. However please note that Fiber-Guard will be charged at $19.95/mth thereafter.
                  </p>
               </div>
               <div class="fiberGuardWrapper">
						<div class="value-added-container">
							<select id="fiberGuard" style="-moz-appearance:none;">
								<option value="">-- Please Select --</option>
								<option value="fiberGuardMod">Fiber Guard @ $19.95/mth (Moderate)</option>
								<option value="fiberGuardHigh">Fiber Guard @ $19.95/mth (High)</option>
                <option value="noFiberGuard">No thanks. I don't need Fiber Guard</option>
							</select>
						</div>
					</div><!-- //fiberGuardWrapper -->

                <!-- ROUTER TOP-UP OPTIONS -->
                <h4 class="value-added-h4" style="margin-top:70px;">Hardware Top-up Options:</h4>
                <p style="color: #fff; text-align: center; font-size: 18px;" id="routerDesc"></p>
                <div class="routerSelectWrapper">
                    <!-- Router 1 -->
                    <div class="routerSelContainer">
                        <div class="routerHead1"></div>
                        <div class="routerSelImg routerSel1"><img id="routerSelect1" src="images/routers/1200G+_001.jpg" width="auto" height="auto"></div>
                        <div class="routerName" id="router1">ASUS RT-AC1200G+</div>
                        <div class="routerPrice" id="routerPriceVal1">Top-up $99</div>
                    </div>

                    <!-- Router 2 -->
                    <div class="routerSelContainer">
                        <!-- Comment added because ASUS RT-AC88U has stock -->
                        <div class="routerHead2"></div>
                        <div class="routerSelImg routerSel2"><img id="routerSelect1" src="images/routers/88u_001.jpg" width="auto" height="auto"></div> 
                        <!-- <div class="routerSelImg notAllowed"><img id="routerSelect1" src="images/routers/88u_out_of_stock.jpg" width="auto" height="auto"></div>-->
                        <div class="routerName" id="router2">ASUS RT-AC88U</div>
                        <!-- <div class="routerPrice" id="routerPriceVal2">Out of Stock</div>-->
                        <div class="routerPrice" id="routerPriceVal2">Top-up $329</div> 
                    </div>

                    <!-- Router 3 -->
                    <div class="routerSelContainer">
                        <div class="routerHead3"></div>
                        <div class="routerSelImg routerSel3"><img id="routerSelect1" src="images/routers/5300_001.jpg" width="auto" height="auto"></div>
                        <div class="routerName" id="router3">ASUS RT-AC5300</div>
                        <div class="routerPrice" id="routerPriceVal3">Top-up $369</div>
                    </div>

                    <!-- Router 4 -->
                    <div class="routerSelContainer monthly">
                        <div class="routerHead4"></div>
                        <div class="routerSelImg routerSel4"><img id="routerSelect1" src="images/routers/ASUS_BRT-AC828.png" width="auto" height="auto"></div>
                        <div class="routerName" id="router4">Asus BRT-828</div>
                        <div class="routerPrice" id="routerPriceVal4">Top-up $459</div>

                    </div>

                    <!-- Router 5 -->
                    <div class="routerSelContainer">
                        <div class="routerHead5 routerSelHead5"></div>
                        <div class="noRouterName routerSel5">I ALREADY HAVE A ROUTER</div>
                        <div class="routerName" id="router5">No Router</div>
                        <div class="routerPrice noRouterPrice" id="routerPriceVal5"></div>
                    </div>

                </div><!-- //routerSelectWrapper -->

                <div class="ftp-admin-fee-wrapper">
                    <div class="ftp-admin-fee value-added-h4" id="divFtp">
                        <span class="ftp-admin-fee-text">FTP Installation Charge</span><span class="ftp-admin-fee-price" id="ftpCharge">$</span>
                    </div>
                    <div class="ftp-admin-fee">
                        <h4 class="adminFeeSubHeader">Compulsary One-Time Fees</h4>
                        <div class="activationFeeText">
                          <span class="oneTimeFeeText" id="activationFee">Service Activation Fee:</span>
                          <span class="oneTimeFeePrice">
                          </span>
                        </div>
                    </div>
                </div>
                <div id="removeVAS">
                <div class="vasHeader"><h3>VALUE-ADDED SERVICES</h3></div>
                <div class="vasInstallPanelLeft vasPanel">
                	  <!-- // Value Added Services are hidden by default and will be visible once user selects a Plan Payment Options -->
                    <div class="oneVoiceWrapper">
                       <h4 class="value-added-h4">OneVoice - Add-on:</h4>
                       <div class="value-added-container">
                           <div class="value-added-wrapper">
                               <div class="value-added-text">OneVoice @ $3.95</div>
                               <div class="value-added-radio"><input type="checkbox" id="oneVoice" name="productRatePlanId" value=""></div>
                           </div>
                           <div class="value-added-wrapper">
                               <div class="value-added-text">Number non-display @ $5.35</div>
                               <div class="value-added-radio"><input type="checkbox" id="numNonDisplay" name="productRatePlanId" value=""></div>
                           </div>
                           <div class="value-added-text-footnote">One Time Charge of $50 applies for new OneVoice application</div>
                       </div>
                    </div><!-- //oneVoiceWrapper -->
                 	  <div class="installationChargeWrapper">
                       <h4 class="value-added-h4 installationChargeNoDisplay">Installation Charge:</h4>
                       <div class="value-added-container installationChargeNoDisplay">
                           <div class="value-added-wrapper">
                               <div class="value-added-text">Installation Charge @ $80 per trip (For 1Gbps fibre bundle sign up)</div>
                               <div class="value-added-radio"><input type="checkbox" id='installationCharge' name="productRatePlanId" value=""></div>
                           </div>
                       </div>
                    </div><!-- //installationChargeWrapper -->
                </div><!-- //vasInstallPanelLeft -->

                <div class="vasInstallPanelRight vasPanel">
                   <!-- <div class="elevenSportsWrapper">
                       <h4 class="value-added-h4">Eleven Sports Network Subscription:</h4>
                       <div class="value-added-container">
                           <div class="value-added-wrapper">
                               <div class="value-added-text elevenSportNetworkText">6 months Free subscription to Eleven Sports Network ($19.90/mth thereafter)</div>
                               <div class="value-added-radio"><input type="checkbox" id='elevenSportsNetwork' name="productRatePlanId" value=""></div>
                           </div>
                           <div class="value-added-text-footnote">12 month subscription contract applies</div>
                       </div>
                    </div> -->
                    <div class="vq4KmediaPlayerWrapper">
                       <h4 class="value-added-h4 tv5 ">ViewQwest TV Media Player:</h4>
                       <div class="value-added-container">
                            <div class="value-added-wrapper">
                               <div class="value-added-text">ViewQwest TV Media Player @ $188</div>
                               <div class="value-added-radio"><input type="radio" id='mediaPlayer' class="mediaPlayer" name="productRatePlanId" value="" style="height:18px; width:18px;"></div>
							    <div class="value-added-text">ViewQwest TV Media Player with Daiyo Antenna Bundle - $198</div>
                               <div class="value-added-radio"><input type="radio" id='mediaPlayer2' class="mediaPlayer" name="productRatePlanId" value="" style="height:18px; width:18px;"></div>
                           </div>
                       </div>
                    </div><!-- //vq4KmediaPlayerWrapper -->
                </div><!-- //vasInstallPanelRight -->
              </div> <!-- One Time -->
                 <div class="netflixFieldWrapper">
                     <h4 class="value-added-h4">Do You Watch Netflix?</h4>
                     <div class="value-added-text">
                        <input type="radio" name="netflix" class="netflix" value="Yes" checked> Yes
                        ( <input type="checkbox" id="netflixUSA"> Netflix USA / <input type="checkbox" id="netflixSG"> Netflix Singapore)<br /><br />
                        <input type="radio" name="netflix" class="netflix" value="No"> No
                     </div>
                 </div><!-- //remarksFieldWrapper -->
                 <div class=remarksFieldWrapper>
                     <h4 class="value-added-h4">Remarks:</h4>
                     <div class="value-added-container">
                        <textarea cols="auto" rows="5" id="remarksField"></textarea>
                     </div>
                 </div><!-- //remarksFieldWrapper -->

                <div class="plan-details-next-button">
                    <input type="button" id="next" value="Next" onclick="gotoPreferredInstallation();">
                </div>

      			<!-- FEEDBACK FORM -->
               <div class="feedbackFormWrapper">
                 <div class="feedbackFormDivider"></div>
                 <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                 <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                 <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                    <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                 </div><!-- //feedbackFormContent -->
               </div><!-- //feedbackFormWrapper -->

            </div><!-- //planDetailsWrapper -->
    	</div><!-- //wrapper -->
    </body>
    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
		function feedbackFormSubmit(){
			$('#feedbackFormGeneric').verify({
				if (result) {
					//$('#feedbackFormGeneric').submit(function(e) {
					var firstName = $('#firstName').val();
					var lastName = $('#lastName').val();
					var email = $('#email').val();
					var phone = $('#phone').val();
					var message = $('#message').val();
					var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
					$.ajax({
						url: "feedbackFormEngine.php",
						data: param,
						type: "POST",
						success:(function(data) {
							swal({
									title: "Thank You!",
									text: "Email sent successfullly.",
									showConfirmButton: false,
									type: "success"
								});
								//window.location.href=window.location.href;
						}),
						complete:function(){
							window.location.href=window.location.href;
						}
					})
				}
			})
		}

		$("#feedbackFormContent").load("feedbackForm.php");
		$('.feedbackFormLink').magnificPopup({
			type:'inline',
			midClick: true
		});
	</script>

    <script type="text/javascript">
		  $(document).bind("mobileinit", function () {
				$.extend($.mobile, {
					 ajaxEnabled: false
				});
		  });
	 </script>

    <script>
		$(document).ready(function(){
		  $('input').iCheck({
			checkboxClass: 'icheckbox_flat-red',
			radioClass: 'iradio_flat-red',
			handle: 'checkbox'
		  });
		});
	</script>
  <!-- <script>
      if(getVqValue('stepFourVisited') != true)
      {
        window.location.href = "registration.php";
      }
      else
      {
        setVqValue('stepFiveVisited', true);
      }
  </script> -->
</html>
