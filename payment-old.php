<?php session_start(); ?>

<html>
<head>
	<title>VQ Online Application Form</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="css/pageLoaderLoadingBar.css" rel="stylesheet" type="text/css" />
	<link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
 	<link href="css/css-mobile.css" rel="stylesheet" type="text/css">
 	<link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
 	<link href="css/css-tablet.css" rel="stylesheet" type="text/css">
 	<link href="css/css-notebook.css" rel="stylesheet" type="text/css">
 	<link href="css/css-desktop.css" rel="stylesheet" type="text/css">
 	<link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
   <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

   <script src="jquery-1.12.0.js"></script>
	<script src="jquery.cookie.js"></script>
	<script src="util.js"></script>
	<script type="text/javascript" src="backfix.min.js"></script>
 	<script src="js/jquery-ui.min.js"></script>
	<script src="js/custom/payment.js" type="text/javascript"></script>
	<script src="js/pageLoader.js"></script>
   <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>

	<style type="text/css">
		@media only screen and (max-width: 768px){
		.signupFlow{
			margin-top: 10px;
			text-align: center;
			margin-left: -370px;
		}
		#progressbar li:nth-child(1):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -285px;
		}
		#progressbar li:nth-child(2):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -235px;
		}
		#progressbar li:nth-child(3):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -185px;
		}
		#progressbar li:nth-child(4):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -133px;
		}
		#progressbar li:nth-child(5):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -80px;
		}
		#progressbar li:nth-child(6):before {
			content: ".";
			background: none;
			color: #F15757;
			font-weight: bolder;
			font-size: 25px;
			margin-right: -26px;
		}
		#progressbar li:nth-child(2):after {
			content: none;
		}
		#progressbar li:nth-child(3):after {
			content: none;
		}
		#progressbar li:nth-child(4):after {
			content: none;
		}
		#progressbar li:nth-child(5):after {
			content: none;
		}
		#progressbar li:nth-child(6):after {
			content: none;
		}
		#progressbar li:nth-child(7):after {
			content: none;
		}
		#progressbar li:nth-child(8):after {
			background: #F15757;
		}
		#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4, #pbtxt5, #pbtxt6, #pbtxt7{
			display: none;
		}
		}
	</style>

	<!-- Zuora Public javascript library -->
	<script type="text/javascript" src="https://static.zuora.com/Resources/libs/hosted/1.1.0/zuora-min.js"/></script>
</head>

<body onLoad="loadHostedPage();">
<div class="top-border"></div>
    <div class="signupFlow">
        <ul id="progressbar">
            <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
            
            <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
            <li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>
            <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
            <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
            <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
            <li id="pb8" class="active"><span id="pbtxt8">PAYMENT</span></li>
            <div id="progressbarConnector"></div>
        </ul>
    </div><!-- //signupFlow -->

    <div class="zouraPaymentWrapper">
    	<div id="zuora_payment">
        </div>
    </div>
</body>
</html>
