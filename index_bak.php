<?php session_start(); ?>
<!-- VQ OSP Malaysia -->
<!DOCTYPE html>
<html>
    <head>
		  <title>VQ Online Application Form</title>
		   <meta http-equiv="refresh" content="0;http://devmysignup.vqbn.com/signup.php">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <script src="jquery-1.12.0.js"></script>
        <script src="js/verify.notify.min.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>

    </head>

    <script type="text/javascript">
		removeAllVqValue();
	 </script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TF99FW');</script>
	<!-- End Google Tag Manager -->

	<script type="text/javascript">
	$(document).ready(function(){
	   $.ajax({
			url:'autoDelNRICfiles.php',
			type:'POST',
			data:"json",
		   	success:function(data)
		   	{
				console.log("NRIC Folder Cleared!");
			}
		});
		return false;
	});
	</script>
    <body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

    	<div class="top-border"></div>
    	<div class="wrapper">
        	<div class="indexWrapper">
                <div class="headerWrapper">
                    <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                    <div class="pageHeader">Residential Broadband Online Signup	</div>
                    <hr id="top-hr">
                </div><!-- //headerWrapper -->
                <div class="indexButton">
                    <a class="indexButtonOption" onclick="gotoSignup();" style="cursor:pointer;"><div class="indexButtonOptionText1">New<br>Subscriber</div></a>
                    <!--<a class="indexButtonOption" href="signup.php"><div class="indexButtonOptionText1">New<br>Subscriber</div></a>-->
                    <a class="indexButtonOption open-popup-link" href="#option2Content"><div class="indexButtonOptionText2">Existing<br>Subscriber<br>Re-Contract</div></a>
                </div><!-- //indexButton -->
                <div id="option2Content" class="white-popup mfp-hide popupContactFormWrapper">
                    <div class="optionContentText1">
                        Sorry! The online signup form is not yet ready for re-contracts.
                    </div>
                    <div class="optionContentText2">
                        To Re-contract kindly fill in your details in the fields below and a member of our staff will contact you for a more personalised signup experience.
                    </div>
                    <div class="contactWrapper">
                        <div id="contactContentWrapper">
                          <div id="contactForm" class="clearfix">
                              <form id="myform" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
                                  <fieldset>
                                      <input type=hidden name="oid" value="00D90000000dJa4">
                                      <input type=hidden name="retURL" value="https://signup.viewqwest.com/callback/feedbackFormSuccess.php">
                                      </span><!-- form-title span end -->
                                      <div class="formFieldWrapper">
                                          <div class="formField">
                                            <label class="textLabel" for="last_name">First Name:</label><br>
                                            <input class="inputText inputTextNormal" type="text" id="last_name" name="last_name" data-validate="required"/>
                                          </div>
                                          <div class="formField">
                                            <label class="textLabel" for="last_name">Last Name:</label><br>
                                            <input class="inputText inputTextNormal" type="text" id="last_name" name="last_name" data-validate="required"/>
                                          </div>
                                          <div class="formField">
                                            <label class="textLabel" for="email">Email: <span style="font-size:12px; font-style:italic;">(E-mail address tagged to your ViewQwest account)</span></label>
                                            <input class="inputText" type="email" name="email" id="email" data-validate="required,email"/>
                                          </div>
                                          <div class="formField contactTextarea">
                                            <label class="textLabel" for="description">Message: <span style="font-size:12px; font-style:italic;">(Optional)</span></label><br>
                                            <textarea rows="6" cols="39" class="textarea" type="text" name="description" id="description"/></textarea>
                                          </div>
                                          <div class="formFieldSubmit">
                                            <button class="contactFormSubmitButton" type="submit">SUBMIT</button>
                                          </div>
                                      </div><!-- form-field-wrapper div end -->
                                  </fieldset>
                                    <select style="display:none;" id="00N9000000E4U94" name="00N9000000E4U94" title="Source Type">
                                        <option value="Digital">Digital</option>
                                    </select>
                                    <select style="display:none;" id="00N9000000E4U8L" name="00N9000000E4U8L" title="Source of Lead">
                                        <option value="E-Signup Form">E-Signup Form</option>
                                    </select>
                                    <select style="display:none;" id="00N9000000E4U8z" name="00N9000000E4U8z" title="Campaign">
                                        <option value=""></option>
                                    </select>
                                    <select style="display:none;" id="recordType" name="recordType">
                                        <option value="01290000000muHR">Residential Leads</option>
                                    </select>
                                    <select style="display:none;" id="00N9000000EVwmu" name="00N9000000EVwmu">
                                        <option value="ViewQwest Subscriber">ViewQwest Subscriber</option>
                                    </select>
                              </form>
                          </div><!-- contact-form div end -->
                        </div><!-- contact-content-wrapper div end -->
                    </div><!-- contact-wrapper div end -->
                </div><!-- //option2Content -->
                <div class="salesStaffField">
                	<div class="salesStaffFieldHeader" style="display:block !important;">ViewQwest Sales Staff</div>
                  	<select class="salesStaffSelect" title="Sales Staff" id="salesStaffName" style="-moz-appearance:none;">
								<option value="None">-- Please select your name --</option>
                      	<option value="Ann Tracy Santos Santos">Ann Tracy Santos Santos</option>
                      	<option value="Chang Chong Hua">Chang Chong Hua</option>
                      	<option value="Chow Pearly Wan Chuin">Chow Pearly Wan Chuin</option>
                      	<option value="Husaini Bin Muhammad Azmi">Husaini Bin Muhammad Azmi</option>
                      	<option value="Kartini Bte Ariffin">Kartini Bte Ariffin</option>
                      	<option value="Norfor Shawn Ramsey">Norfor Shawn Ramsey</option>
                      	<option value="Zhang Zhi Hong Clifford">Zhang Zhi Hong Clifford</option>
                    </select>
                </div>
                <div class="salesLocationField">
                	<div class="salesLocationFieldHeader">Signup Location</div>
                  	<select class="salesLocationSelect" title="Sales Location" id="salesLocation" style="-moz-appearance:none;">
                    		<option value="Atland">Atland</option>
                      	<option value="Suntec">Suntec</option>
                      	<option value="Sim Lim">Sim Lim</option>
                     	<option value="AudioHouse">AudioHouse</option>
                      	<option value="Roadshow">Roadshow</option>
                      	<option value="IT SHOW">IT SHOW</option> <!-- SELECTED by default for signup.vqbn.com -->
                      	<option value="Online">Online</option> <!-- SELECTED by default for signup.viewqwest.com -->
                    </select>
                </div>
                <div class="promoterNameField">
                  <label>Promoter Name:</label><br>
                  <input id="promoterName" type="text" name="promoterName"/>
                </div>

                <!-- FEEDBACK FORM -->
                <div class="feedbackFormWrapper">
                	<div class="feedbackFormDivider"></div>
						<div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                  <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                  <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                     <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                  </div><!-- //feedbackFormContent -->
                </div><!-- //feedbackFormWrapper -->

            </div><!-- //indexWrapper -->
        </div><!-- //wrapper -->
    </body>

    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
		function feedbackFormSubmit(){
			$('#feedbackFormGeneric').verify({
				if (result) {
					var firstName = $('#firstName').val();
					var lastName = $('#lastName').val();
					var email = $('#email').val();
					var phone = $('#phone').val();
					var message = $('#message').val();
					var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
					$.ajax({
						url: "",
						data: param,
						type: "POST",
						success:(function(data) {
							swal({
									title: "Thank You!",
									text: "Email sent successfullly.",
									showConfirmButton: false,
									type: "success"
								});
								//window.location.href=window.location.href;
						}),
						complete:function(){
							window.location.href=window.location.href;
						}
					})
				}
			})
		}

		$("#feedbackFormContent").load("feedbackForm.php");
		$('.feedbackFormLink').magnificPopup({
			type:'inline',
			midClick: true
		});
	</script>

	<script>
		$('.open-popup-link').magnificPopup({
		  type:'inline',
		  midClick: true
		});
	</script>

  <script>
  /*
	Date.prototype.getCurrentDate = function() {
		var dd  = this.getDate().toString();
		var mm = (this.getMonth()+1).toString();
		var yyyy = this.getFullYear().toString();

		return (yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]));
	};

	var date = new Date();
	var dateString = date.getCurrentDate();
	*/

	var dat = new Date();
	var dateToString = dat.toISOString().substring(0, 10);
	console.log(dateToString);

	<!-- JS for SALES LOCATION and SALES STAFF FIELD -->
   var currentLocation = window.location.href;
	if(currentLocation == "https://signup.viewqwest.com/"){
		$("#salesLocation option[value='Online']").attr("selected","selected");
		$('.salesStaffField').hide();
		$('.promoterNameField').hide();
		$('.salesLocationField').hide();
	}
	else{
		$("#salesLocation option[value='Online']").attr("selected","selected");
	}

	function gotoSignup(){
		if(currentLocation == "https://signup.viewqwest.com/"){
			setVqValue('salesLocation', $("#salesLocation").val());
			setVqValue('dateToString', dateToString);
			window.location.href = "signup.php";
		}
		else{
			if($("#salesStaffName").val() != ""){
				setVqValue('salesStaffName', $("#salesStaffName").val()); <!-- ENABLE ON SIGNUP.VQBN.COM-->
			}
			if($("#promoterName").val() != ""){
				setVqValue('promoterName', $("#promoterName").val()); <!-- ENABLE ON SIGNUP.VQBN.COM-->
			}
			if($("#salesLocation").val() != ""){
				setVqValue('salesLocation', $("#salesLocation").val()); <!-- ENABLE ON SIGNUP.VQBN.COM-->
			}
			setVqValue('dateToString', dateToString);
			window.location.href = "signup.php";
		}
	}
	</script>
  <script>
      setVqValue('stepOneVisited', true);
  </script>
</html>
