<?php session_start(); ?>
<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/pageLoaderLoadingBar.css" rel="stylesheet" type="text/css" />
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script type="text/javascript" src="backfix.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/custom/payment.js" type="text/javascript"></script>
        <script src="js/pageLoader.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>

        <style type="text/css">
            @media only screen and (max-width: 768px){
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -370px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -285px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -235px;
                }
                #progressbar li:nth-child(3):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -185px;
                }
                #progressbar li:nth-child(4):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -133px;
                }
                #progressbar li:nth-child(5):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -80px;
                }
                #progressbar li:nth-child(6):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    content: none;
                }
                #progressbar li:nth-child(7):after {
                    content: none;
                }
                #progressbar li:nth-child(8):after {
                    background: #F15757;
                }
                #pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4, #pbtxt5, #pbtxt6, #pbtxt7{
                    display: none;
                }
            }
        </style>

        <!-- Zuora Public javascript library -->
        <script type="text/javascript" src="https://static.zuora.com/Resources/libs/hosted/1.1.0/zuora-min.js"/></script>


</head>

<body>
    <div class="top-border"></div>
    <div class="signupFlow">
        <ul id="progressbar">
            <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>

            <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
            <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
            <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
            <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
            <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
            <li id="pb8" class="active"><span id="pbtxt8">PAYMENT</span></li>
            <div id="progressbarConnector"></div>
        </ul>
    </div><!-- //signupFlow -->

    <div class="wrapper">
        <div class="signupWrapper">
            <div class="vq-logo">
                <img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto">
            </div>
            <div class="pageHeader">Residential Broadband Online Signup</div>
            <hr id="top-hr">
            <form id="payment_form" action="addccdetails.php" method="post">
                <input type="hidden" name="access_key" value="dbbce45210843b96acbeb4957f9e88cb">
                <input type="hidden" name="profile_id" value="F98D4A69-46B9-40F9-860B-7F18839A6DEA">
                <input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">
                <input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code">
                <input type="hidden" name="unsigned_field_names" value="card_type,card_number,card_expiry_date">
                <input type="hidden" name="signed_date_time" value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                <input type="hidden" name="locale" value="en">
                <div id="paymentDetailsSection" class="section" style="padding: 10px; text-align: right; margin-right: 60%;">
                    <span style="display: none;">transaction_type:</span><input type="text" name="transaction_type" size="25" style="display: none;">
                    <span style="display: none;">reference_number:</span><input type="text" name="reference_number" size="25" style="display: none;">
                    <span style="display: none;">amount:</span><input type="text" name="amount" size="25" style="display: none;">
                    <span style="display: none;">currency:</span><input type="text" name="currency" size="25" style="display: none;">
                    <span style="display: none;">payment_method:</span><input type="text" name="payment_method" style="display: none;">
                    <span>First Name:</span><input type="text" name="bill_to_forename"><br/>
                    <span>Last Name:</span><input type="text" name="bill_to_surname"><br/>
                    <span>Email:</span><input type="text" name="bill_to_email"><br/>
                    <span>Phone:</span><input type="text" name="bill_to_phone"><br/>
                    <span>Address:</span><input type="text" name="bill_to_address_line1"><br/>
                    <span>City:</span><input type="text" name="bill_to_address_city"><br/>
                    <span>State:</span><input type="text" name="bill_to_address_state"><br/>
                    <span>Country:</span><input type="text" name="bill_to_address_country" value="MY" readonly="true"><br/>
                    <span>Postal Code:</span><input type="text" name="bill_to_address_postal_code"><br/>
                </div>
                <input type="submit" id="submit" name="submit" value="Next" onclick="setDefaultsForPaymentDetailsSection();"/>
                <script type="text/javascript" src="payment_form.js"></script>
            </form>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">
                    function setDefaultsForPaymentDetailsSection() {
                        $("input[name='transaction_type']").val("authorization,create_payment_token");
                        $("input[name='reference_number']").val(new Date().getTime());
                        $("input[name='amount']").val("100.00");
                        $("input[name='currency']").val("MYR");
                        $("input[name='payment_method']").val("card");
                        /*$("input[name='bill_to_forename']").val("Test");
                         $("input[name='bill_to_surname']").val("Account");
                         $("input[name='bill_to_email']").val("null@cybersource.com");
                         $("input[name='bill_to_phone']").val("02890888888");
                         $("input[name='bill_to_address_line1']").val("1 Card Lane");
                         $("input[name='bill_to_address_city']").val("My City");
                         $("input[name='bill_to_address_state']").val("CA");
                         $("input[name='bill_to_address_country']").val("US");
                         $("input[name='bill_to_address_postal_code']").val("94043");*/
                    }
</script>
<style type="text/css">
    #paymentDetailsSection span{
        font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
        color: #fff;
    }
    #paymentDetailsSection input{
        margin: 4px;
        vertical-align: middle;
        -moz-border-radius: 4px 4px 4px 4px;
        -webkit-border-radius: 4px 4px 4px 4px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
        height: 28px;
        font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
        color: #000;
    }

</style>