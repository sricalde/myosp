<div class="optionContentText1">
	Feedback for ViewQwest MY Online Signup Portal 
</div>
<div class="optionContentText2" style="display:none;">
	To Re-contract kindly fill in your details in the fields below and a member of our staff will contact you for a more personalised signup experience.
</div>
<div class="contactWrapper">
   <div id="contactContentWrapper">
     <div id="contactForm" class="clearfix">
         <form role="form" id="feedbackFormGeneric" method="post" action="feedbackFormEngine.php">
            <div class="formFieldWrapper">
                <div class="formField">
                  <label class="textLabel" for="firstName">First Name:</label><br>
                  <input class="inputText inputTextNormal" type="text" id="firstName" name="firstName" data-validate="required"/>
                </div>
                <div class="formField">
                  <label class="textLabel" for="lastName">Last Name:</label><br>
                  <input class="inputText inputTextNormal" type="text" id="lastName" name="lastName" data-validate="required"/>
                </div>
                <div class="formField">
                  <label class="textLabel" for="email">Email: <span style="font-size:12px; font-style:italic;"></span></label>
                  <input class="inputText" type="email" name="email" id="email" data-validate="required,email"/>
                </div>
                <div class="formField">
                  <label class="textLabel" for="phone">Phone: <span style="font-size:12px; font-style:italic;">(Optional)</span></label><br>
                  <input class="inputText" type="phone" name="phone" id="phone"/>
                </div>
                <div class="formField contactTextarea">
                  <label class="textLabel" for="message">Message: <span style="font-size:12px; font-style:italic;">(Optional)</span></label><br>
                  <textarea rows="6" cols="39" class="textarea" type="text" name="message" id="message"/></textarea>
                </div>
                <div class="formFieldSubmit">
                  <button class="contactFormSubmitButton" id="feedbackFormSubmitButton" onClick="feedbackFormSubmit(event)">SUBMIT</button>
                  <!--<button class="contactFormSubmitButton" id="feedbackFormSubmitButton">SUBMIT</button>-->
                </div>
            </div><!-- form-field-wrapper div end -->
         </form>
     </div><!-- contact-form div end -->
   </div><!-- contact-content-wrapper div end -->
</div><!-- contact-wrapper div end -->