<?php

session_start();
require_once './config.php';

$loginurl = "https://test.salesforce.com/services/oauth2/token";
$params = "grant_type=password"
        . "&client_id=" . CLIENT_ID
        . "&client_secret=" . CLIENT_SECRET
        . "&username=" . SF_USERNAME
        . "&password=" . SF_PASSWORD . SF_TOKEN;
$curl = curl_init($loginurl);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

if ($status != 200) {
    die("Error: call to URL failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
}

curl_close($curl);


$response = json_decode($json_response, true);
if (isset($_REQUEST['action'])) {
    $access_token = base64_decode($_REQUEST['act']);
    $instance_url = base64_decode($_REQUEST['iu']);
} else {
    $access_token = $response['access_token'];
    $instance_url = $response['instance_url'];
}

if (!isset($access_token) || $access_token == "") {
    die("Error - access token missing from response!");
}

if (!isset($instance_url) || $instance_url == "") {
    die("Error - instance URL missing from response!");
}

$_SESSION['access_token'] = $access_token;
$_SESSION['instance_url'] = $instance_url;

//var_dump($response);
CreateSFAccount($access_token, $instance_url);

function CreateSFAccount($access_token, $instance_url) {
    $url = $instance_url . "/services/data/v20.0/sobjects/Account";
    $sf_FullName__c = $_SESSION['salutation'] . ' ' . $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
    $sf_Salutation = $_SESSION['salutation'];
    $sf_FirstName = $_SESSION['firstName'];
    $sf_LastName = $_SESSION['lastName'];
    $sf_Gender__c = $_SESSION['gender'];
    $sf_NRIC_Passport_FIN_No__pc = $_SESSION['nric'];
    $sf_Phone = $_SESSION['phone'];
    $sf_PersonMobilePhone = $_SESSION['mobile'];
    $sf_PersonEmail = $_SESSION['email'];
    $sf_PersonBirthdate = $_SESSION['dob'];
    $sf_Nationality__c = 'Malaysian';
    $sf_Status__c = 'Active';
    $sf_RecordTypeId = '012900000003dcU';
    $sf_PersonMailingCountry = 'Malaysia';
    $sf_PersonMailingPostalCode = $_SESSION['postal'];
    $sf_PersonOtherCountry = 'Malaysia';
    $sf_Signup_ID__c = $_SESSION['customerUID'];

    $content = json_encode(array(
        "FullName__c" => $sf_FullName__c,
        "Salutation" => $sf_Salutation,
        "FirstName" => $sf_FirstName,
        "LastName" => $sf_LastName,
        "Gender__c" => $sf_Gender__c,
        "NRIC_Passport_FIN_No__pc" => $sf_NRIC_Passport_FIN_No__pc,
        "Phone" => $sf_Phone,
        "PersonMobilePhone" => $sf_PersonMobilePhone,
        "PersonEmail" => $sf_PersonEmail,
        "PersonBirthdate" => $sf_PersonBirthdate,
        "Nationality__c" => $sf_Nationality__c,
        "Status__c" => $sf_Status__c,
        "RecordTypeId" => $sf_RecordTypeId,
        "PersonMailingCountry" => $sf_PersonMailingCountry,
        "PersonMailingPostalCode" => $sf_PersonMailingPostalCode,
        "PersonOtherCountry" => $sf_PersonOtherCountry,
         "Signup_ID__c" => $sf_Signup_ID__c
    ));

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        "Authorization: OAuth $access_token",
        "Content-type: application/json"
    ));
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
    $json_response = curl_exec($curl);
    curl_close($curl);
    $json = json_decode($json_response, true);
    print_r($json);

    $crm_id = $json['id'];
    
    CreateZAccount($crm_id);
}

function CreateZAccount($crm_id) {
  $zr_url = "https://apisandbox-api.zuora.com/rest/v1/accounts";
  $username = USERNAME;
  $password = PASSWORD;

  $zr_content = '{
  "name":"'.$_SESSION['firstName'] . ' ' . $_SESSION['lastName'].'",
  "currency":"MYR",
  "notes":"CYB Networks",
  "billCycleDay":15,
  "crmId":"'.$crm_id.'",
  "autoPay":"false",
  "paymentTerm":"Net 30",
  "billToContact":{
    "address1":"address1",
    "address2":"address2",
    "city":"Kuala Lumpur",
    "country":"Malaysia",
    "county":"",
    "fax":"",
    "firstName":"'.$_SESSION['firstName'].'",
    "homePhone":"",
    "lastName":"'.$_SESSION['lastName'].'",
    "mobilePhone":"'.$_SESSION['phone'].'",
    "nickname":"",
    "otherPhone":"",
    "personalEmail":"'.$_SESSION['email'].'",
    "zipCode":"",
    "state":"",
    "workEmail":"testcybersource@gmail.com",
    "workPhone":"09187148153"
  },
  "soldToContact":{
    "address1":"address1",
    "address2":"address2",
    "city":"Kuala Lumpur",
    "country":"Malaysia",
    "county":"",
    "fax":"",
    "firstName":"Thelma Trivino",
    "homePhone":"",
    "lastName":"Ricalde",
    "mobilePhone":"15551238755",
    "nickname":"",
    "otherPhone":"",
    "personalEmail":"",
    "zipCode":"",
    "state":"",
    "workEmail":"testcybersource@test.com",
    "workPhone":""
  },
 "hpmCreditCardPaymentMethodId": "2c92c0f93c1deb6e013c47cefff26fde"
},
  "subscription":{
    "contractEffectiveDate": "2018-12-01",
    "termType":"TERMED",
    "initialTerm":12,
    "autoRenew":true,
    "renewalTerm":12,
    "notes":"test subscription",
    "subscribeToRatePlans":[
      {
        "productRatePlanId": "2c92c0f95e0da918015e137dbbcd42b3"
      }
    ]
  },
';

  $zr_curl = curl_init($zr_url);
  curl_setopt($zr_curl, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($zr_curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($zr_curl, CURLOPT_HEADER, false);
  curl_setopt($zr_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept:application/json', 'Content-Length: ' . strlen($zr_content)));
  curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($zr_curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($zr_curl, CURLOPT_POSTFIELDS, $zr_content);
  curl_setopt($zr_curl, CURLOPT_USERPWD, "$username:$password");

  $json_response = curl_exec($zr_curl);
  curl_close($zr_curl);
  $json = json_decode($json_response);
  foreach($json as $name => $value){
      if($name == 'accountId'){
          $account_id = $value;
      }
  }
  //AddCCRefPayment($account_id);
  }
  
function AddCCRefPayment($account_id){
$zr_payment_token = "https://rest.apisandbox.zuora.com/v1/action/create";

$username = USERNAME;
$password = PASSWORD;
$payment_token = $_SESSION['payment_token'];
echo $payment_token; 
//$payment_token = '5032837188546780803524';
$zr_content = '{
   "objects": [
    {
      "AccountId": "'.$account_id.'",
      "TokenId": "'.$payment_token.'",
      "Type": "CreditCardReferenceTransaction"
    }
  ],
  "type": "PaymentMethod"
}';

$zr_curl = curl_init($zr_payment_token);
curl_setopt($zr_curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($zr_curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($zr_curl, CURLOPT_HEADER, false);
curl_setopt($zr_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept:application/json', 'Content-Length: ' . strlen($zr_content)));
curl_setopt($zr_curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($zr_curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($zr_curl, CURLOPT_POSTFIELDS, $zr_content);
curl_setopt($zr_curl, CURLOPT_USERPWD, "$username:$password");

$json_response = curl_exec($zr_curl);
curl_close($zr_curl);

var_dump($json_response);
echo '<br/>';
echo '<br/>';
}

function UpdatePaymentToken($old_token){
    
}
?>

