<?php

session_start();
//send email
require 'PHPMailer/PHPMailerAutoload.php';
$emailAdd = $_SESSION['email'];
$planPrice = $_SESSION['planPrice'];
$planSpeed = $_SESSION['planSpeed'];
$planIncludes = $_SESSION['planIncludes'];
$planContact = $_SESSION['planContact'];
$result = array();

$mail = new PHPMailer;

$mail->isSMTP();                            // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                     // Enable SMTP authentication
$mail->Username = 'noreply.viewqwest@gmail.com';          // SMTP username
$mail->Password = 'admin12345!'; // SMTP password
$mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                          // TCP port to connect to

$mail->setFrom('no-reply@viewqwest.com', 'ViewQwest');
$mail->addReplyTo('no-reply@viewqwest.com', 'ViewQwest');
$mail->addAddress($emailAdd);   // Add a recipient
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

$mail->isHTML(true);  // Set email format to HTML
//$custName = $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
$custName = $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];

$bodyContent = '';

$bodyContent .= '<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ViewQwest Pte Ltd</title>
</head>

<body>

<!---=== THIS IS THE HEADING PART ===--->
<table align="center" bgcolor="#000000" width="800" border="0" cellspacing="0" cellpadding="0" style="border:solid #000 1px">
 <tr>
    <td width="36%" height="85" style="border-bottom:solid #C4122E 6px; padding-left: 20px;"><a href="http://www.viewqwest.com" target="_blank"><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?id=01590000006CJXo&oid=00D90000000dJa4&lastMod=1416217223000" alt="www.viewqwest.com" width="149" height="30"></td>
    <td width="100%" style="border-bottom:solid #C4122E 6px; padding-right: 20px;"><table width="30%" border="0" cellspacing="7" cellpadding="0" align="right">
      <tr>
        <td><a href="https://www.facebook.com/Viewqwest" target="_blank"><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?
id=01590000006CJZu&oid=00D90000000dJa4&lastMod=1416217495000" width="25" height="24"></a></td>
        <td><a href="http://www.linkedin.com/company/viewqwest-pte-ltd" target="_blank"><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?
id=01590000006CJai&oid=00D90000000dJa4&lastMod=1416217596000" width="25" height="24"></a></td>
        <td><a href="https://twitter.com/ViewQwest" target="_blank"><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?
id=01590000006CJbH&oid=00D90000000dJa4&lastMod=1416217633000" width="25" height="24"></a></td>
        <td><a href="https://plus.google.com/116219660059232618202/about?gclid=CK299cu_zb4CFdYVjgodcikAoA" target="_blank"><img src="https://c.ap1.content.force.com/servlet/servlet.ImageServer?
id=01590000006CJaE&oid=00D90000000dJa4&lastMod=1416217553000" width="24" height="24"></a></td>
      </tr>
    </table></td>
  </tr>
<!---=== END OF HEADING PART ===--->


  <tr>
    <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="4%">&nbsp;</td>
              <td width="93%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              
              
              <!---=== THIS IS THE SALUTATION PART ===--->
              <td>
              
              Dear '.$custName.', 
              
              </td>
              <!---=== END OF SALUTATION PART PART ===--->
              
              
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              
              
              <!---=== THIS IS THE BODY PART ===--->
              <td>

<center><h3><u>Welcome to ViewQwest</u></h3></center>

<p>Thank you for signing up and welcome to the ViewQwest family</p>

<p>Please review your order information below and notify us if there are discrepancies so that the necessary changes can be made during the initial phase of installation. </p>

<p><b><u>Plan Details</u></b></br><br/></p>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">&nbsp;<b>Price</b>&nbsp;</td>
<td width="65%">&nbsp;'.$planPrice.'&nbsp;</td>
</tr>

<tr>
<td>&nbsp;<b>Speed</b>&nbsp;</td>
<td>&nbsp;'.$planContact.'&nbsp;</td>
</tr>


<tr>
<td>&nbsp;<b>Contract Term (months)</b>&nbsp;</td>
<td>&nbsp;'.$planContact.'&nbsp;</td>
</tr>

<tr>
<td>&nbsp;<b>Plan includes</b></td>
<td> '.$planIncludes.' </td>
</tr>


</table>
<br/>
<p>Your order will be assigned to our Case Manager for further processing and they will be in touch with you soon</p>
<p>Please feel free to contact us at 03 - 2775 0100, Monday to Friday (9:00am to 6:00pm) or email <a href="mailto:sales.my@viewqwest.com">sales.my@viewqwest.com</a> for any concerns and clarifications</p>
           
              </td>
              <!---=== END OF THE BODY PART ===--->
                
                
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              
              
              <!---=== THIS IS THE SIGNATURE PART ===--->
              <td>
              
              Best Regards,<br>
ViewQwest Team<br><br> 

</td>
              <td>&nbsp;</td>
              <!---=== END OF SIGNATURE PART ===--->
              
                            
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
  
  
<!---=== THIS IS THE FOOTER PART ===--->
<td height="78" colspan="2" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#939598;">Copyright © 2017 ViewQwest  Sdn Bhd (Company Regn No.:888580-K), All rights reserved. &nbsp; <a style="color:#D03238; text-decoration:none;" href="mailto:sales.my@viewqwest.com 
">sales.my@viewqwest.com 
</a><br>
Lower Level 3-3A & 5, Tower 1, Avenue 7, The Horizon Annexe, Bangsar South City, No. 8 Jalan Kerinchi, 59200 Kuala Lumpur  |  <a style="color:#D03238; text-decoration:none;" href="http://viewqwest.com">www.ViewQwest.com</a>  |  T: 2775 0100 </td>
</tr>
<!---=== END OF FOOTER PART ===--->
  
  
</table>

</body>
</html>';

$mail->Subject = 'ViewQwest - Official Receipt (E-Form Signup)';
$mail->Body = $bodyContent;

if (!$mail->send()) {
  echo "Not sent!";
} else {
   echo "Sent!";
   
}


?>