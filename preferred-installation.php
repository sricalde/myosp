<?php session_start(); ?>

<!DOCTYPE html>

<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
        <script src="js/custom/preferred-installation.js" type="text/javascript"></script>
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <style type="text/css">
			@media only screen and (min-width: 481px) and (max-width: 640px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -170px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -149px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -65px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				width: 37px;
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -22px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(7):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				background: #F15757;
			}
			#pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt7{
				display: none;
			}
			}
			@media only screen and (min-width: 321px) and (max-width: 480px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -214px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -149px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -65px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				width: 37px;
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -22px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(7):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				background: #F15757;
			}
			#pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt7{
				display: none;
			}
			}
			@media only screen and (max-width: 320px){
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin-left: -290px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -188px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -136px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -82px;
			}
			#progressbar li:nth-child(4):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				content: ". .";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -18px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(7):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				background: #F15757;
			}
			li#pb7{
				width: 0px;
			}
			#pb8{
				display: none !important;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4, #pbtxt7{
				display: none;
			}
			}
		</style>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TF99FW');</script>
		<!-- End Google Tag Manager -->

    </head>
    <body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

    	<div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
                
                <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>
                <li id="pb5" class="active"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>
                <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">PAYMENT</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
    	<div class="wrapper">
        	<div class="preferredInstallationWrapper">
                <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <h1 class="preferredInstallation">PREFERRED INSTALLATION DATE & TIME</h1>
                <div class="value-added-container">
                    <div class="value-added-wrapper vasInstallationWrapper">
                        <input class="installationDateInput" name="installationDateStr" id="installationDateStr" size="27" readonly>
                        <select name="installationTimeSlot" id="installationTimeSlot"></select>
                    </div><br>
                    <div id="datepicker" class="datePickerWrapper"></div>
                    <div class="dateInstallationFootnote">
                        Kindly note that indicated preferred installation date & time are subjected to availability.
                        Our staff will contact you within 3 working days to confirm your installation date & time.
                  	</div>
                </div><!-- //value-added-container -->
                <div class="preferredInstallationNextButton">
                    <input type="button" id="next" value="Next" onclick="gotoPreview();">
                </div><!-- //plan-details-next-button -->
			</div><!-- //preferredInstallationWrapper -->
		</div><!-- //wrapper -->
    </body>
    <script>
		$(document).ready(function(){
		  $('input').iCheck({
			checkboxClass: 'icheckbox_flat-red',
			radioClass: 'iradio_flat-red'
		  });
		});
	</script>
</html>
