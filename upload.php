<?php
$target_dir = "nricUpload/";
$target_file = $target_dir . basename($_FILES["file"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
       // echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
		echo json_encode($uploadOk);
    } else {
        //echo "File is not an image.";
        $uploadOk = 0;
		echo json_encode($uploadOk);
    }
}
if (file_exists($target_file)) {
    //echo "Sorry, file already exists.";
    $uploadOk = 0;
	$x = 1;
	$y = 1;
	echo json_encode($x);
	echo json_encode($y);
}
if ($_FILES["file"]["size"] > 7242880 ) {
    //echo "Sorry, your file is too large.";
    $uploadOk = 0;
	$x = 2;
	$y = 2;
	echo json_encode($x);
	echo json_encode($y);
}
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "PNG" && $imageFileType != "JPG") {
    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
	$x = 3;
	$y = 3;
	echo json_encode($x);
	echo json_encode($y);
}
if ($uploadOk == 0) {
    //echo "Sorry, your file was not uploaded.";
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
       $x = 4;
	   $y = 4;
    echo json_encode($x);
	echo json_encode($y);
    } else {
       // echo "Sorry, there was an error uploading your file.";
    }
}
?>