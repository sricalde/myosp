<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
}
thead {
	background-color: #eeeeee;
}
tbody {
	background-color: #FFF;
}
th, td {
	padding: 3pt;
}
table.collapse {
	border-collapse: collapse;
	border: 1px solid black;
}
table.collapse td {
	border: 1px solid black;
	background-color: #fff;
}
.tbl_title {
	background-color: #CEE5ED;
	text-align:center;
	padding-top:10px;
	padding-bottom:10px;
	font-weight:bold;
	font-size: 18px;
}
/** TERMS and CONDITIONS **/
.terms-wrapper{
	width: 100%;
	font-size: 12px;
	line-height: 16px;
}
.terms-points{
	font-weight: bold;
}
.terms-contents a{
	vertical-align: top;
}
.terms-points-text li{
	vertical-align: top;
}
.headerBold{
	font-weight: bold;
}
ol#termsOLLI{
	vertical-align: top;
}

</style>

</head>

<body>
<h2>VQ Online Application Form</h2>
<hr>
<br />
<table class="collapse" cellpadding="4" cellspacing="0" width="100%">
  <tr>
    <th colspan="2" class="tbl_title"><strong>SUBSCRIBER'S PARTICULARS</strong></th>
  </tr>
  <tr>
    <td width="25%">Salutation:</td>
    <td width="75%"><?php echo $vq_salutation; ?></td>
  </tr>
  <tr>
    <td>First Name:</td>
    <td><?php echo $vq_first_name; ?></td>
  </tr>
  <tr>
    <td>Last Name:</td>
    <td><?php echo $vq_last_name; ?></td>
  </tr>
  <tr>
    <td>MyKad / Passport:</td>
    <td><?php echo $vq_nric_passport; ?></td>
  </tr>
  <tr>
    <td>Address:</td>
    <td><?php echo $vq_address; ?></td>
  </tr>
  <tr>
    <td>Nationality:</td>
    <td><?php echo $vq_nationality; ?></td>
  </tr>
  <tr>
    <td>Gender:</td>
    <td><?php echo $vq_gender; ?></td>
  </tr>
  <tr>
    <td>Date of Birth:</td>
    <td><?php echo $vq_dob; ?></td>
  </tr>
  <tr>
    <td>Email:</td>
    <td><?php echo $vq_email; ?></td>
  </tr>
  <tr>
    <td>Mobile Number:</td>
    <td><?php echo $vq_mobile; ?></td>
  </tr>
  <tr>
    <td>Home Number:</td>
    <td><?php echo $vq_phone; ?></td>
  </tr>
	<tr>
    <td>Subscribed Plan:</td>
    <td>
			<?php echo $vq_price; ?> <br/>			<?php echo $vq_speed; ?> <br/>			<?php echo $vq_contract; ?> <br/>			<?php echo $vq_freebies; ?> <br/>
    </td>
  </tr>

</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br>
<div class="terms-wrapper">
  <div class="terms-contents">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
              <td colspan="3" class="tbl_title">TERMS AND CONDITIONS</td>
            </tr>
            <tr valign="top" >
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr valign="top" >
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr valign="top" >
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>

      <!-- TERMS OF SERVICE -->
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">1.</td>
              <td colspan="2">Terms of Service - ViewQwest Fibre Broadband Bundle</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">a)</td>
              <td colspan="2">Service Application</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                Subscriber must be 18 years old and above to be eligible for the services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                  The following original documents must be submitted to ViewQwest during registration via online signup portal::
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="85px;" valign="middle">
                  - A copy of your photo identification document (front and back) as indicated in the online signup portal (the subscriber must be Malaysian or Permanent Resident)<br>
                  - If the subscriber is a foreigner, the subscriber must be an Employment Pass Holder, Work Permit Holder, Student Pass Holder, or Dependent Pass Holder (with a minimum validity period of 12 months)<br>
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                  ViewQwest reserves the right to modify or amend the Terms and Conditions in the Service Application and Agreement Form and any other agreements made with the subscriber. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                  iv.	ViewQwest’s General Terms &amp; Conditions can be downloaded from our website, <a href="http://www.viewqwest.com/subscribercare/ViewqwestMY_TermsAndConditions.pdf">http://www.viewqwest.com/subscribercare/ViewqwestMY_TermsAndConditions.pdf</a>. 
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>	  	  <tr valign="top" >        <td>&nbsp;</td>        <td>&nbsp;</td>      </tr>	  <tr valign="top" >        <td>&nbsp;</td>        <td>&nbsp;</td>      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">b)</td>
              <td colspan="2">Fees and Charges</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                All amounts quoted in this contract are in Malaysian Ringgit, and include prevailing GST or other government charges.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                  By registering for ViewQwest service, subscriber is liable for the following charges , unless expressly waived by ViewQwest:
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="85px;" valign="middle">
                  - One-Time Registration Fees (if any); and<br>
                  - Any relevant taxes payable on the service(s) including without limitation, service tax or goods and service tax or other taxes and charges and shall indemnify ViewQwest for payment of the same; and<br>
                  - If the service package is bundled with an equipment, subscriber is subjected to monthly or upfront payment in accordance with the relevant service package; and<br>
                  - Other miscellaneous charges as determined by ViewQwest<br>
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                  For all Fibre Broadband plan, subscriber will be charged any miscellaneous fees, including without limitation the following, where applicable and may be incurred by subscriber, in respect of each Service Address:
              </td>
            </tr>
        	</table>
        </td>
      </tr>
      <tr valign="top">
         <td>&nbsp;</td>
         <td>&nbsp;</td>
   	</tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="1" style="border-collapse:collapse">
           <tr>
             <td width="77%" height="40" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Description of One Time Service Charges</td>
             <td width="23%" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Charges (Subject to 6% GST)</td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Failure to submit all documentation and to confirm an installation date within 2 weeks from submission of this form</td>
             <td style="padding-left:14px;">RM50</td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Successful Installation of non-ViewQwest supported router</td>
             <td style="padding-left:14px;">RM150</td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Reactivation Charge</td>
             <td style="padding-left:14px;">RM100</td>
           </tr>
         </table>
       </td>
     </tr>
     <tr valign="top" >
       <td>&nbsp;</td>
       <td>&nbsp;</td>
     </tr>
     <tr>
        <td colspan="2">
         <table width="100%" border="1" style="border-collapse:collapse">
           <tr>
             <td width="77%" height="40" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Description of Miscellaneous Charges (if applicable)</td>
             <td width="23%" align="left" valign="middle" style="font-weight:bold; padding-left:14px;"><strong>Charges (Subject to 6% GST)</strong></td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Cancellation of confirmed ViewQwest onsite installation appointment</td>
             <td style="padding-left:14px;">RM100</td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Failure to be available during ViewQwest onsite installation appointment</td>
             <td style="padding-left:14px;">RM50</td>
           </tr>
           <tr>
             <td style="padding-left:14px;">Late Payment charge due to 3 consecutive declined charges by debit/credit card within 1 month period</td>
             <td style="padding-left:14px;">RM10</td>
           </tr>
         </table>
       </td>
     </tr>
     <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
		</tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
			 	<tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                ViewQwest shall be entitled to revise the charges shown above without prior notice. 
              </td>
            </tr>
          </table>
       </td>
     </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">c)</td>
              <td colspan="2">Setia Eco Glades – Exclusive Offer</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                This promotion is only applicable for NEW sign up
              </td>
            </tr>									<tr valign="top">			<td width="3%">&nbsp;</td>              <td width="3%">ii)</td>              <td width="94%">     			Customers will be entitled to the promotion when they sign up for any of the Fibre Broadband plans.              			</td>            			</tr>									<tr valign="top">             			<td width="3%">&nbsp;</td>    			<td width="3%">iii)</td>   			<td width="94%">  			Early Termination Charges will apply if the customers decide to terminate their Fibre Broadband plans during the 24-month contract. Should subscribers terminate their subscription, regardless of any reason, they shall be liable for total monthly subscriptions for the remaining contract and termination penalties as followed:  			<br/>			<br/>				-	ASUS RT-AC 1200G+ Router: RM439 (w/GST)			<br/>			<br/>			</td>            			</tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                SDS Distribution (authorised dealer of Asus Router in Malaysia) is the partner of ViewQwest of this promotion and is responsible for all device warranty.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
				ViewQwest reserves the right to amend the Terms & Conditions and reject any application without prior notice.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                  Requests stated in the Comments section are subjected to management approval, and can be rejected without prior notice.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                 Other Terms &amp; Conditions apply
              </td>
            </tr>
  			</table>
        </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>	   	  <tr valign="top" > 	  <td>&nbsp;</td>        	  <td>&nbsp;</td>     	  </tr>	 	  <tr valign="top" > 	  <td>&nbsp;</td> 	  <td>&nbsp;</td>    	  </tr>	  <br/>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">d)</td>
              <td colspan="2">Service Package</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                 Unless other specified by us, ViewQwest Fibre Broadband service will be provided to subscriber for 24 months, and thereafter the subscription will continue automatically upon subscription expiry. ViewQwest is not liable for the change in the subscription amount, based on the prevailing No Contract Plan, upon subscription expiry should subscriber choose not to re-contract after 21 months into the contract. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                  Contract period will commence upon date of service activation. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
              		Subscribers may upgrade their service before 21st month of the contract period, only to a plan with a higher monthly subscription fee. Subscribers are not allowed to downgrade their service during the contract period. Once the upgrade is approved and processed, the original contract period will end and reset. The new contract period will commence from the date of service activation of the upgraded service for another 24 months. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                Subscribers may re-contract their service after 21st month of the contract period, to a plan of a same or higher monthly subscription fee. Subscribers are not allowed to downgrade their service during the contract period. Once the re-contract is approved and processed, the original contract period will end and reset. The new contract period will commence from the date of service activation of the upgraded service for another 24 months.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                  Subscribers may downgrade, re-contract or upgrade their service only after completing the whole contract period. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
				ViewQwest will not be liable for any damages including loss of profits, revenue, business and anticipated savings for any service interrupted due to reasons beyond ViewQwest’s control but not limited to any event of force majeure. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                ViewQwest will not be responsible for the performance of the subscriber’s wireless connection speed. The wireless connection is subjected to environmental factors within the home, which include but not limited to walls, electronic devices, pillars and mirror. Wireless connection may fluctuate due to the factors mentioned above, which are not within ViewQwest’s control. ViewQwest shall not be liable for any loss or damage due to such fluctuation. Subscriber is solely responsible for providing necessary equipment at his or her own cost to extend wireless coverage should the need arise.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">e)</td>
              <td colspan="2">ViewQwest Service Installation</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                  Should there be issues with the Fibre Termination Point or the Digital Resident Gateway, ViewQwest undertakes to liaise with Fibre@Home to resolve the issues, but is not responsible and accountable for any delay in delivery of ViewQwest service as a result of the issues.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                 Should the issue pertaining to Fibre Termination Point or the Digital Resident Gateway persists more than 15 days from the day the issue is reported, customer can choose to cancel ViewQwest service with no penalty fee. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                  If installation has been selected, subscriber or authorized representative has to be present during the scheduled installation appointment date and time at the service installation address. Failure to be available on the date of ViewQwest onsite installation appointment will result in subscriber being subjected to the miscellaneous charge stated in Table 1(b).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                 To postpone ViewQwest onsite installation, subscriber will have to notify ViewQwest 5 working days in advance of the confirmed ViewQwest onsite installation appointment date. Subscriber is allowed to postpone the onsite installation date, as long as it is within 14 days of the initial onsite installation appointment date. Postponing of onsite installation for more than 30 days of the initial onsite installation date is subjected to management’s approval. Should the postponement be approved, it will be at the discretion of ViewQwest to charge the subscriber the monthly subscription for the uninstalled period.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                 Should there be any additional installation or material required, it will be at the discretion of the subscriber and are not reimbursable by ViewQwest.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                Subscriber is to provide ViewQwest authorised personnel with assistance, co-operation, facilities and environmental conditions for installation of ViewQwest service. Such facilities include but not limited to housing of equipment, secure and constant electrical supply, back-up electrical supply and electrical necessities needed to enable the installation and activation of ViewQwest service.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                The ONU is under warranty for the entire duration of the subscription. In case of loss, defects, damage, misuse, acts of God, accident or unauthorised alteration/repair, replacement of the ONU will be chargeable.
              </td>
            </tr>
      	</table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">              <td width="3%">&nbsp;</td>              <td width="3%">viii)</td>              <td width="94%">                ViewQwest reserves the right to replace any hardware, with an equivalent in terms of performance, without prior notice to the subscriber.               </td>            </tr>			<tr valign="top">              <td width="3%">&nbsp;</td>              <td width="3%">x)</td>              <td width="94%">				ViewQwest will not be responsible for routers purchased from third parties nor internal wiring related to the provision of ViewQwest services.               </td>            </tr>			<tr valign="top">              <td width="3%">&nbsp;</td>              <td width="3%">xi)</td>              <td width="94%">               In the event that a subscriber opts to use an existing router that is supported (please refer to a list of supported routers: <a href="http://faq.viewqwest.com)">http://faq.viewqwest.com)</a>, ViewQwest undertakes to install it free of charge. If the router is not a supported model, ViewQwest undertakes to install it on a best-effort basis, and will charge a fee upon successful installation. A successful installation is defined as the availability of Internet access via the router’s Ethernet port(s) as well as internet access via WiFi (if available on the router).              </td>            </tr>
          </table>
        </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>	  	  <tr valign="top" >        <td>&nbsp;</td>        <td>&nbsp;</td>      </tr>	  <tr valign="top" >        <td>&nbsp;</td>        <td>&nbsp;</td>      </tr>	  
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">f)</td>
              <td colspan="2">Billing and Payments</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                 Subscriber will be charged according to the service subscribed and ViewQwest will bill the subscriber according to the billing cycle relevant to the subscriber’s account.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
				Selected payment plan (i.e. monthly or annual billing) in the endorsed Service Application and Agreement Form is effective for the whole contract term indicated herein and will continue automatically upon subscription expiry unless this contract is replaced by a new service upon re-contract or by a new service of greater value for contract duration less than 21 months. No change in payment plan is allowed during the contract term. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
				Billing will be monthly in advance for subscriber who opted for the monthly payment plan. The subscriber hereby agrees to pay ViewQwest for all charges incurred on their monthly invoice. All charges are calculated and billed as per listed here (valid at the time signing this form).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                Subscriber is required to check the invoices sent to the registered email periodically. However, issuance of invoice is not compulsory for subscriber to make payment to ViewQwest.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                  Bills are to be settled full within 7 net days from the bill date, as stated in the invoice. Failure to do so will result in suspension or termination of service registered under the subscriber’s account, until full payment is made. 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                Subscriber is responsible for verifying the accuracy of the bill and informing ViewQwest of any discrepancy within 7 net days from the bill date, failing which subscriber will be deemed to have accepted the bill as correctly rendered and final.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                If there is any dispute in the fees stated in the bill, subscriber must inform ViewQwest’s billing department in writing within 7 net days from bill date. Upon acknowledging the dispute, ViewQwest will investigate the dispute and provide a response to the subscriber as soon as possible, and the decision will be conclusive and binding upon the subscriber. If the dispute is resolved in favour of ViewQwest, subscriber is liable to pay the disputed amount immediately. 
              </td>
            </tr>
         </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">viii)</td>
              <td width="94%">
                Subscriber is responsible for verifying the accuracy of the payment. Payment is done through deduction of subscription from the submitted Credit or Debit Card, or such other mode of payment as may be made available by ViewQwest from time to time. 
              </td>
            </tr>
      	 </table>
         </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">g)</td>
              <td colspan="2">Service Suspension and Termination</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
				Service Suspension 
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="80px;" valign="middle">
			   -	Service registered under the subscriber may be suspended due to non-payment or request by the customer. <br/>				-	If subscriber fails to settle 2 bills consecutively, an email will be sent to the subscriber as a reminder for the missed payment. <br/>				-	Subscriber may request to extend the due date or pay on a later date as reviewed and approved by ViewQwest on a case to case basis. <br/>				-	If subscriber fails to settle the total amount within 3 days from the date of email or fails to pay on the later date, ViewQwest will contact the subscriber to inform about the suspension.<br/>				-	If there is no response by the subscriber, the service registered under the subscriber’s account will be suspended without prior notice. <br/>				-	If no payment is made within 14 days after the suspension, the service registered under the subscriber will be terminated with no prior notice and subscriber shall not have objection to the termination. ViewQwest might undertake necessary legal action against the subscriber to recover the amount due by the subscriber. <br/>				-	The service registered under the subscriber will be fully restored only after subscriber has paid fully the total amount owing to ViewQwest.<br/>						   -	The remaining contract term will be extended by the number of months the service was suspended.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                  Service Termination
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="80px;" valign="middle">
                	-	Service registered under the subscriber may be terminated with no prior notice due to non-payment, due to notice from subscriber to terminate the service with ViewQwest or any other reasons as a result of subscriber’s negligence for the improper use of the service. <br/>					-	Should there be any remaining free months of subscription in the case of early termination, subscriber will not be entitled to those months and is liable to reimburse ViewQwest for the total monthly subscriptions.  <br/>					-	A reactivation charge must be paid in advance should the services be required again. <br/>					-	Subscriber who wishes to terminate the service shall inform ViewQwest’s Customer Service 7 days in advance of termination. The termination will be done within 3 to 5 working days upon return of all hardware or equipment provided in good working order and condition to ViewQwest’s showroom at Suite 3-3A &amp; 5, Lower Level 3, The Horizon Annexe, Avenue 7, Bangsar South, No.8, Jalan Kerinchi, 59200 Kuala Lumpur, failing which ViewQwest may in its option deem the service (and any plans thereof) to be continuing at the full stipulated charges.<br/>					-	Subscriber who wishes to terminate the service before the 24th month will be liable to early termination charges such as port disconnection fee, total monthly subscriptions for the remaining months and termination penalties if applicable. <br/>					-	Subscriber shall return all leased hardware or equipment in good working order and condition to ViewQwest, failing which, the equipment will be considered lost and the subscriber shall be liable to pay the applicable charges as stated herein. 					<br/>
              </td>
            </tr>
      	 </table>
         </td>
      </tr>
      <tr valign="top" >
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div><!-- //terms-contents -->
</div><!-- //terms-wrapper -->

</body>
</html>
