<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
}
thead {
	background-color: #eeeeee;
}
tbody {
	background-color: #FFF;
}
th, td {
	padding: 3pt;
}
table.collapse {
	border-collapse: collapse;
	border: 1px solid black;
}
table.collapse td {
	border: 1px solid black;
	background-color: #fff;
}
.tbl_title {
	background-color: #CEE5ED;
	text-align:center;
	padding-top:10px;
	padding-bottom:10px;
	font-weight:bold;
	font-size: 18px;
}
/** TERMS and CONDITIONS **/
.terms-wrapper{
	width: 100%;
	font-size: 12px;
	line-height: 16px;
}
.terms-points{
	font-weight: bold;
}
.terms-contents a{
	vertical-align: top;
}
.terms-points-text li{
	vertical-align: top;
}
.headerBold{
	font-weight: bold;
}
ol#termsOLLI{
	vertical-align: top;
}

</style>

</head>

<body>
<h2>VQ Online Application Form</h2>
<hr>
<br />
<div style="page-break-after: always;">
<table class="collapse" cellpadding="4" cellspacing="0" width="100%">
  <tr>
    <th colspan="2" class="tbl_title"><strong>SUBSCRIBER'S PARTICULARS</strong></th>
  </tr>
  <tr>
    <td width="25%">Salutation:</td>
    <td width="75%"><?php echo $vq_salutation; ?></td>
  </tr>
  <tr>
    <td>First Name:</td>
    <td><?php echo $vq_first_name; ?></td>
  </tr>
  <tr>
    <td>Last Name:</td>
    <td><?php echo $vq_last_name; ?></td>
  </tr>
  <tr>
    <td>NRIC / Passport:</td>
    <td><?php echo $vq_nric_passport; ?></td>
  </tr>
  <tr>
    <td>Address:</td>
    <td><?php echo $vq_address; ?></td>
  </tr>
  <tr>
    <td>Nationality:</td>
    <td><?php echo $vq_nationality; ?></td>
  </tr>
  <tr>
    <td>Gender:</td>
    <td><?php echo $vq_gender; ?></td>
  </tr>
  <tr>
    <td>Date of Birth:</td>
    <td><?php echo $vq_dob; ?></td>
  </tr>
  <tr>
    <td>Email:</td>
    <td><?php echo $vq_email; ?></td>
  </tr>
  <tr>
    <td>Mobile Number:</td>
    <td><?php echo $vq_mobile; ?></td>
  </tr>
  <tr>
    <td>Home Number:</td>
    <td><?php echo $vq_phone; ?></td>
  </tr>
	<tr>
    <td>Subscribed Plan:</td>
    <td>
			<?php if($vq_plan != ""){ ?>
      <div>
				<h4 style="margin-top:0px; margin-bottom:5px;"><?= $vq_plan_name ?></h4>
				<?= $vq_plan_desc ?>
			</div>
      <?php }else {?>
      <div>
        <h4 style="margin-top:0px; margin-bottom:5px;">N/A</h4>
      </div>
      <?php } ?>
    </td>
  </tr>
	<tr>
		<td>Promo Offer</td>
		<td>
			<?php
			echo $vq_promo;
		?>
		</td>
	</tr>

  <tr>
    <td>Hardware Top-Up</td>
    <td>
    	<?php
			if($vq_routerLabel == ""){
				echo "N/A";
			}
			else{
				echo $vq_routerLabel." (Top-up: ".$vq_routerPrice.")";
			}
		?>
   	</td>
  </tr>
  <!--
  <tr>
    <td>FTP Installation Charge</td>
    <td><?php //echo $vq_ftpCharge; ?></td>
  </tr>
  -->
  <tr>
    <td>Service Activation Fee</td>
    <td><del>$53.50</del> <strong>WAIVED</strong></td>
  </tr>
  <tr id="oneVoiceRow">
    <td>OneVoice</td>
    <td>
    	<div>
		<?php
		if($vq_oneVoice != "" && $vq_numNonDisplay != ""){
			echo "OneVoice @ $3.95"."<br>";
			echo "Number Non-Display @ $5.35"."<br>";
			echo "OneVoice One Time Charge @ $50"."<br>";
		}
		elseif($vq_oneVoice != ""){
			echo "OneVoice @ $3.95"."<br>";
			echo "OneVoice One Time Charge @ $50"."<br>";
		}
		elseif($vq_numNonDisplay != ""){
			echo "Number Non-Display @ $5.35"."<br>";
		}
		else{
			echo "N/A"."<br>";
		}
		?>
        </div>
    </td>
  </tr>
  <tr>
    <td>Fiber Guard</td>
    <td>
    <?php
		if ($vq_fiberGuard == "fiberGuardMod"){
			echo "FIBER GUARD @ $19.95/mth (Moderate)";
		}
		elseif($vq_fiberGuard == "fiberGuardHigh"){
			echo "FIBER GUARD @ $19.95/mth (High)";
		}
		else{
			echo "N/A";
		}

	?>
    </td>
  </tr>
  <tr>
    <td>ViewQwest TV Media Player</td>
    <td>
    	<?php 
			if($vq_mediaPlayer == "" && $vq_mediaPlayer2 == "") {
				echo "N/A";
			}
			if($vq_mediaPlayer != ""){
				echo "ViewQwest TV Media Player @ $188";
			}
			
			if($vq_mediaPlayer2 != ""){
				echo "ViewQwest TV Media Player with Daiyo Antenna Bundle @ $198";
			}
		?>
   	</td>
  </tr>
	<tr>
    <td>Eleven Sports Network Subscription</td>
    <td>
			<?php
				if($vq_elevenSportsNetwork != ""){
					if($vq_plan == "1Gbps $49.90"){
							echo "6 Months Free Subscription to Eleven Sports Network ($19.90/mth thereafter)";
					}
					else if($vq_plan == "2Gbps-SN $69.90"){
						echo "9 Months Free Subscription to Eleven Sports Network ($19.90/mth thereafter)";
					}
					else {
						echo "Monthly Subscription to Eleven Sports Network ($19.90/mth)";
					}
				}
				else{
					echo "N/A";
				}
			?>
   	</td>
  </tr>
  <tr>
    <td>Installation Charge</td>
    <td>
    	<?php if($vq_installationCharge != ""){
			echo "Installation Charge @ $80 per trip";
		}
		else{
			echo "N/A";
		}
		?>
   	</td>
  </tr>
  <tr>
    <td>Remarks</td>
    <td>
    	<?php if($vq_remarksField != ""){
			echo $vq_remarksField;
		}
		else{
			echo "N/A";
		}
		?>
   	</td>
  </tr>
	<tr>
		<td>Watch Netflix</td>
		<td>
		<?php
			if($vq_netflix == "YES"){
				if($vq_netflixOption == "USA")
					echo "Netflix USA";
				else
					echo "Netflix Singapore";
			}
			else{
				echo "N/A";
			}
		?>
		</td>
	</tr>

  <!--
  <tr>
    <td>Referral NRIC</td>
    <td>
		<?php //echo $vq_referralNric;?>
   	</td>
  </tr>

  <tr>
    <td>Installation Date</td>
    <td>
		<?php //echo $vq_installationDate;?>
   	</td>
  </tr>
  <tr>
    <td>Installation Time Slot</td>
    <td>
		<?php //echo $vq_installationTimeSlot;?>
   	</td>
  </tr>
  -->
</table>
</div>
<br>
<?php include './includes/upfrontProductRatePlan.php'; ?>
</body>
</html>
