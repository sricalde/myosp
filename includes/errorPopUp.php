<div id="proceedToPayment" title="Proceed to Payment" style="display:none;">
    <div style="font-size:15px; line-height:25px;">We will now redirect you to payment page.  Please note that clicking the BACK BUTTON will remove all the details that you have entered.</div>
    <br>
    <div style="font-size:15px; line-height:25px;">Click OK to proceed or click X to cancel.</div>
</div>

<div id="uploadSuccess" title="Upload Successful" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Your file has been uploaded.</div>
</div>

<div id="fileExists" title="File Exists" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Sorry, file already exists.  Please upload another file.</div>
</div>

<div id="fileTooLarge" title="File Too Large" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Sorry, your file is too large for upload.  File should not exceed 5mb in size.</div>
</div>

<div id="fileTooLarge2" title="Upload Not Responding" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Sorry, upload is taking too long to respond.  Could be your file is too large for upload.  File should not exceed 5mb in size.</div>
</div>

<div id="wrongFileFormat" title="Wrong File Format" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>
</div>

<div id="acceptTerms" title="Accept Terms and Conditions" style="display:none;">
    <div style="font-size:15px; line-height:25px;">You must accept our Terms and Conditions to proceed.</div>
</div>

<div id="uploadNRIC" title="Upload NRIC" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Please upload your NRIC (Front and Back) to proceed.</div>
</div>

<div id="uploadNRICfront" title="Upload NRIC (FRONT)" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Please upload your NRIC (FRONT) to proceed.</div>
</div>

<div id="uploadNRICback" title="Upload NRIC (BACK)" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Please upload your NRIC (BACK) to proceed.</div>
</div>

<div id="acceptTermsandUploadNRIC" title="ERROR" style="display:none;">
    <div style="font-size:15px; line-height:25px;">Please accept our Terms and Conditions to proceed.</div>
</div>
