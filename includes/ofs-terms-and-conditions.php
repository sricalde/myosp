<div class="terms-wrapper">
  <div class="terms-contents">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <!-- TERMS OF SERVICE -->
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">1.</td>
              <td colspan="2">Terms of Service – ViewQwest Fibre Broadband Bundle</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">a)</td>
              <td colspan="2">Service Application</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                  Subscriber must be 18 years old and above to be eligible for the services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                The following original documents must be submitted to ViewQwest during registration via online signup portal:
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
                  - A copy of your photo identification document (front and back) as indicated in the online signup portal (the subscriber must be Singaporean or Permanent Resident)<br />
                  - If the subscriber is a foreigner, the subscriber must be an Employment Pass Holder, Work Permit Holder, Student Pass Holder, Long-Term Visit Pass or Dependent Pass Holder (with a minimum validity period of 12 months)<br />
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                ViewQwest reserves the right to modify or amend the Terms and Conditions in the Service Application and Agreement Form and any other agreements made with the subscriber.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                  ViewQwest’s General Terms & Conditions can be downloaded from our website, <a href="http://www.viewqwest.com/subscribercare/Viewqwest_TermsAndConditions.pdf" target="_blank"> http://www.viewqwest.com/subscribercare/Viewqwest_TermsAndConditions.pdf</a>.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">b)</td>
              <td colspan="2">Fees and Charges</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                All amounts quoted in this contract are in Singapore dollars, and include prevailing GST or other government charges.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                By registering for ViewQwest service, subscriber is liable for the following charges , unless expressly waived by ViewQwest:
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
                  -	One-Time Registration Fees (if any); and<br>
                  -	Any relevant taxes payable on the service(s) including without limitation, service tax or goods and service tax or other taxes and charges and shall indemnify ViewQwest for payment of the same; and<br>
                  -	If the service package is bundled with equipment, subscriber is subjected to monthly or upfront payment in accordance with the relevant service package; and<br>
                  -	Other miscellaneous charges as determined by ViewQwest<br>
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                For all Fibre Broadband plan, subscriber will be charged any miscellaneous fees, including without limitation the following, where applicable and may be incurred by subscriber, in respect of each Service Address:
              </td>
            </tr>
        	</table>
         <tr>
            <td colspan="2">
             <table width="100%" border="1" style="border-collapse:collapse">
               <tr>
                 <td width="77%" height="40" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Description of One Time Service Charges</td>
                 <td width="23%" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Charges (Inclusive of 7 % GST)</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Weekdays 6pm-8pm and Saturday installations for all Fibre Broadband Plan</td>
                 <td style="padding-left:14px;">$80</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Weekdays 9am-6pm installations for 1Gbps Fibre Broadband Plan</td>
                 <td style="padding-left:14px;">$80</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Failure to submit all documentation and to confirm an installation date within 2 weeks from submission of this form</td>
                 <td style="padding-left:14px;">$150</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Successful Installation of non-ViewQwest supported router</td>
                 <td style="padding-left:14px;">$80</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Reactivation Charge</td>
                 <td style="padding-left:14px;">$79.25</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Relocation Charge</td>
                 <td style="padding-left:14px;">$107</td>
               </tr>
               <tr>
                 <td style="padding-left:14px;">Dispatch service to perform 1-to-1 exchange for ViewQwest 4K Media Player at the subscriber’s premises (per instance)</td>
                 <td style="padding-left:14px;">$50</td>
               </tr>
             </table>
           </td>
         </tr>
         <tr>
               <td colspan="2">
                <table width="100%" border="1" style="border-collapse:collapse">
                  <tr>
                    <td width="77%" height="40" align="left" valign="middle" style="font-weight:bold; padding-left:14px;">Description of Miscellaneous Charges (if applicable)</td>
                    <td width="23%" align="left" valign="middle" style="font-weight:bold; padding-left:14px;"><strong>Charges (Inclusive of 7% GST)</strong></td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Installation of NetLink Trust FTP Charge (High-Rise Residential Building)</td>
                    <td style="padding-left:14px;">Waived</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Installation of NetLink Trust FTP Charge (Landed Residential Premise)</td>
                    <td style="padding-left:14px;">$246.10</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Cancellation/ Modification of confirmed NetLink Trust FTP Appointment (High-Rise Residential Building)</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Cancellation/ Modification of confirmed NetLink Trust FTP Appointment (Landed Residential Premise)</td>
                    <td style="padding-left:14px;">$481.50</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Cancellation of Fibre less than 6 days prior to the installation date (High-Rise Residential Building)</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Cancellation of Fibre less than 6 days prior to the installation date (Landed Residential Premise)</td>
                    <td style="padding-left:14px;">$481.50</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Penalty Fee if Subscriber wishes to cancel after the NetLink Trust activation date has been booked by ViewQwest</td>
                    <td style="padding-left:14px;">$150</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Replacement of ONU in case of damage/failure to return upon contract termination</td>
                    <td style="padding-left:14px;">$267.50</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Late payment charge due to 3 consecutive declined charges by credit card within a 1 month period</td>
                    <td style="padding-left:14px;">$5</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Failure to confirm onsite installation date within 30 days after NetLink Trust activation date</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Cancellation of confirmed ViewQwest  onsite installation appointment after NetLink Trust activation</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Failure to be available during ViewQwest onsite installation appointment</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                  <tr>
                    <td style="padding-left:14px;">Failure to receive the equipment within 5 days after last delivery attempt by Courier Service</td>
                    <td style="padding-left:14px;">$235.40</td>
                  </tr>
                </table>
              </td>
            </tr>
        </td>
      </tr>
      <!-- Continuation of "b) Installation of Equipment" Table (Table separated for proper paging -->
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
               ViewQwest shall be entitled to revise the charges shown above without prior notice.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">c)</td>
              <td colspan="2">Online Flash Sale (2Gbps Single Network Fibre Broadband) </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                  Should subscribers terminate their subscription, regardless of any reason from the day of sign-up to contract expiry date, they shall be liable for termination penalties as followed. There will be no refunds on any fees or part thereof upon early termination of the contract.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
                  -	ASUS RT-AC1200G+ Router: $199 (w/GST)<br>
                  -	ASUS RT-AC88U Router: $429 (w/GST)<br>
                  -	ASUS RT-AC5300 Router: $469 (w/GST)<br>
                  -	ViewQwest 4K Media Player: $188 (w/GST)<br>
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                 This promotion is strictly for new customers only, and is not available to existing ViewQwest subscribers.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                  Customers will be entitled to the promotion when they sign up within the promotional period as indicated by ViewQwest.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                Chosen router is final and non-exchangeable for billing credits or any other products and services. Customer is limited to $100 off either ASUS RT-AC1200G+ Router, ASUS RT-AC88U Router or ASUS RT-AC5300 Router stated in the Service Application and Agreement Form for Online Flash Sale (2Gbps Single Network Fibre Broadband) as part of the bundle promotion. Price difference between all the options is non-exchangeable for billing credits or any other products and services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                Selected items will be delivered on day of installation for new 2Gbps Single Network Fibre Broadband sign-up. In any case of stock shortage, another appointment will be scheduled to deliver the items. ViewQwest reserves the right to replace the router model with another model of same value.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                Avertek (authorised dealer of Asus Router in Singapore) is the partner of ViewQwest of this promotion and is responsible for all device warranty.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                Customers will be entitled to the promotion only when they sign up via ViewQwest Online Application Portal (https://signup.viewqwest.com/). Promotion is not available via other sign-up channels.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">viii)</td>
              <td width="94%">
                Promotional price of $999 will be billed to the customer within 1 week after sign-up, before activation of ViewQwest fibre broadband service.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ix)</td>
              <td width="94%">
                12 months Freedom DNS is given free with every sign-up of Online Flash Sale (2Gbps Single Network Fibre Broadband). It is non-transferable, non-exchangeable or non-redeemable for cash or any other products or services. Freedom DNS is optional at $10.70/month upon end of 12 months free trial.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">x)</td>
              <td width="94%">
                12 months OneVoiceTM is given free with every sign-up of Online Flash Sale (2Gbps Single Network Fibre Broadband). It is non-transferable, non-exchangeable or non-redeemable for cash or any other products or services. OneVoiceTM is optional at $3.95/mth for a minimum contract of 12 months.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xi)</td>
              <td width="94%">
                One free unit of ViewQwest TV Media Player is given free with every sign-up of Online Flash Sale (2Gbps Single Network Fibre Broadband). It is non-exchangeable or non-redeemable for cash or any other products or services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xii)</td>
              <td width="94%">
                $100 off router selection is only applicable to ASUS RT-AC1200G+, ASUS RT-AC88U and ASUS RT-AC5300 routers. The $100 router discount is non-transferable, non-exchangeable or non-redeemable for cash or any other products or services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xiii)</td>
              <td width="94%">
                $235.40 of NetLink Trust installation is waived for customers staying in High-Rise Residential Building with every sign-up of Online Flash Sale (2Gbps Single Network Fibre Broadband). Customers staying in Landed Residential Premise will be required to pay for a balance of $246.10. Should there be no need for installation of NetLink Trust Fibre Termination Point, the waiver amount is non-exchangeable or non-redeemable for cash or any other products or services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xiv)</td>
              <td width="94%">
                  ViewQwest reserves the right to deduct the premium charges as shown above from the Credit/Debit card submitted should there be any early termination of their contract regardless of any reason.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xv)</td>
              <td width="94%">
                ViewQwest reserves the right to amend the Terms & Conditions and reject any application without prior notice.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xvi)</td>
              <td width="94%">
                Requests stated in the Comments section are subjected to management approval, and can be rejected without prior notice.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xvii)</td>
              <td width="94%">
                Other Terms & Conditions apply
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">d)</td>
              <td colspan="2">Service Package</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                Subscriber is required to make an advance payment for any One Time Registration Fees stated in the Service Application and Agreement Form. The One Time Registration Fees is non-refundable upon termination of the contract, regardless of remaining subscription period.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                Unless other specified by us, ViewQwest Fibre Broadband service will be provided to subscriber for 24 months, and thereafter the subscription will continue automatically upon subscription expiry. ViewQwest is not liable for the subscription amount upon subscription expiry should subscriber choose not to re-contract after 21 months into the contract.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                  Contract period will commence upon date of service activation.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                Subscribers may upgrade their service before 21st month of the contract period, only to a plan with a higher monthly subscription fee. Subscribers are not allowed to downgrade their service during the contract period. Once the upgrade is approved and processed, the original contract period will end and reset. The new contract period will commence from the date of service activation of the upgraded service for another 24 months.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                Subscribers may re-contract their service after 21st month of the contract period, to a plan of a same or higher monthly subscription fee. Subscribers are not allowed to downgrade their service during the contract period. Once the re-contract is approved and processed, the original contract period will end and reset. The new contract period will commence from the date of service activation of the upgraded service for another 24 months.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                Subscribers may downgrade, re-contract or upgrade their service only after completing the whole contract period.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                  All Value Added Services (except those with contractual period) can be subscribed to/terminated, at any point, regardless of the period of ViewQwest Fibre Broadband Service contract. Termination of any service has to be sent in to <a href="mailto:cs@viewqwest.com">cs@viewqwest.com</a>. Any verbal instruction shall not be honoured.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">viii)</td>
              <td width="94%">
                From time to time the premium may be out of stock on the day of installation of the service. In such cases, ViewQwest undertakes to obtain the item and provide it to the subscriber within a reasonable time. Service provision and billing will be unaffected by this delay.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ix)</td>
              <td width="94%">
                ViewQwest will not be liable for any damages including loss of profits, revenue, business and anticipated savings for any service interrupted due to reasons beyond ViewQwest’s control but not limited to any event of force majeure.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">e)</td>
              <td colspan="2">NetLink Trust and ViewQwest Service Installation</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                Should there be issues with the Fibre Termination Point, ViewQwest undertakes to liaise with NetLink Trust to resolve the issues, but is not responsible and accountable for any delay in delivery of ViewQwest service as a result of the issues.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                ViewQwest pays Netlink Trust upfront for the FTP installation and the subscriber commits to reimburse ViewQwest through the “Fibre Termination Point installation – Monthly installment plan”, if selected instead of the one-time payment. In the event that the subscriber terminates their subscription before the end of this contract, the remaining charges shall apply.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                If no onsite installation is confirmed prior to NetLink Trust activation, ViewQwest onsite service installation date must be booked within 30 days of the NetLink Trust activation date. Failure to confirm on the onsite installation date will subject subscriber to the miscellaneous charge as stated in Table 1(b).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                If installation has been selected, subscriber or authorized representative has to be present during the scheduled installation appointment date and time at the service installation address. Failure to be available on the date of ViewQwest onsite installation appointment will result in subscriber being subjected to the miscellaneous charge stated in Table 1(b).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                  There are no Sunday installations available.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                To postpone ViewQwest onsite installation, subscriber will have to notify ViewQwest 5 working days in advance of the confirmed ViewQwest onsite installation appointment date. Subscriber is allowed to postpone the onsite installation date, as long as it is within 30 days of the initial onsite installation appointment date. Postponing of onsite installation for more than 30 days of the initial onsite installation date is subjected to management’s approval. Should the postponement be approved, it will be at the discretion of ViewQwest to charge the subscriber the monthly subscription for the uninstalled period.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                Cancellation of confirmed ViewQwest onsite installation appointment after NetLink Trust activation date will subject subscriber to the miscellaneous charge as stated in Table 1(b).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">viii)</td>
              <td width="94%">
                Should there be any additional installation or material required, it will be at the discretion of the subscriber and are not reimbursable by ViewQwest.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ix)</td>
              <td width="94%">
                Subscriber is to provide ViewQwest authorised personnel with assistance, co-operation, facilities and environmental conditions for installation of ViewQwest service. Such facilities include but not limited to housing of equipment, secure and constant electrical supply, back-up electrical supply and electrical necessities needed to enable the installation and activation of ViewQwest service.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">x)</td>
              <td width="94%">
                The ONU is under warranty for the entire duration of the subscription. In case of loss, defects, damage, misuse, acts of God, accident or unauthorised alteration/repair, replacement of the ONU will be chargeable.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xi)</td>
              <td width="94%">
                ViewQwest reserves the right to replace any hardware, with an equivalent in terms of performance, without prior notice to the subscriber.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xii)</td>
              <td width="94%">
                ViewQwest will not be responsible for routers purchased from third parties nor internal wiring related to the provision of ViewQwest services.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xiii)</td>
              <td width="94%">
                In the event that a subscriber opts to use an existing router that is supported (please refer to a list of supported routers: http://faq.viewqwest.com), ViewQwest undertakes to install it free of charge. If the router is not a supported model, ViewQwest undertakes to install it on a best-effort basis, and will charge a fee upon successful installation. A successful installation is defined as the availability of Internet access via the router’s Ethernet port(s) as well as internet access via WiFi (if available on the router).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">xiv)</td>
              <td width="94%">
                 	Self-installation of equipment for 1Gbps Fibre Broadband sign-up:
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
						      -	After NetLink Trust activation date, relevant equipment will be delivered to customer for self-installation. Onsite installation is chargeable at $80 per trip for both weekdays and Saturday (i.e. ViewQwest engineer will be sent to premise to assist with installation).<br>
                  -	Should there be any high-loss issue (indicated by a red alarm light), customers are to call in to inform ViewQwest. ViewQwest will then arrange for an onsite visit to check on the issue. There will not be any onsite installation charge should it be genuine high loss issue.<br>
                  -	If equipment is faulty upon delivery, ViewQwest will arrange to replace the equipment. Arrangement to replace the equipment will not be chargeable.<br>
                  -	It will be at the discretion of ViewQwest to charge an onsite fee if the onsite is scheduled by the subscriber for rectification of issues that are not due to ViewQwest's fault.<br>
                  -	Customers will have to receive the equipment within 5 working days after the last delivery attempt by courier service. Failure to receive the items within 5 working days after last delivery attempt by courier service will subject subscriber to the miscellaneous charge stated in Table 1(b). Should customers wish to proceed with the installation, the equipment will have to be self-collected at ViewQwest Bukit Timah Showroom.<br>
                  -	NinjaVan is the official courier service of ViewQwest for the delivery of the items. Should you have any enquiries regarding the delivery of your item, kindly contact NinjaVan at <a href="mailto:support_sg@ninjavan.co">support_sg@ninjavan.co</a> or call +65 6602 8271.<br>
                  -	Tracking ID will be available for your information after your item is received by NinjaVan.<br>
                  -	ViewQwest only provides support for routers indicated in the Service Application and Agreement Form. No assistance will be provided for any other unsupported routers.<br>
                  -	Billing will start 1 day after successful delivery of the relevant equipment.<br>
                  -	ViewQwest will not be responsible whatsoever for delays in the delivery as handled by courier service.<br>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">f)</td>
              <td colspan="2">Billing and Payments</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                Promotional price of $999 is effective for the whole contract term indicated herein. Customers will be billed $79.90/mth upon subscription expiry unless this contract is replaced by a new service upon re-contract or by a new service of greater value for contract duration less than 21 months.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                Billing will be monthly in advance for subscriber who opted for the monthly payment plan. The subscriber hereby agrees to pay ViewQwest for all charges incurred on their monthly invoice. All charges are calculated and billed as per listed here (valid at the time signing this form).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iii)</td>
              <td width="94%">
                Subscriber is required to check the invoices sent to the registered email periodically. However, issuance of invoice is not compulsory for subscriber to make payment to ViewQwest.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">iv)</td>
              <td width="94%">
                Bills are to be settled full within 7 net days from the bill date, as stated in the invoice. Failure to do so will result in suspension or termination of service registered under the subscriber’s account, until full payment is made.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">v)</td>
              <td width="94%">
                Subscriber is responsible for verifying the accuracy of the bill and informing ViewQwest of any discrepancy within 7 net days from the bill date, failing which subscriber will be deemed to have accepted the bill as correctly rendered and final.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vi)</td>
              <td width="94%">
                If there is any dispute in the feels stated in the bill, subscriber must inform ViewQwest’s billing department in writing within 7 net days from bill date. Upon acknowledging the dispute, ViewQwest will investigate the dispute and provide a response to the subscriber as soon as possible, and the decision will be conclusive and binding upon the subscriber. If the dispute is resolved in favour of ViewQwest, subscriber is liable to pay the disputed amount immediately.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">vii)</td>
              <td width="94%">
                Subscriber is responsible for verifying the accuracy of the payment. Payment is done through deduction of subscription from the submitted Credit or Debit Card, or such other mode of payment as may be made available by ViewQwest from time to time.
              </td>
            </tr>
      	 </table>
         </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">g)</td>
              <td colspan="2">Service Suspension and Termination</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">i)</td>
              <td width="94%">
                  Service Suspension
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
						-	Service registered under the subscriber may be suspended due to non-payment or request by the customer.<br>
                  -	If subscriber fails to settle 2 bills consecutively, an email will be sent to the subscriber as a reminder for the missed payment.<br>
                  -	Subscriber may request to extend the due date or pay on a later date as reviewed and approved by ViewQwest on a case to case basis.<br>
                  -	If subscriber fails to settle the total amount within 3 days from the date of email or fails to pay on the later date, ViewQwest will contact the subscriber to inform about the suspension.<br>
                  -	If there is no response by the subscriber, the service registered under the subscriber's account will be suspended without prior notice.<br>
                  -	If no payment is made within 14 days after the suspension, the service registered under the subscriber will be terminated with no prior notice and subscriber shall not have objection to the termination. ViewQwest might undertake necessary legal action against the subscriber to recover the amount due by the subscriber.<br>
                  -	The service registered under the subscriber will be fully restored only after subscriber has paid fully the total amount owing to ViewQwest.<br>
                  -	Subscriber may choose to suspend the service for no more than six months within the contract term, after which the monthly subscription plan charges will resume as per normal.<br>
                  -	There will be a recurring charge of $16.05 (after GST) per month to the monthly bill during the requested suspension period.<br>
                  -	The remaining contract term will be extended by the number of months the service was suspended.<br>
                  -	More Terms and Conditions can be found on the Temporary Suspension of Services Application Form.<br>
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">ii)</td>
              <td width="94%">
                  Service Termination
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">&nbsp;</td>
              <td width="3%">&nbsp;</td>
              <td width="94%" height="60px;" valign="middle">
						      -	Service registered under the subscriber may be terminated with no prior notice due to non-payment, due to notice from subscriber to terminate the service with ViewQwest or any other reasons as a result of subscriber's negligence for the improper use of the service.<br>
                  -	A reactivation charge must be paid in advance should the services be required again.<br>
                  -	Subscriber who wishes to terminate the service shall inform ViewQwest's Customer Service 7 days in advance of termination. The termination will be done within 3 to 5 working days upon return of all hardware or equipment provided (ONU, Patch Cable and Power Adaptor) in good working order and condition to ViewQwest's showroom at 200 Bukit Timah Road, Singapore 229862, failing which ViewQwest may in its option deem the service (and any plans thereof) to be continuing at the full stipulated charges.<br>
                  -	Subscriber who wishes to terminate the service before the 24th month will be liable to early termination charges such as port disconnection fee, total monthly subscriptions for the remaining months and termination penalties if applicable.<br>
                  -	Subscriber shall return all leased hardware or equipment (ONU, Patch Cable and Power Adaptor) in good working order and condition to ViewQwest, failing which, the equipment will be considered lost and the subscriber shall be liable to pay the applicable charges as stated herein.<br>
                  -	In the event subscriber terminates the Fibre Broadband Service registered under the subscriber, all Value Added Services shall also automatically be terminated. Subscriber shall be liable to pay all termination charges involved, if applicable.<br>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">2.</td>
              <td colspan="2">Terms of service – Eleven Sports Network Subscription</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
               <td width="3%">a)</td>
               <td colspan="2">
                  Free 6 months subscription is applicable for 1Gbps Fibre Broadband (24 months) new and recontract subscriptions only.  Free 9 months subscription is applicable for 2Gbps Single Network Fibre Broadband (24 months) new and recontract subscriptions only.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">b)</td>
               <td colspan="2">
                  For 1Gbps subscription, free 6 months will be available from the 1st to 6th month, thereafter there will be a charge of $19.90/month from 7th to 12th month. For 2Gbps Single Network subscription, free 9 months will be available from the 1st to 9th month, thereafter there will be a charge of $19.90/month from 10th to 12th month.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">c)</td>
               <td colspan="2">
                  Should there be termination of this service for any reason before the expiry of the agreed contract period of 12months will result in the subscriber being liable to pay the remainder of the contract fee until the contract expiry date.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">d)</td>
               <td colspan="2">
                  12months Eleven Network subscription will commence from the date that your service is activated which will be informed via email.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">e)</td>
               <td colspan="2">
                  Details on how to access Eleven Network subscription will be indicated in your Ready for service email.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">f)</td>
               <td colspan="2">
                  Eleven Network subscription is non-exchangeable for cash or kind, is non-refundable and non-transferrable.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">g)</td>
               <td colspan="2">
                  Each subscriber is entitled to sign up for 1 new Eleven Sports account for each Eleven Sports Network Subscription.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">h)</td>
               <td colspan="2">
                  There will not be any refund or cancellation for any delayed match streaming.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">i)</td>
               <td colspan="2">
                  You are entitled for 3 LIVE and 3 delayed matches per match week.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">j)</td>
               <td colspan="2">
                  For technical support, you can contact Eleven Sports via email at info@elevensportsnetwork.sg, live support chat on <a href="http://www.elevensports.sg" target="_blank">http://www.elevensports.sg</a> and hotline number 31588848 (Operating Hours: Weekdays 3pm to 1am and Weekends 9am to 1am).
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">k)</td>
               <td colspan="2">
                  Please visit <a href="www.elevensports.sg/guide" target="_blank">www.elevensports.sg/guide</a> for more details on programme listing.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">l)</td>
               <td colspan="2">
                  ViewQwest reserves the right to amend the Terms & Conditions and reject any application without prior notice.
               </td>
            </tr>
            <tr valign="top">
               <td width="3%">m)</td>
               <td colspan="2">
                  m)	Other terms and conditions apply.
               </td>
            </tr>
      	</table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">3.</td>
              <td colspan="2">Terms of service – Refer-A-Friend Program</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">a)</td>
              <td colspan="2">
                  Referral program applies when an existing customer refer a new customer to sign up for any 2-year ViewQwest fibre broadband plan.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">b)</td>
              <td colspan="2">
                  A new customer can only be referred once, regardless of the number of service activation addresses.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">c)</td>
              <td colspan="2">
                  Customer cannot be referred by himself or herself, or by any existing ViewQwest employee.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">d)</td>
              <td colspan="2">
                  Referral section must be filled up with valid name and NRIC of existing customer.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">e)</td>
              <td colspan="2">
                  $40 ViewQwest E-store voucher for new and existing customer will be issued via email upon service activation of the new customer.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">f)</td>
              <td colspan="2">
                  $40 ViewQwest E-store voucher is applicable with a minimum spending of $100 within a single cart on ViewQwest E-store.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">g)</td>
              <td colspan="2">
                  Only one ViewQwest E-store voucher per order is allowed.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">h)</td>
              <td colspan="2">
                  Subscription for any 2-year ViewQwest fibre broadband plans are subjected to other Terms and Conditions as stated in the application form.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">i)</td>
              <td colspan="2">
                  Terms & Conditions for ViewQwest E-store voucher apply.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">j)</td>
              <td colspan="2">
                  ViewQwest E-store Terms & Conditions apply.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">k)</td>
              <td colspan="2">
                  ViewQwest reserves the right to amend the Terms & Conditions.
              </td>
            </tr>
			</table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">4.</td>
              <td colspan="2">Terms of service – OneVoice&#8482 Residential</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">a)</td>
              <td colspan="2">
              		Subscribers must commit to OneVoice&#8482 for a minimum of 12 months. Termination of OneVoice&#8482 for any reason before 12 months is completed will result in the subscriber being liable to pay the remainder of the OneVoice&#8482 contract service fee. 30 days written notice must be submitted for any termination.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">b)</td>
              <td colspan="2">
              		All incoming and outgoing local calls are free.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">c)</td>
              <td colspan="2">
              		IDD services are activated on OneVoice&#8482 plans by request.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">d)</td>
              <td colspan="2">
            		In the event that a subscriber decides to take up the OneVoice&#8482 service despite having less than 12 months remaining in their ViewQwest contract, the subscriber is still liable to pay the remainder of the OneVoice&#8482 contract service fee upon termination of the subscription.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">5.</td>
              <td colspan="2">Terms of service – ViewQwest 4K Media Player</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">a)</td>
              <td colspan="2">
              		Warranty period is 1 year from the date of commencement of this contract.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">b)</td>
              <td colspan="2">
              		Any hardware fault within this 1 year warranty period entitles the subscriber to a 1-to-1 exchange of the Viewqwest 4K Media Player. To utilize the 1-to-1 exchange, the ViewQwest 4K Media Player, with full set of accessories and box, must be brought to ViewQwest's showroom at 200 Bukit Timah Road, Singapore 229862.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">c)</td>
              <td colspan="2">
              		A chargeable dispatch service is also available wherein ViewQwest undertakes to perform the 1-to-1 exchange at the subscriber's premises (as stated in clause 2c).
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">d)</td>
              <td colspan="2">
              		The ViewQwest 4K Media Player device allows access to media channels only. The availability, quality and content of the media channels is the sole responsibility of the channel owners.
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">6.</td>
              <td colspan="2">Terms of Service - Freedom DNS</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">a)</td>
              <td colspan="2">
            		Freedom DNS grants access to certain geo-blocked websites around the world. ViewQwest neither owns nor is responsible for content on said websites. Content may be added/removed at any point at the discretion of the respective website/app owners.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">b)</td>
              <td colspan="2">
            		ViewQwest shall not be responsible for any fees incurred or subscriptions to any website/app accessed through Freedom DNS.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">c)</td>
              <td colspan="2">
              		The listed websites/apps accessible through Freedom DNS are subject to change. ViewQwest reserves the right to add/remove support for any website/app at any time without prior notification.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">d)</td>
              <td colspan="2">
              		Termination of Freedom DNS after the trial period, if any, has to be sent in to <a href="mailto:cs@viewqwest.com">cs@viewqwest.com</a>. Any verbal instruction shall not be honoured.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top" style="font-weight:bold;">
              <td width="3%">7.</td>
              <td colspan="2">Terms of Service – Fiber-Guard (Internet Parental Control)</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:justify;">
            <tr valign="top">
              <td width="3%">a)</td>
              <td colspan="2">
              		Subscriber needs to indicate in the Service Application and Agreement Form if Fiber-Guard (Internet Parental Control) is required.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">b)</td>
              <td colspan="2">
              		Fiber-Guard is intended to block undesirable websites and content only. Freedom DNS will not be applicable should subscriber choose to opt for Fiber-Guard.
              </td>
            </tr>
            <tr valign="top">
              <td width="3%">c)</td>
              <td colspan="2">
              	"Moderate" and "High" refer to the number of different categories of websites that are being blocked - for more information and the full list of categories being blocked, please visit <a href="http://www.viewqwest.com/faq.php" target="_blank">http://www.viewqwest.com/faq.html</a>.
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div><!-- //terms-contents -->
</div><!-- //terms-wrapper -->
