<?php session_start(); ?>
<!DOCTYPE html>

<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/verify.notify.min.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
		  <script src="js/pageLoader.js"></script>

		  <link href="css/pageLoaderLoadingBar.css" rel="stylesheet" type="text/css" />
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <style type="text/css">
		  <!-- CSS for Terms and Conditions -->
		  thead {
				background-color: #eeeeee;
			}
			tbody {
				background-color: #FFF;
			}
			th, td {
				padding: 3pt;
			}
			table.collapse {
				border-collapse: collapse;
				border: 1px solid black;
			}
			table.collapse td {
				border: 1px solid black;
				background-color: #fff;
			}
			.tbl_title {
				background-color: #CEE5ED;
				text-align:center;
				padding-top:10px;
				padding-bottom:10px;
				font-weight:bold;
				font-size: 18px;
			}
			.terms-wrapper{
				width: 100%;
				font-size: 12px;
				line-height: 16px;
			}
			.terms-points{
				font-weight: bold;
			}
			.terms-contents a{
				vertical-align: top;
			}
			.terms-points-text li{
				vertical-align: top;
			}
			.headerBold{
				font-weight: bold;
			}
			ol#termsOLLI{
				vertical-align: top;
			}
			<!-- //CSS for Terms and Conditions -->

			@media only screen and (min-width: 481px) and (max-width: 640px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -280px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -189px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -136px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -82px;
			}
			#progressbar li:nth-child(4):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(8):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				background: #FFF;
			}
			#progressbar li:nth-child(7):after {
				background: #F15757;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4{
				display: none;
			}
			}
			@media only screen and (min-width: 321px) and (max-width: 480px) {
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -280px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -189px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -136px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -82px;
			}
			#progressbar li:nth-child(4):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(8):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				background: #FFF;
			}
			#progressbar li:nth-child(7):after {
				background: #F15757;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4{
				display: none;
			}
			}
			@media only screen and (max-width: 320px){
			.signupFlow{
				margin-top: 10px;
				text-align: center;
				margin: auto;
				margin-left: -280px;
			}
			#progressbar li:nth-child(1):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -189px;
			}
			#progressbar li:nth-child(2):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -136px;
			}
			#progressbar li:nth-child(3):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -82px;
			}
			#progressbar li:nth-child(4):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-right: -26px;
			}
			#progressbar li:nth-child(7):before {
				content: ".";
				background: none;
				color: #F15757;
				font-weight: bolder;
				font-size: 25px;
				margin-left: -26px;
			}
			#progressbar li:nth-child(2):after {
				content: none;
			}
			#progressbar li:nth-child(3):after {
				content: none;
			}
			#progressbar li:nth-child(4):after {
				content: none;
			}
			#progressbar li:nth-child(5):after {
				content: none;
			}
			#progressbar li:nth-child(7):after {
				content: none;
			}
			#progressbar li:nth-child(6):after {
				background: #FFF;
			}
			li#pb8 {
				width: 0px;
			}
			#pbtxt1, #pbtxt2, #pbtxt3, #pbtxt4, #pbtxt8{
				display: none;
			}
			}
		</style>

<span id="popupFunctions" style="display:none;"></span>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TF99FW');</script>
<!-- End Google Tag Manager -->
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="top-border"></div>
    <div class="signupFlow">
        <ul id="progressbar">
            <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
            
            <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
             <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
            <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
           <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
            <li id="pb7" class="active"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
            <li id="pb8"><span id="pbtxt8">SUCCESS</span></li>
            <div id="progressbarConnector"></div>
        </ul>
    </div><!-- //signupFlow -->
	
    <div class="wrapper">
		
		<div id="email-verification" class="termsConditionsWrapper">
			<h1>EMAIL VERIFICATION</h1>
			<span id="verify-token-div"></span>
			<p style="color: #ffffff;">A confirmation email has been sent. Please go to your inbox now, open the email and paste the CODE below.</p>
			<input class="registration-input-type" type="text" id="token-field" size="50" style="width: 150px;"/>
			<input type="button" id="verify_token" class="previewNextButton" value="Verify">
		</div>
    	<div id="t-c" class="termsConditionsWrapper">
            <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
            <div class="pageHeader">Residential Broadband Online Signup</div>
            <hr id="top-hr">
            <h1 style="text-align:center;">TERMS AND CONDITIONS</h1>
                <div class="termsWrapper" id="terms">
                    <script type="text/javascript">
                       
                        $("#terms").load( "./includes/terms-and-conditions.php" );
                       
                    </script>
                </div>
                <div class="terms-and-conditions-wrapper preview-plan-detail">
                    <div class="terms-and-conditions-checkbox" style="vertical-align:top; margin-top:5px;"><input type="checkbox" name="terms_and_conditions" value=""/></div>
                    <div class="terms-and-conditons-text">I have read and accept these Terms and Conditions, and hereby do consent to be charged by Sdn. Bhd. for services requested.</div>
                </div>
                <div id="errorPopUp" class="white-popup mfp-hide">
                    <script type="text/javascript">
                        $("#errorPopUp").load( "includes/errorPopUp.php" )
                    </script>
                </div>
           	<div class="termsNextButton">
            	<input type="button" id="next" class="previewNextButton" value="Next">
            </div>
	<input type="hidden" value="<?php echo $_SESSION['nric']; ?>" id="save_nric"/>
      	<!-- FEEDBACK FORM -->
         <div class="feedbackFormWrapper">
           <div class="feedbackFormDivider"></div>
           <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
           <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
           <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
              <div class="feedbackFormContainer" id="feedbackFormContent"></div>
           </div><!-- //feedbackFormContent -->
         </div><!-- //feedbackFormWrapper -->

      </div><!-- //termsConditionsWrapper -->
    </div><!-- //wrapper -->
	

</body>

<!-- FEEDBACK FORM -->
    <script type="text/javascript">
		function feedbackFormSubmit(){
			$('#feedbackFormGeneric').verify({
				if (result) {
					//$('#feedbackFormGeneric').submit(function(e) {
					var firstName = $('#firstName').val();
					var lastName = $('#lastName').val();
					var email = $('#email').val();
					var phone = $('#phone').val();
					var message = $('#message').val();
					var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
					$.ajax({
						url: "feedbackFormEngine.php",
						data: param,
						type: "POST",
						success:(function(data) {
							swal({
									title: "Thank You!",
									text: "Email sent successfullly.",
									showConfirmButton: false,
									type: "success"
								});
								//window.location.href=window.location.href;
						}),
						complete:function(){
							window.location.href=window.location.href;
						}
					})
				}
			})
		}

		$("#feedbackFormContent").load("feedbackForm.php");
		$('.feedbackFormLink').magnificPopup({
			type:'inline',
			midClick: true
		});
	</script>

    <script type="text/javascript">
        $("#popupFunctions").load( "js/popupFunctions.js" )
    </script>

    <script>
		$('.open-popup-link').magnificPopup({
		  type:'inline',
		  midClick: true
		});
	</script>
    <script>
		$(document).ready(function(){
		  $('input').iCheck({
			checkboxClass: 'icheckbox_flat-red',
			radioClass: 'iradio_flat-red'
		  });
		});
	</script>
<script>
	$(document).ready(function() {
		$("#t-c").css('display','none');
		$("#email-verification").css('display','block');
		$("#verify-token-div").html("");
		var email_sent = <?php echo $_SESSION['email_sent'] ?>;
		
		String.prototype.equalIgnoreCase = function(str) {
			return (str != null       && typeof str === 'string'&& this.toUpperCase() === str.toUpperCase());	
		}
		
		$('#verify_token').click(function(){
			var oldToken = "<?php echo $_SESSION['securitytoken']; ?>";
			var newToken = $("#token-field").val();
			if (oldToken.equalIgnoreCase(newToken)) {
				$("#t-c").css('display','block');
				$("#email-verification").css('display','none');
				console.log("Valid Token");
				$("#verify-token-div").html("");
				console.log(oldToken);
			} else {
				$("#t-c").css('display','none');
				$("#email-verification").css('display','block');
				$("#verify-token-div").html("<div style='color: red;'>Invalid Token</div>");
			   console.log("Invalid Token");
			   console.log(oldToken);
			}
		});
		var nric = $("#save_nric").val();
		// go to next page (Payment)
		$('#next').click(function(){
			if (($("input[name='terms_and_conditions']:checked").length)<=0) {
				acceptTerms();
			} else if (email_sent == 'false') {
				window.location.href= "callback/error.php";
				console.log(email_sent);
			} else {
				$.ajax({
					method: "POST",
					url: "callback/create_accnt.php",
					data: 'nric='+nric,
					dataType: "json",
					success: function (data) {
						if(data.responseCode == "success") {
							window.location.href= "callback/success.php";
							console.log(data.responseCode);
							// console.log(nric);
						} else {
							window.location.href= "callback/error.php";
							console.log(data.responseCode);
							// console.log(nric);
						}
					}
				});
			}
		});
});
</script>
</html>
