<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            #monthOnly .ui-datepicker-calendar {
                display: none;
            }
        </style>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="jquery-1.12.0.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="assets/icheck-1.x/icheck.js"></script>
        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
        <script src="js/custom/registration.js" type="text/javascript"></script> 
        <script src="js/verify.notify.min.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>

        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="js/jquery-ui.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/icheck-1.x/skins/flat/red.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <style>
            @media only screen and (min-width: 481px) and (max-width: 640px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -115px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -80px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(6):before {
                    width: 60px;
                    content: ". .";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -38px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    background: #F15757;
                }
                #pb8{
                    display: none !important;
                }
                #pbtxt1, #pbtxt2, #pbtxt7{
                    display: none;
                }
            }
            @media only screen and (min-width: 321px) and (max-width: 480px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -175px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -80px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(6):before {
                    width: 60px;
                    content: ". .";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -38px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(6):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    background: #F15757;
                }
                li#pb7{
                    width: 0px;
                }
                li#pb8{
                    display: none !important;
                }
                #pbtxt1, #pbtxt2, #pbtxt7{
                    display: none;
                }
            }
            @media only screen and (max-width: 320px){
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -185px;
                }
                #progressbar li:nth-child(1):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -80px;
                }
                #progressbar li:nth-child(2):before {
                    content: ".";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-right: -26px;
                }
                #progressbar li:nth-child(5):before {
                    width: 60px;
                    content: ". . .";
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -32px;
                }
                #progressbar li:nth-child(2):after {
                    content: none;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #progressbar li:nth-child(5):after {
                    content: none;
                }
                #progressbar li:nth-child(4):after {
                    background: #F15757;
                }
                li#pb6 {
                    width: 0px;
                }
                #pb7, #pb8{
                    display: none !important;
                }
                #pbtxt1, #pbtxt2, #pbtxt5, #pbtxt6{
                    display: none;
                }
            }
        </style>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TF99FW');</script>
        <!-- End Google Tag Manager -->

    </head>
    <body>

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div style="display:none;"><input type="text" id="customerUID" placeholder="Customer UID" size="50" value=""/></div>

        <!--
        Start of DoubleClick Floodlight Tag: Please do not remove
        Activity name of this tag: Signup Portal - Reg page testing
        URL of the webpage where the tag is expected to be placed: https://signup.viewqwest.com/registration.html
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 01/10/2017
        -->

        <script type="text/javascript">
            //UID Generator generates 12 AlphaNumeric character and google tag manager (GTM) will fetch the result for monitoring of singups.
            $.ajax({
                url: 'UIDGenerator.php',
                type: 'POST',
                data: "json",
                async: false,
                success: function (data)
                {
                    cusUID = data;
                    $("#customerUID").val(cusUID);
                    setVqValue('customerUID', $("#customerUID").val());
                    // console.log(getVqValue('customerUID'));
                }
            });

            var customerUID_GTM = getVqValue('customerUID');
            console.log(customerUID_GTM);

            var axel = Math.random() + "";
            var a = axel * 10000000000000;

            document.write('<iframe src="https://5691183.fls.doubleclick.net/activityi;src=5691183;type=resid002;cat=signu008;u2=' + customerUID_GTM + ';dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>

        <noscript>
        <iframe src="https://5691183.fls.doubleclick.net/activityi;src=5691183;type=resid002;cat=signu008;u2=" + customerUID_GTM + ";dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>
        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

        <div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1"><span id="pbtxt1">NEW SUBSCRIBER</span></li>

                <li id="pb3" class="active"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">SUCCESS</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
        <div class="wrapper">
            <div class="registrationWrapper">
                <div class="vq-logo"><img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto"></div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <h1 class="registration-h1">SUBSCRIBER'S DETAILS</h1>
                <div class="registration-left-panel">
                    <div>
                        <select class="registration-select" title="Salutation" id="name_salutationacc2" style="-moz-appearance:none;">
                            <option value="Mr.">Mr.</option>
                            <option value="Ms.">Ms.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Dr.">Dr.</option>
                            <option value="Prof.">Prof.</option>
                        </select>
                    </div>
                    <div>
                        <input class="registration-input-type" type="text" id="firstName" placeholder="First Name" size="50"/>
                        <input class="registration-input-type" type="text" id="lastName" placeholder="Last Name" size="50"/>
                        <div>
                            <select class="registration-select" id="nationality" style="-moz-appearance:none;">
                                <option value="" selected disabled>Residency Status</option>
                                <option value="Malaysian/Permanent Resident">Malaysian/Permanent Resident</option>
                                <option value="Foreigner">Foreigner</option>
                            </select>
                        </div>
                        <input class="registration-input-type" type="text" id="nric" size="50"/>
                        <input class="registration-input-type" type="text" id="dob" placeholder="Date of Birth (YYYY-MM-DD)" size="50" onkeydown="return false" onfocus="blur();"/>
                    </div>
                    <div>
                    </div>
                    <div class="registration-gender-input">
                        <div class="registration-gender-input-header">Gender:</div>
                        <div class="registration-input-radio">
                            <input checked="" type="radio" name="gender" value="Male"/> Male
                        </div>
                        <div class="registration-input-radio">
                            <input class="registration-input-radio" type="radio" name="gender" value="Female"/> Female
                        </div>
                    </div>
                    <div id="results" style="vertical-align: top; padding-left: 100px;"></div>
                </div><!-- registration-left-panel -->

                <div class="registration-right-panel">
                    <!--<div>
                        <select class="registration-select" id="reType">
                            <option value="" selected disabled>Residential Type</option>
                            <option value="COM">Commercial High Rise</option>
                            <option value="COL">Commercial Low Rise</option>
                            <option value="IND">Industrial</option>
                            <option value="LND">Landed</option>
                            <option value="HDB">HDB</option>
                            <option value="APT">Condominium</option>
                            <option value="OTH">Other</option>
                            <option value="GOV">Government</option>
                            <option value="RES">Residential shop house</option>
                            <option value="SCH">School</option>
                            <option value="EXCHANGE">Exchange</option>
                        </select>
                    </div>-->
                    <div><input class="registration-input-type" type="text" id="address1" placeholder="Address1" size="50" /></div>
                    <div><input class="registration-input-type" type="text" id="address2" placeholder="Address2" size="50" /></div>
                    <div><input class="registration-input-type" type="text" id="postal" placeholder="Postal Code" size="50" maxlength="6"/></div>
                    <div><select class="registration-select" id="areaCode" style="width: 70px; display: none;">
                            <option value="010">010</option>
                            <option value="011">011</option>
                            <option value="012">012</option>
                            <option value="013">013</option>
                            <option value="014">014</option>
                            <option value="015">015</option>
                            <option value="016">016</option>
                            <option value="017">017</option>
                            <option value="018">018</option>
                            <option value="019">019</option>	
                        </select>
                        <input class="registration-input-type" type="text" id="phone" placeholder="Phone Number e.g. 0101234567" size="50" maxlength="11"/></div>
                    <div><input class="registration-input-type" type="text" id="mobile" placeholder="Mobile Number e.g. 01012345678" size="50" maxlength="12"/></div>
                    <div><input class="registration-input-type" type="text" id="email" placeholder="Email Address" size="50"/></div>

                </div><!-- registration-right-panel -->
                <!--<h1 class="registration-h1">DEBIT/CREDIT CARD DETAILS</h1>
                <div class="registration-left-panel">
                    <div><input class="registration-input-type" type="text" id="ccName" placeholder="Name on Card" size="50" /></div>
                    <div><input class="registration-input-type" type="text" id="ccNumber" placeholder="Card Number" size="50" maxlength="22"/></div>
                </div>
                <div class="registration-right-panel">
                    <div>
                             <div> <input class="registration-input-type" type="text" id="ccYear" placeholder="Expiration Year (YYYY)" size="4" maxlength="4" /></div><div><input class="registration-input-type" type="text" id="ccMonth" placeholder="Expiration Month (MM)" size="4" maxlength="2"/></div>
                    </div>-->

                    <div class="registration-button"><input type="button" id="prev" value="Previous" onclick="window.location = '/mysignup-live/';"/></div>
                    <div class="registration-button"><input type="button" id="next" value="Next" onclick="gotoPlanDetails();"/></div>
                </div>
                <!-- FEEDBACK FORM -->
                <div class="feedbackFormWrapper">
                    <div class="feedbackFormDivider"></div>
                    <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                    <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                    <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                        <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                    </div><!-- //feedbackFormContent -->
                </div><!-- //feedbackFormWrapper -->

            </div><!-- //registrationWrapper -->
        </div><!-- //wrapper -->
       
    </body>
    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
        function feedbackFormSubmit() {
            $('#feedbackFormGeneric').verify({
                if (result) {
                    var firstName = $('#firstName').val();
                    var lastName = $('#lastName').val();
                    var email = $('#email').val();
                    var phone = $('#phone').val();
                    var message = $('#message').val();
                    var param = {"firstName": firstName, "lastName": lastName, "email": email, "phone": phone, "message": message};
                    $.ajax({
                        url: "feedbackFormEngine.php",
                        data: param,
                        type: "POST",
                        success: (function (data) {
                            swal({
                                title: "Thank You!",
                                text: "Email sent successfullly.",
                                showConfirmButton: false,
                                type: "success"
                            });
                            //window.location.href=window.location.href;
                        }),
                        complete: function () {
                            window.location.href = window.location.href;
                        }
                    });
                }
            });
        }

        $("#feedbackFormContent").load("feedbackForm.php");
        $('.feedbackFormLink').magnificPopup({
            type: 'inline',
            midClick: true
        });
    </script>

    <script>
        $(function () {
            $("#dob").datepicker({
                showMonthAfterYear: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                maxDate: "-18Y",
                yearRange: "-100:maxDate",
                beforeShow: function () {
                    $(".ui-datepicker").css('font-size', 17);
                }
            });
            $('#expDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: false,
                dateFormat: 'MM yy',
                yearRange: '2019:2028',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        });

        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });
    </script>
    <script>
        $("#nric").css("display", "none");
        $('select#nationality').change(function () {
            var value = $(this).val();
            if (value === "Malaysian/Permanent Resident")
            {
                $("#nric").css("display", "block");
                $("#nric").attr("placeholder", "MyKad Number (######–##–####)").blur();
            } else
            {
                $("#nric").css("display", "block");
                $("#nric").attr("placeholder", "Passport").blur();
            }
        });
    </script> 
</html>
