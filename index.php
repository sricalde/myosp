<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>VQ Online Application Form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/css-mobile-small.css" rel="stylesheet" type="text/css">
        <link href="css/css-mobile.css" rel="stylesheet" type="text/css">
        <link href="css/css-smartphone.css" rel="stylesheet" type="text/css">
        <link href="css/css-tablet.css" rel="stylesheet" type="text/css">
        <link href="css/css-notebook.css" rel="stylesheet" type="text/css">
        <link href="css/css-desktop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css"/>
        <link rel="stylesheet" type="text/css" href="assets/Magnific-Popup-master/dist/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="assets/sweetalert-master/dist/sweetalert.css"/>

        <script src="assets/sweetalert-master/dist/sweetalert.min.js"></script>
        <script src="jquery-1.12.0.min.js"></script>
        <script src="js/verify.notify.min.js"></script>
        <script src="jquery.cookie.js"></script>
        <script src="util.js"></script>
        <script src="assets/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
        <script src="backfix.min.js"></script>
        <style>
            @media only screen and (min-width: 481px) and (max-width: 640px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -25px;
                }
                #progressbar li:nth-child(4):before {
                    width: 60px;
                    content: ". . . .";
                    padding: 0px 20px 0px 20px;
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -40px;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #pb5, #pb6, #pb7, #pb8{
                    display: none !important;
                }
                #pbtxt4{
                    display: none;
                }
            }
            @media only screen and (min-width: 321px) and (max-width: 480px) {
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -25px;
                }
                #progressbar li:nth-child(4):before {
                    width: 60px;
                    content: ". . . .";
                    padding: 0px 20px 0px 20px;
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -40px;
                }
                #progressbar li:nth-child(4):after {
                    content: none;
                }
                #pb5, #pb6, #pb7, #pb8{
                    display: none !important;
                }
                #pbtxt4{
                    display: none;
                }
            }
            @media only screen and (max-width: 320px){
                .signupFlow{
                    margin-top: 10px;
                    text-align: center;
                    margin-left: -30px;
                }
                #progressbar li:nth-child(3):before {
                    width: 80px;
                    content: ". . . . .";
                    padding: 0px 20px 0px 20px;
                    background: none;
                    color: #F15757;
                    font-weight: bolder;
                    font-size: 25px;
                    margin-left: -40px;
                }
                #progressbar li:nth-child(3):after {
                    content: none;
                }
                #pb4, #pb5, #pb6, #pb7, #pb8{
                    display: none !important;
                }
                #pbtxt3{
                    display: none;
                }
            }
        </style>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TF99FW');</script>
        <!-- End Google Tag Manager -->

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF99FW"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div class="top-border"></div>
        <div class="signupFlow">
            <ul id="progressbar">
                <li id="pb1" class="active"><span id="pbtxt1">NEW SUBSCRIBER</span></li>
                
                <li id="pb3"><span id="pbtxt3">SUBSCRIBER'S DETAILS</span></li>
                <!--<li id="pb4"><span id="pbtxt4">VIEWQWEST SERVICE PLAN</span></li>-->
                <!--<li id="pb5"><span id="pbtxt5">PREFERRED INSTALLATION DATE AND TIME</span></li>-->
                <li id="pb6"><span id="pbtxt6">CONFIRMATION OF DETAILS</span></li>
                <li id="pb7"><span id="pbtxt7">TERMS AND CONDITIONS</span></li>
                <li id="pb8"><span id="pbtxt8">SUCCESS</span></li>
                <div id="progressbarConnector"></div>
            </ul>
        </div><!-- //signupFlow -->
        <div class="wrapper">
            <div class="signupWrapper">
                <div class="vq-logo">
                    <img src="images/vq-logo/vq-logo-small.png" width="auto" height="auto">
                </div>
                <div class="pageHeader">Residential Broadband Online Signup</div>
                <hr id="top-hr">
                <h1 class="index-h1">CHOOSE THE PLAN YOU WISH TO SIGN UP</h1>
                <div id="signup-buttons">
                    

                   <!-- <div class="signupPlanWrapper" style="display: none;">
                            <div class="planSelectHeaderWrapper">
                               <div class="planSelectionHeader " style="margin-top:50px;">Fibernet 1Gbps - Upfront Promo</div>
                               <div class="planSelectionHeaderDivider"></div>
                            </div>
                            <div class="planSelectWrapper">
                                  <div class="planSelectContainerLink" onclick="gotoFeasibilityCheck('1Gbps $888');">
                                  <div class="signupButton">
                                     <div class="signupButtonText_1Line">Fibernet 1Gbps @ $888</div>
                                  </div>
                                  <div class="planSelectDetailsWrapper">
                                     <div class="planSelectPriceWrapper">
                                        <div class="planSelectPrice">$888 <span class="onetime-payment">(One-Time Payment)</span></div>
                                     </div>
                                     <div class="planSelectDetailsContents">
                                        <div class="planSelectIncludes">Plan Includes:</div>
                                        <div class="planSelectText">24 months subscription to Fibernet 1Gbps</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free 9 months Freedom DNS (optional @ $10.70/mth thereafter) </div>
                                        <div class="planSelectDivider"></div>
										<div class="planSelectText">Free Modem Rental</div> 
										<div class="planSelectDivider"></div>		
                                        <div class="planSelectText">$100 Router Discount</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free 1x Static IP Address</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free Delivery & Self-Installation</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Registration Fee ($53.50) Waived.</div>
                                     </div>
                                  </div>
                               </div>
                          </div>
          
                          </div>
						  <div class="signupPlanWrapper" style="display: none;">
                            <div class="planSelectHeaderWrapper">
                               <div class="planSelectionHeader" style="margin-top:50px;">Fibernet 2Gbps â€“ Upfront Promo</div>
                               <div class="planSelectionHeaderDivider"></div>
                            </div>
                            <div class="planSelectWrapper">
                                  <div class="planSelectContainerLink" onclick="gotoFeasibilityCheck('2Gbps $1299');">
                                  <div class="signupButton">
                                     <div class="signupButtonText_1Line">Fibernet 2Gbps @ $1299</div>
                                  </div>
                                  <div class="planSelectDetailsWrapper">
                                     <div class="planSelectPriceWrapper">
                                        <div class="planSelectPrice">$1299 <span class="onetime-payment">(One-Time Payment)</span></div>
                                     </div>
                                     <div class="planSelectDetailsContents">
                                        <div class="planSelectIncludes">Plan Includes:</div>
                                        <div class="planSelectText">24 months subscription to Fibernet 2Gbps â€“ Single Network</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free 12 months Freedom DNS (optional @ $10.70/mth thereafter) </div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free 12 months OneVoice (optional @ $3.95/mth thereafter)</div>
                                        <div class="planSelectDivider"></div>
										<div class="planSelectText">Free Modem Rental</div> 
										<div class="planSelectDivider"></div>		
                                        <div class="planSelectText">$100 Router Discount</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free 1x Static IP Address</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free Delivery & VQ Installation</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Registration Fee ($53.50) Waived.</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free FTP Installation (up to $235.40)</div>
                                     </div>
                                  </div>
                               </div>
                          </div>
          
					  </div>-->
			<div class="signupPlanWrapper">
                        <div class="planSelectHeaderWrapper" style="display: none;">
                            <div class="planSelectionHeader">100Mbps Bundle</div>
                            <div class="planSelectionHeaderDivider"></div>
                        </div>
                        <div class="planSelectWrapper">
                            <div class="planSelectContainerLink" onclick="gotoFeasibilityCheck('100Mbps - RM148');">
                                <div class="signupButton">
                                    <div class="signupButtonText"><span class="textLineBreak">100Mbps Bundle<br>(24 Month Contact)</div>
                                </div>
                                <div class="planSelectDetailsWrapper">
                                    <div class="planSelectPriceWrapper">
                                        <div class="planSelectPrice">RM148<span class="planSelectPricePerMonth">/mth</span></div>
                                    </div>
                                    <div class="planSelectDetailsContents">
                                        <div class="planSelectIncludes">Plan Includes:</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free Rental AC1200 Smart WiFi <br/> Router (worth RM320)</div>
										<div class="planSelectDivider"></div>
										 <div class="planSelectText">Free Dynamic Public IP Address</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">24/7 Support</div>
                                    </div><!-- //planSelectDetailsContents -->
                                </div><!-- //planSelectDetailsWrapper -->
                            </div><!-- //planSelectContainerLink -->
                        </div><!-- //planSelectWrapper -->
                    </div><!-- //signupPlanWrapper -->


                    <div class="signupPlanWrapper">
                        <div class="planSelectHeaderWrapper" style="display: none;">
                            <div class="planSelectionHeader planSelectionHeader2Gbps" style="margin-top:50px;">300Mbps Bundle</div>
                            <div class="planSelectionHeaderDivider"></div>
                        </div>
                        <div class="planSelectWrapper">
                            <div class="planSelectContainerLink" onclick="gotoFeasibilityCheck('300Mbps - RM188');">
                                <div class="signupButton">
                                    <div>300Mbps Bundle<br>(24 Month Contact)</div>
                                </div>
                                <div class="planSelectDetailsWrapper">
                                    <div class="planSelectPriceWrapper">
                                        <div class="planSelectPrice">RM188<span class="planSelectPricePerMonth">/mth</span></div>
                                    </div>
                                    <div class="planSelectDetailsContents">
                                        <div class="planSelectIncludes">Plan Includes:</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free Rental AC1200 Smart WiFi <br/> Router (worth RM320)</div>
										<div class="planSelectDivider"></div>
										 <div class="planSelectText">Free Dynamic Public IP Address</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">24/7 Support</div>
                                    </div><!-- //planSelectDetailsContents -->
                                </div><!-- //planSelectDetailsWrapper -->
                            </div><!-- //planSelectContainerLink -->
                        </div><!-- //planSelectWrapper -->

                    </div><!-- //signupPlanWrapper -->
                    
                    <div class="signupPlanWrapper" style="margin-top: 80px;" id="signupPlanWrapperCenter">
                        <div class="planSelectHeaderWrapper" style="display: none;">
                            <div class="planSelectionHeader planSelectionHeader2Gbps" style="margin-top:50px;">500Mbps Bundle</div>
                            <div class="planSelectionHeaderDivider"></div>
                        </div>
                        <div class="planSelectWrapper">
                            <div class="planSelectContainerLink" onclick="gotoFeasibilityCheck('500Mbps - RM298');">
                                <div class="signupButton">
                                    <div>500Mbps Bundle<br>(24 Month Contact)</div>
                                </div>
                                <div class="planSelectDetailsWrapper">
                                    <div class="planSelectPriceWrapper">
                                        <div class="planSelectPrice">RM298<span class="planSelectPricePerMonth">/mth</span></div>
                                    </div>
                                    <div class="planSelectDetailsContents">
                                        <div class="planSelectIncludes">Plan Includes:</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">Free Rental AC1200 Smart WiFi <br/> Router (worth RM320)</div>
										<div class="planSelectDivider"></div>
										 <div class="planSelectText">Free Dynamic Public IP Address</div>
                                        <div class="planSelectDivider"></div>
                                        <div class="planSelectText">24/7 Support</div>
                                    </div><!-- //planSelectDetailsContents -->
                                </div><!-- //planSelectDetailsWrapper -->
                            </div><!-- //planSelectContainerLink -->
                        </div><!-- //planSelectWrapper -->

                    </div><!-- //signupPlanWrapper -->
                    
                </div><!-- //signup-buttons -->
				
                <!-- FEEDBACK FORM -->
                <div class="feedbackFormWrapper">
					<p class="gst-note" style="font-family: 'open_sansregular';font-size: 12px;  color: #cbcbcb;">All prices are subject to 6% GST.</p>
                    <div class="feedbackFormDivider"></div>
                    <div class="feedbackFormText">Do You Have Some Feedback About This Signup Portal?</div>
                    <div class="feedbackFormButton feedbackFormLink" href="#feedbackFormContent">Contact Us!</div>
                    <div id="feedbackFormContent" class="white-popup mfp-hide popupContactFormWrapper">
                        <div class="feedbackFormContainer" id="feedbackFormContent"></div>
                    </div><!-- //feedbackFormContent -->
                </div><!-- //feedbackFormWrapper -->

            </div><!-- //signupWrapper -->
			
        </div><!-- //wrapper -->
    </body>
    <script type="text/javascript">
        bajb_backdetect.OnBack = function ()
        {
        var onBack = confirm("Your data will be lost. Are you sure you want to go back ?");
        if (onBack == true) {
        alert("Your data will be lost");
        } else {

        window.history.forward()
        }
        }
    </script>

    <script>
        function gotoFeasibilityCheck(plan) {
        // swal({
        // title: "Session Alert",
                // text: 'We are starting your signup session.\n'
                // + 'Kindly do not refresh or press the \"Back\" button while going through the signup process as this may result in a submission error as information entered may be lost.'
                // ,
                // showConfirmButton: true,
                // type: "warning",
                // confirmButtonText: "Proceed"
                
        // }, function(){
			setVqValue('plan', plan);
			window.location.href = "registration.php"; 
        // });
        }
        //console.log(getVqValue('salesStaffName'));
        //console.log(getVqValue('salesLocation'));
    </script>

    <!-- FEEDBACK FORM -->
    <script type="text/javascript">
        $("#feedbackFormContent").load("feedbackForm.php");
        $('.feedbackFormLink').magnificPopup({
        type: 'inline',
                midClick: true
        });
    </script>
<!-- <script>
  if(getVqValue('stepOneVisited') != 'true')
  {
    window.location.href = "index.php";
  }
  else
  {
    setVqValue('stepTwoVisited', true);
  }
</script> -->
</html>

 